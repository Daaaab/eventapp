import 'package:flutter/material.dart';

class HexUtils {
  static Color setBrightness(Color color, double factor) {
    final r = (color.red * factor).round();
    final g = (color.green * factor).round();
    final b = (color.blue * factor).round();
    return Color.fromARGB(
      color.alpha,
      _normalize(r),
      _normalize(g),
      _normalize(b),
    );
  }

  static int _normalize(int value){
    if(value > 254)
      return 254;
    else if (value < 0)
      return 0;
    else
      return value;
  }

  static Color convert(String hex) {
    final modified = hex.replaceFirst(RegExp(r"#"), "0xff");
    return Color(int.parse(modified));
  }
}
