import 'package:intl/intl.dart';

class Format{
  static String hourMinutes(DateTime dateTime) => DateFormat().add_Hm().format(dateTime);
}