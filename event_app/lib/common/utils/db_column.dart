class DbColumn<T, K>{
  final T _name;
  final K _type;
  
  const DbColumn(this._name, this._type);

  T get name => _name;
  K get type => _type;
}