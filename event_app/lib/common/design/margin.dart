class Margin{
  static const double half = 4;
  static const double small = 8;
  static const double medium = 16;
  static const double large = 24;
  static const double edgeMargin = large;
}