import 'package:event_app/common/design/colors/design_colors.dart';
import 'package:event_app/common/design/font_size.dart';
import 'package:event_app/common/di/kiwi_injector.dart';
import 'package:flutter/material.dart';

//- Text normal (tytuły eventów)
//- Text description (opisy)
//- Text highlight
//- Text header
//- Text group
//- Text hour

class TextStyles {
  static final design = getContainer()<DesignColors>();
  static final TextStyle normal = _TextStyleNormal(design);
  static final TextStyle title = _TextStyleTitle(design);
  static final TextStyle description = _TextStyleDescription(design);
  static final TextStyle extraDescription = _TextStyleExtraDescription(design);
  static final TextStyle highlight = _TextStyleHighlight(design);
  static final TextStyle header = _TextStyleHeader(design);
  static final TextStyle group = _TextStyleGroup(design);
  static final TextStyle hour = _TextStyleHour(design);
  static final TextStyle rating = _TextStyleRating(design);
  static final TextStyle ratingSelected = _TextStyleRatingSelected(design);
  static final TextStyle highlightContrast = _TextStyleHighlightContrast(design);
  static final TextStyle welcome = _TextStyleWelcome(design);
  static final TextStyle welcomeHilight = _TextStyleWelcomeHighlight(design);
}

class _TextStyleTitle extends TextStyle {
  _TextStyleTitle(DesignColors colors)
      : super(
          color: colors.textColor,
          fontWeight: FontWeight.bold,
          fontSize: FontSize.large,
        );
}

class _TextStyleNormal extends TextStyle {
  _TextStyleNormal(DesignColors colors)
      : super(
          color: colors.textColor,
          fontSize: FontSize.medium,
        );
}

class _TextStyleDescription extends TextStyle {
  _TextStyleDescription(DesignColors colors)
      : super(
          color: colors.textColorAlt,
          fontSize: FontSize.medium,
        );
}

class _TextStyleExtraDescription extends TextStyle {
  _TextStyleExtraDescription(DesignColors colors)
      : super(
          color: colors.textColorAlt,
          fontSize: FontSize.small,
        );
}

class _TextStyleHighlight extends TextStyle {
  _TextStyleHighlight(DesignColors colors)
      : super(
          color: colors.textHighlightColor,
          fontSize: FontSize.medium,
          fontWeight: FontWeight.bold,
        );
}

class _TextStyleHighlightContrast extends TextStyle {
  _TextStyleHighlightContrast(DesignColors colors)
      : super(
          color: colors.white,
          fontSize: FontSize.medium,
          fontWeight: FontWeight.bold,
        );
}

class _TextStyleHeader extends TextStyle {
  _TextStyleHeader(DesignColors colors)
      : super(
          color: colors.textColorHeader,
          fontSize: FontSize.large,
        );
}

class _TextStyleGroup extends TextStyle {
  _TextStyleGroup(DesignColors colors)
      : super(
          color: colors.textColorHeader,
          fontSize: FontSize.large,
          fontWeight: FontWeight.bold,
        );
}

class _TextStyleHour extends TextStyle {
  _TextStyleHour(DesignColors colors)
      : super(
          color: colors.textColorAlt,
          fontSize: FontSize.large,
          fontWeight: FontWeight.bold,
        );
}

class _TextStyleRating extends TextStyle {
  _TextStyleRating(DesignColors colors)
      : super(
          color: colors.textColor,
          fontWeight: FontWeight.bold,
          fontSize: FontSize.large,
        );
}

class _TextStyleRatingSelected extends TextStyle {
  _TextStyleRatingSelected(DesignColors colors)
      : super(
          color: colors.white,
          fontWeight: FontWeight.bold,
          fontSize: FontSize.large,
        );
}

class _TextStyleWelcomeHighlight extends TextStyle {
  _TextStyleWelcomeHighlight(DesignColors colors)
      : super(
    color: colors.textHighlightColor,
    fontWeight: FontWeight.bold,
    fontSize: FontSize.large,
  );
}

class _TextStyleWelcome extends TextStyle {
  _TextStyleWelcome(DesignColors colors)
      : super(
    color: colors.textColor,
    fontSize: FontSize.large,
  );
}
