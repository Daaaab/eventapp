import 'dart:ui';

abstract class DesignColors{

  // Basic colors///////////////
  Color get white;
  Color get primaryColor;
  Color get accentColor;

  Color get ratingBackgroundColor;

  Color get textColor;
  Color get textColorAlt;
  Color get textColorHeader;
  Color get textHighlightColor;
  Color get textErrorColor;
  //////////////////////////////

  Color get backgroundColor;
  Color get appBarBackgroundColor;

  //////////////////////////////

  Color get statusBarColor;

  /////////////////////////////

  Color get separatorColor;

  Color get logoColor;
}