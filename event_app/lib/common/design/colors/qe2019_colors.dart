import 'dart:ui';

import 'package:event_app/common/design/colors/design_colors.dart';
import 'package:flutter/material.dart';

class QE2019Colors implements DesignColors{

  static final _white = Colors.white;
  static final _qeGreen = Color(0xff347C39);

  final white = _white;

  final Color primaryColor = _white;
  final Color accentColor = _qeGreen;

  final Color ratingBackgroundColor = Color(0xffECFFEC);

  final Color textColor = Color(0xff252525);
  final Color textColorAlt = Color(0xff515151);
  final Color textColorHeader = Color(0xff8F8F8F);
  final Color textHighlightColor = _qeGreen;
  final Color textErrorColor = Color(0xffc7282c);

  final Color backgroundColor = _white;
  final Color appBarBackgroundColor = _white;

  final Color statusBarColor = _qeGreen.withOpacity(0.3);
  final Color separatorColor = Color(0xffE8E8E8);
  final Color logoColor = Color(0xff9AB19B);
}