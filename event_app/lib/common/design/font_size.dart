class FontSize{
  static const double small = 10;
  static const double medium = 14;
  static const double large = 16;
}