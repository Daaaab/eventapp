import 'package:event_app/bloc/main/main_bloc.dart';
import 'package:event_app/common/architecture/bloc_provider.dart';
import 'package:event_app/infrastructure/navigate/navigate.dart';
import 'package:event_app/model/app/event.dart';
import 'package:event_app/model/app/speaker.dart';
import 'package:event_app/pages/event_details/event_details_page.dart';
import 'package:event_app/pages/main/main_page.dart';
import 'package:event_app/pages/speaker_details/speaker_details_page.dart';
import 'package:event_app/pages/welcome/welcome_page.dart';
import 'package:flutter/material.dart';

//todo dodać routy do poszczególnych tabów maina
class RoutesConfig{
  final Map<String, Handler> _route = Map();
  static TransactionType _defaultTransaction = TransactionType.fromBottomRight;


  final _welcomeHandler = Handler(
      transactionType: _defaultTransaction,
      pageBuilder: (BuildContext context,arg){
        return WelcomePage();
      }
  );

  final _mainHandler = Handler(
      transactionType: _defaultTransaction,
      pageBuilder: (BuildContext context,arg){
        return BlocProvider(bloc: MainBloc(),  child:  MainPage());
      }
  );

  final _eventDetailsHandler = Handler(
    transactionType: _defaultTransaction,
    pageBuilder: (context, arg){
      assert(arg['event'] is Event);
      return EventDetailsPage(arg['event']);
    }
  );

  final _speakerDetailsHandler = Handler(
      transactionType: _defaultTransaction,
      pageBuilder: (context, arg){
        assert(arg['speaker'] is Speaker);
        assert(arg['events'] is List<Event>);
        return SpeakerDetailsPage(arg['speaker'], arg['events']);
      }
  );

  Map<String, Handler> get route => _route;

  RoutesConfig(){
    _route.addAll({
      'main' : _mainHandler,
      'agenda/details' : _eventDetailsHandler,
      'speakers/details' : _speakerDetailsHandler,
      'welcome' : _welcomeHandler,
    });
  }
}