class Config{

  /* Agenda Config*/
  static const bool showFromHours = true;
  static const bool showToHours = false;
  static const bool showDuration = true;
  static const bool showLocation = true;
  static const bool collapsableStartHours = false;
  static const bool showSpeakers = true;
  static const bool showGroupsInFavourites = false;
  static const bool chipAsCategory = false;
  static const bool upperCaseTabs = true;
  static const bool titleEllipsis = false;
  /* Speakers Config*/
  static const bool sortSpeakersBySurname = true;

  /*DEBUG*/
  static const bool alwaysShowWelcome = false;
}