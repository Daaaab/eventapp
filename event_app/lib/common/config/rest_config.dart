class RestConfig{
  static const backendUrl = "https://us-central1-eventaapp-5d3a4.cloudfunctions.net/";
  static const backendPath = "us-central1-eventaapp-5d3a4.cloudfunctions.net";

  static const getModules = "getModules";
  static const getAgenda = "getAgenda";
  static const addFcmToken = "addFcmToken";
  static const setRating = "setRating";
  static const getImage = "getImage";
}
