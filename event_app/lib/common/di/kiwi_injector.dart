import 'package:event_app/common/design/colors/design_colors.dart';
import 'package:event_app/common/design/colors/qe2019_colors.dart';
import 'package:event_app/infrastructure/data/database/app/app_database.dart';
import 'package:event_app/infrastructure/data/repository/repository.dart';
import 'package:event_app/infrastructure/data/repository/repository_mock.dart';
import 'package:event_app/infrastructure/security/user_id/user_id.dart';
import 'package:event_app/infrastructure/security/user_id/user_id_impl.dart';
import 'package:kiwi/kiwi.dart';

part 'kiwi_injector.g.dart';

abstract class KiwiInjector {
  @Register.singleton(Repository, from: RepositoryMock)
  @Register.singleton(UserId, from: UserIdImpl)
  @Register.singleton(AppDatabase)
  @Register.singleton(DesignColors, from: QE2019Colors)
  void mock();
}

/*
flutter packages pub run build_runner build
 */
Container getContainer(){
  var container = Container();
  return container;
}

void setupInjector(){
  _$KiwiInjector().mock();
}