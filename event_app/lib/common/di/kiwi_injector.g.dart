// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'kiwi_injector.dart';

// **************************************************************************
// InjectorGenerator
// **************************************************************************

class _$KiwiInjector extends KiwiInjector {
  void mock() {
    final Container container = Container();
    container.registerSingleton((c) => AppDatabase());
    container
        .registerSingleton<Repository, RepositoryMock>((c) => RepositoryMock());
    container.registerSingleton<UserId, UserIdImpl>((c) => UserIdImpl());
    container
        .registerSingleton<DesignColors, QE2019Colors>((c) => QE2019Colors());
  }
}
