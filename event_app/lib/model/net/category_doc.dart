class CategoryDoc {

  final String _id;
  final String _name;
  final String _color;

  CategoryDoc(this._id, this._name, this._color);

  factory CategoryDoc.fromJSON(Map<String, dynamic> json) =>
    CategoryDoc(
        json['id'],
        json['name'],
        json['color'],
    );

  String get color => _color;

  String get name => _name;

  String get id => _id;
}