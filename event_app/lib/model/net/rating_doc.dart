import 'package:event_app/model/app/rating.dart';

class RatingDoc{

  final String _eventId;
  final String _comment;
  final int _rate;
  final String _status;
  final String _uuid;

  RatingDoc(this._eventId, this._comment, this._rate, this._status, this._uuid);

  factory RatingDoc.fromRating(Rating rating, String uuid) => RatingDoc(rating.eventId, rating.comment, rating.rate, rating.status, uuid);

  String get uuid => _uuid;

  String get status => _status;

  int get rate => _rate;

  String get comment => _comment;

  String get eventId => _eventId;
}