class LinkDoc{
  final String link;
  final String text;

  LinkDoc(this.link, this.text);

  factory LinkDoc.fromJSON(Map<String, dynamic> json){

    return LinkDoc(
      json['link'],
      json['text'],
    );
  }
}