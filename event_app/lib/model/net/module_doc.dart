import 'package:event_app/model/net/article_module_doc.dart';
import 'package:event_app/model/net/gps_module_doc.dart';
import 'package:event_app/model/net/partners_module_doc.dart';

import 'link_doc.dart';
import 'location_map_doc.dart';

class ModuleDoc{
  final int _order;
  final String _type;
  final String _label;

  ModuleDoc(this._order, this._type, this._label);

  factory ModuleDoc.fromJSON(Map<String, dynamic> json) {

    final type = json['type'];
    ModuleDoc result;

    switch(type){
      case 'article':{

        final jsonLinks = json['links'] as List<dynamic>;
        final linksList = jsonLinks.map((json) =>LinkDoc.fromJSON(json)).toList();

        result = ArticleModuleDoc(
          json['order'],
          type,
          json['label'],
          json['content'],
          json['title'],
          json['photo'],
          linksList,
        );
        break;
      }
      case 'location_map':{
        result = LocationMapDoc(
          json['order'],
          type,
          json['label'],
          json['mapFile'],
        );
        break;
      }
      case 'partners':{
        result = PartnersModuleDoc(
          json['order'],
          type,
          json['label'],
          json['partners'],
        );
        break;
      }
      case 'gps':{
        result = GpsModuleDoc(
          json['order'],
          type,
          json['label'],
          json['points'],
        );
        break;
      }
    }

    return result;
  }

  String get label => _label;

  String get type => _type;

  int get order => _order;
}