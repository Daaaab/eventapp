import 'package:event_app/model/net/module_doc.dart';

class LocationMapDoc extends ModuleDoc {
  final String mapFile;

  LocationMapDoc(int order, String type, String label, this.mapFile) : super(order, type, label);
}
