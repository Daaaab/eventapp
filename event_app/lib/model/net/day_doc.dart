class DayDoc{
  String _name;
  int _order;
  String _id;

  DayDoc(this._name, this._order, this._id);

  factory DayDoc.fromJSON(Map<String, dynamic> json){
    return DayDoc(
      json['name'],
      json['order'],
      json['id'],
    );
  }

  String get name => _name;
  int get order => _order;
  String get id => _id;

//  {
//  "name": "Day 2",
//  "order": 2,
//  "id": "j2mJaJIsWrshKgP24mg1"
//  }
}