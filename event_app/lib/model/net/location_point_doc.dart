import 'package:google_maps_flutter/google_maps_flutter.dart';

class LocationPointDoc{
  final String _address1;
  final String _address2;
  final String _name;
  final LatLng _pos;

  LocationPointDoc(this._address1, this._address2, this._name, this._pos);

  LatLng get pos => _pos;

  String get name => _name;

  String get address2 => _address2;

  String get address1 => _address1;
}