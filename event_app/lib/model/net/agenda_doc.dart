import 'package:event_app/model/net/day_doc.dart';
import 'package:event_app/model/net/event_doc.dart';
import 'package:event_app/model/net/group_doc.dart';
import 'package:event_app/model/net/speaker_doc.dart';

class AgendaDoc{
  List<DayDoc> _days;
  List<EventDoc> _events;
  List<SpeakerDoc> _speakers;
  List<GroupDoc> _groups;

  AgendaDoc(this._days, this._events, this._speakers, this._groups);

  factory AgendaDoc.fromJSON(Map<String, dynamic> json){
    var eventsListMap = json['events'] as List<dynamic>;
    var mappedEventsList = eventsListMap.map((json) => EventDoc.fromJSON(json)).toList();

    var daysListMap = json['days'] as List<dynamic>;
    var mappedDaysList = daysListMap.map((json) => DayDoc.fromJSON(json)).toList();

    var speakersListMap = json['speakers'] as List<dynamic>;
    var mappedSpeakersList = speakersListMap.map((json) => SpeakerDoc.fromJSON(json)).toList();

    var groupsListMap = json['groups'] as List<dynamic>;
    var mappedGroupsList = groupsListMap.map((json) => GroupDoc.fromJSON(json)).toList();

    return AgendaDoc(mappedDaysList, mappedEventsList, mappedSpeakersList, mappedGroupsList);
  }

  factory AgendaDoc.empty(){
    return AgendaDoc([],[],[],[]);
  }

  List<GroupDoc> get groups => _groups;
  List<DayDoc> get days => _days;
  List<EventDoc> get events => _events;
  List<SpeakerDoc> get speakers => _speakers;
}