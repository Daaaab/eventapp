import 'package:event_app/model/net/module_doc.dart';
import 'package:event_app/model/net/partner_doc.dart';

class PartnersModuleDoc extends ModuleDoc{

  List<PartnerDoc> _partnersList;

  PartnersModuleDoc(int order, String type, String label, List<dynamic> partnersList):super(order, type, label){
    this._partnersList = List();
    partnersList.forEach((partnerJson){
      this._partnersList.add(PartnerDoc.fromJSON(partnerJson));
    });
  }

  List<PartnerDoc> get partnersList => _partnersList;
}