import 'package:event_app/model/net/location_point_doc.dart';
import 'package:event_app/model/net/module_doc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class GpsModuleDoc extends ModuleDoc {
  List<LocationPointDoc> _locationPoints;

  GpsModuleDoc(int order, String type, String label, List<dynamic> locationPoints) : super(order, type, label) {
    _locationPoints = List();

    locationPoints.forEach((jsonPoint) {
      final jsonLatLng = jsonPoint['pos'] as Map<String, dynamic>;

      _locationPoints.add(LocationPointDoc(
        jsonPoint['address1'],
        jsonPoint['address2'],
        jsonPoint['name'],
        LatLng(
          jsonLatLng['_latitude'],
          jsonLatLng['_longitude'],
        ),
      ));
    });
  }

  List<LocationPointDoc> get locationPoints => _locationPoints;
}
