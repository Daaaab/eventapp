import 'package:event_app/model/net/module_doc.dart';

import 'link_doc.dart';

class ArticleModuleDoc extends ModuleDoc{

  final String _content;
  final String _title;
  final String _photo;
  final List<LinkDoc> _links;

  ArticleModuleDoc(int order, String type, String label, this._content, this._title, this._photo, this._links) : super(order, type, label);

  String get content => _content;

  String get title => _title;

  String get photo => _photo;

  List<LinkDoc> get links => _links;
}