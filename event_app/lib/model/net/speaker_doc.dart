class SpeakerDoc{

  String _id;
  String _title;
  String _description;
  String _name;
  String _surname;
  String _company;
  String _photo;
  bool _isFeatured;

  SpeakerDoc(this._id, this._title, this._description, this._name, this._surname, this._company, this._photo, this._isFeatured);

  factory SpeakerDoc.fromJSON(Map<String, dynamic> json){
    return SpeakerDoc(
      json['id'],
      json['title'],
      json['description'],
      json['name'],
      json['surname'],
      json['company'],
      json['photo'],
      json['featured']
    );
  }

  bool get isFeatured => _isFeatured;
  String get photo => _photo;
  String get company => _company;
  String get surname => _surname;
  String get name => _name;
  String get description => _description;
  String get title => _title;
  String get id => _id;

//  {
//  "name": "Krystyna",
//  "surname": "Czubówna",
//  "title": "Lektor",
//  "company": "Januszex Ltd.",
//  "description": "Proin aliquam quis, massa. Donec in nisl. Ut aliquet, diam magna iaculis arcu iaculis at, aliquet elit. Mauris eget massa. Nunc et mi. Cras ut metus feugiat dui, in est a lorem id mauris turpis, et netus et netus et ultrices posuere, tempor lorem. Suspendisse potenti. Morbi cursus wisi, ullamcorper pede quis massa et magnis dis parturient montes, nascetur ridiculus mus. Praesent ac ligula bibendum leo vel massa. Pellentesque sodales.  Duis consequat wisi. Aenean nulla luctus id, odio. Nam nec nunc mauris, rutrum euismod, enim. Sed faucibus, tortor eget quam tristique velit, vitae arcu a augue. Sed mauris rhoncus aliquam convallis. Cras consectetuer nec, turpis. Nullam vulputate imperdiet, risus nibh nulla eu mauris dui quis ante. Nam lacus. Vivamus fermentum ultrices volutpat. Curabitur at lacus rhoncus tempor. Phasellus ipsum pharetra accumsan nisl mollis luctus et mi. Sed tincidunt nonummy, tellus.",
//  "featured": true,
//  "id": "F2iNVe5dkGlYKpZCwOyi"
//  }
}