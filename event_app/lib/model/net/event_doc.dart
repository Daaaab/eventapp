
import 'package:event_app/model/net/category_doc.dart';
import 'package:event_app/model/net/speaker_doc.dart';
import 'package:flutter/foundation.dart';

class EventDoc{

  String _id;
  String _title;
  String _description;
  String _startTime;
  String _endTime;
  String _dayId;
  String _location;
  String _group;
  List<CategoryDoc> _category;
  List<SpeakerDoc> _speakers;
  
  EventDoc(this._id, this._title, this._description, this._startTime, this._endTime, this._dayId, this._location, this._group, this._speakers, this._category);

  factory EventDoc.fromJSON(Map<String, dynamic> json){

    List<SpeakerDoc> mappedSpeakersList;
    List<CategoryDoc> mappedCategoriesList;

    if(json.containsKey('speakers') && json['speakers'][0] != null){
      var speakersListMap = json['speakers'] as List<dynamic>;
      mappedSpeakersList = speakersListMap.map((speakerJson)=> SpeakerDoc.fromJSON(speakerJson)).toList();
    }

    if(json.containsKey('category')&& json['category'][0] != null){
      var categoriesListMap = json['category'] as List<dynamic>;
      mappedCategoriesList = categoriesListMap.map((json) => CategoryDoc.fromJSON(json)).toList();
    }

    return EventDoc(
      json['id'],
      (json['title'] as String).trim(),
      json['description'],
      json['startTime'],
      json['endTime'],
      json['dayId'],
      json['location'],
      json['group'],
      mappedSpeakersList,
      mappedCategoriesList,
    );
  }

  List<SpeakerDoc> get speakers => _speakers;
  String get dayId => _dayId;
  String get endTime => _endTime;
  String get startTime => _startTime;
  String get description => _description;
  String get title => _title;
  String get id => _id;
  String get location => _location;
  String get group => _group;
  List<CategoryDoc> get category => _category;

//  {
//            "id": "9Vpg4JvRBiiADmM1T0vY",
//            "title": "Event 2",
//            "description": "Lorem ipsum dolor sit amet risus. Sed in enim urna, id ipsum. Integer non nunc. Nunc ut odio et malesuada fames ac elit porttitor eros, ut tortor. Donec suscipit sed, suscipit enim. Mauris pretium tincidunt. Nullam euismod, volutpat aliquam tortor. Praesent in augue turpis, fermentum mi, gravida eros eros in felis non nisl ac nunc. Maecenas at metus. Aenean ac lacus. Duis quis wisi. Praesent feugiat. Donec euismod non, sollicitudin.  Nunc gravida. In lacus sit amet neque vitae ornare eu, tortor. Maecenas tortor id fringilla et, lobortis fringilla, massa. Vivamus ut eros nec felis facilisis neque, fringilla est, at libero. Curabitur lobortis semper. Sed tincidunt viverra. Pellentesque laoreet purus sit amet, consectetuer adipiscing laoreet, est ullamcorper aliquam. Etiam nibh massa sit amet, volutpat ligula lorem nec nulla. Fusce gravida, erat consectetuer adipiscing elit. Mauris a nulla. Phasellus vitae lorem pretium.",
//            "startTime": "2019-02-28T11:15:00.000Z",
//            "endTime": "2019-02-28T12:00:00.000Z",
//            "speakers": [
//                "F2iNVe5dkGlYKpZCwOyi",
//                "KUA2AUi0jdGwm1ljkyQG"
//            ],
//            "dayId": "j2mJaJIsWrshKgP24mg1"
//   }
}