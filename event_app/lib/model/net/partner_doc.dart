class PartnerDoc{
  String _photo;
  String _type;
  bool _isLarge;
  int _order;

  PartnerDoc(this._photo, this._type, this._isLarge, this._order);

  factory PartnerDoc.fromJSON(Map<String, dynamic> json){
    return PartnerDoc(
        json['photo'],
        json['type'],
        json['isLarge'],
        json['order']
    );
  }

  int get order => _order;

  bool get isLarge => _isLarge;

  String get type => _type;

  String get photo => _photo;
}