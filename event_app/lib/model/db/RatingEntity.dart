import 'package:event_app/model/app/rating.dart';

class RatingEntity{
  final String _eventId;
  final String _comment;
  final int _rate;
  final String _status;

  RatingEntity(this._eventId, this._comment, this._rate, this._status);

  factory RatingEntity.fromRating(Rating rating) => RatingEntity(rating.eventId, rating.comment, rating.rate, rating.status);
  factory RatingEntity.withUpdatedStatus(Rating rating, String newStatus) => RatingEntity(rating.eventId, rating.comment, rating.rate, newStatus);
  String get status => _status;

  int get rate => _rate;

  String get comment => _comment;

  String get eventId => _eventId;
}