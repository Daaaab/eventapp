import 'package:event_app/model/app/speaker.dart';
import 'package:event_app/model/net/speaker_doc.dart';

class SpeakerMap {
  static Speaker mapDocToEvent(SpeakerDoc doc) =>
      Speaker(
        doc.id,
        doc.title,
        doc.description,
        doc.name,
        doc.surname,
        doc.company,
        doc.photo,
        doc.isFeatured
      );
}
