import 'package:event_app/model/app/group.dart';
import 'package:event_app/model/net/group_doc.dart';

class GroupMap {
  static Group mapDocToGroup(GroupDoc doc) => Group(doc.name, doc.order, doc.id);
}