import 'package:event_app/model/app/day.dart';
import 'package:event_app/model/net/day_doc.dart';

class DayMap {
  static Day mapDocToDay(DayDoc doc) => Day(doc.name, doc.order, doc.id);
}