import 'package:event_app/model/app/category.dart';
import 'package:event_app/model/app/event.dart';
import 'package:event_app/model/mapping/speaker_map.dart';
import 'package:event_app/model/net/event_doc.dart';

class EventMap {
  static Event mapDocToEvent(EventDoc doc) => Event(
      doc.id,
      doc.title,
      doc.description,
      doc.dayId,
      DateTime.parse(doc.startTime).toLocal(),
      DateTime.parse(doc.endTime).toLocal(),
      doc.location,
      doc.group,
      doc.speakers?.map((speakerDoc) => SpeakerMap.mapDocToEvent(speakerDoc))?.toList(),
      doc.category?.map((categoryDoc) => Category.fromDoc(categoryDoc))?.toList(),
  );
}
