import 'package:event_app/common/config/config.dart';
import 'package:event_app/model/app/speakers_list_item.dart';

class Speaker implements Comparable<Speaker>, SpeakersListItem{
  String _id;
  String _title;
  String _description;
  String _name;
  String _surname;
  String _company;
  String _photo;
  bool _isFeatured;

  Speaker(this._id, this._title, this._description, this._name, this._surname, this._company, this._photo, this._isFeatured);

  bool get isFeatured => _isFeatured;
  String get photo => _photo;
  String get company => _company;
  String get surname => _surname;
  String get name => _name;
  String get description => _description;
  String get title => _title;
  String get id => _id;


  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is Speaker &&
              runtimeType == other.runtimeType &&
              _id == other._id;

  @override
  int get hashCode => _id.hashCode;

  @override
  int compareTo(Speaker other) {
    if(_isFeatured != null && _isFeatured && !other.isFeatured){
      return 1;
    }else if(Config.sortSpeakersBySurname){
      return _compareBySurname(other);
    }else{
      return _compareByName(other);
    }
  }

  int _compareByName(Speaker other){
    var a = _name.toLowerCase()+_surname.toLowerCase();
    var b = other.name.toLowerCase()+other.surname.toLowerCase();

    return a.compareTo(b);
  }

  int _compareBySurname(Speaker other){
    var a = _surname.toLowerCase()+name.toLowerCase();
    var b = other.surname.toLowerCase()+other.name.toLowerCase();

    return a.compareTo(b);
  }
}