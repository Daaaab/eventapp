
import 'package:event_app/model/app/agenda_list_item.dart';
import 'package:event_app/model/app/category.dart';
import 'package:event_app/model/app/speaker.dart';

class Event implements AgendaListItem, Comparable<Event>{
  String _id;
  String _title;
  String _description;
  String _dayId;
  String _location;
  String _groupId;
  List<Speaker> _speakers;
  DateTime _startTime;
  DateTime _endTime;
  List<Category> _category;

  Event(this._id, this._title, this._description, this._dayId, this._startTime, this._endTime, this._location, this._groupId, this._speakers, this._category);

  String get id => _id;
  DateTime get startTime => _startTime;
  String get title => _title;
  String get description => _description;
  String get dayId => _dayId;
  String get location => _location;
  String get groupId => _groupId;
  DateTime get endTime => _endTime;
  List<Category> get category => _category;
  List<Speaker> get speakers => _speakers;

  @override
  int compareTo(Event other) {
    if (this.startTime.isBefore(other.startTime))
      return -1;
    else if (this.startTime.isAfter(other.startTime))
      return 1;
    else if (this.startTime.isAtSameMomentAs(other.startTime))
      return 0;
    else
      return 0;
  }
}