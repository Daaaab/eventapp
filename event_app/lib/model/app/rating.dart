import 'package:event_app/model/db/RatingEntity.dart';
import 'package:event_app/model/net/rating_doc.dart';
import 'package:quiver/core.dart' as quiver;

class Rating{

  static const String pending = 'PENDING';
  static const String resend = 'RESEND';
  static const String resending = 'RESENDING';
  static const String sent = 'SENT';
  static const String unsent = 'UNSEND';
  static const String failed = 'FAILED';
  static const String disabled = 'BLOCKED';

  final String _eventId;
  final String _comment;
  final int _rate;
  final String _status;

  Rating(this._eventId, this._comment, this._rate, this._status)
      : assert(_status == pending || _status == sent || _status == unsent || _status == failed || _status == disabled|| _status == resending|| _status == resend);

  factory Rating.fromRatingEntity(RatingEntity rating) => Rating(rating.eventId, rating.comment, rating.rate, rating.status);
  factory Rating.withUpdatedStatus(RatingDoc rating, String newStatus) => Rating(rating.eventId, rating.comment, rating.rate, newStatus);
  factory Rating.updateStatus(Rating rating, String newStatus) => Rating(rating.eventId, rating.comment, rating.rate, newStatus);

  String get status => _status;

  int get rate => _rate;

  String get comment => _comment;

  String get eventId => _eventId;

  bool operator ==(o) => o is Rating && _eventId == o._eventId && _comment == o._comment && _rate == o._rate && _status == o._status;
  int get hashCode => quiver.hash4(_status, _rate, _eventId, _comment);
}