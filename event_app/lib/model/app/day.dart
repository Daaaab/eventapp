class Day implements Comparable<Day>{

  static final String favouritesDayId = 'favs';

  String _name;
  int _order;
  String _dayId;

  Day(this._name, this._order, this._dayId);

  String get name => _name;
  int get order => _order;
  String get dayId => _dayId;

  factory Day.empty() => Day("" , 0 , "");
  factory Day.favourite() => Day('Ulubione', 999, favouritesDayId);

  @override
  int compareTo(Day other) {
    if(this.order > 0 && other.order <0 || this.order > other.order){
      return 1;
    }else if (this.order < 0 && other.order > 0){
      return - 1;
    }else if (this.order < 0 && other.order < 0 || this.order == other.order){
      return 0;
    } else {
      return -1;
    }
  }
}