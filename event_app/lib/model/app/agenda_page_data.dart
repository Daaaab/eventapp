import 'package:event_app/model/app/day.dart';

class AgendaPageData{

  final List<Day> _daysList;
  final bool _showFilters;

  AgendaPageData(this._daysList, this._showFilters);

  bool get showFilters => _showFilters;

  List<Day> get daysList => _daysList;
}