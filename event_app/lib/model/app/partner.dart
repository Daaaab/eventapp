import 'package:event_app/model/net/partner_doc.dart';

class Partner{
  final String _photo;
  final bool _isLarge;

  Partner(this._photo, this._isLarge);

  factory Partner.fromPartnerDoc(PartnerDoc doc){
    final isLarge = doc.isLarge == null ? false : doc.isLarge;
    return Partner(doc.photo, isLarge);
  }

  bool get isLarge => _isLarge;

  String get photo => _photo;
}