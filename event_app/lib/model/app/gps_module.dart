import 'package:event_app/model/app/location_point.dart';
import 'package:event_app/model/app/module.dart';

class GpsModule extends Module{

  final List<LocationPoint> _locationPoints;

  GpsModule(String type, int order, String label, this._locationPoints) : super(type, order, label);

  List<LocationPoint> get locationPoints => _locationPoints;

}