import 'package:event_app/model/app/speakers_list_item.dart';

class SpeakerType implements SpeakersListItem{
  final String _type;

  SpeakerType(this._type);

  String get type => _type;

  factory SpeakerType.featured() => SpeakerType('Gość Specjalny');
  factory SpeakerType.normal() => SpeakerType('Prelegenci');
}