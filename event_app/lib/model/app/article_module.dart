import 'package:event_app/model/app/link.dart';
import 'package:event_app/model/app/module.dart';

class ArticleModule extends Module{

  final String _content;
  final String _title;
  final String _photo;
  final List<Link> _links;

  ArticleModule(int order, String type, String label, this._content, this._title, this._photo, this._links) : super(type, 0, label);

  String get content => _content;

  String get title => _title;

  String get photo => _photo;

  List<Link> get links => _links;
}