import 'package:event_app/model/net/category_doc.dart';

class Category{
  final String _id;
  final String _name;
  final String _color;

  Category(this._id, this._name, this._color);

  factory Category.fromDoc(CategoryDoc doc) => Category(doc.id, doc.name, doc.color);

  String get color => _color;

  String get name => _name;

  String get id => _id;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is Category &&
              runtimeType == other.runtimeType &&
              _id == other._id &&
              _name == other._name &&
              _color == other._color;

  @override
  int get hashCode =>
      _id.hashCode ^
      _name.hashCode ^
      _color.hashCode;



}