
import 'package:event_app/model/app/event.dart';

typedef bool Predicate(Event event);

class Filter{

  static const categoryFilter = 'categoryFilter';
  static const groupFilter = 'groupFilter';

  final String _id;
  final String _text;
  final String color;
  final String _type;
  final Predicate _predicate;
  bool isEnabled;

  Filter(this._id, this._text, this._type, this._predicate, {this.color = '#BBBBBB', this.isEnabled = false});

  Predicate get predicate => _predicate;

  String get text => _text;

  String get id => _id;

  String get type => _type;

  List<Event> apply(List<Event> events) =>
     events.where((event) =>
         _predicate(event)).toList();
}