import 'package:google_maps_flutter/google_maps_flutter.dart';

class LocationPoint{
  final String _name;
  final String _address1;
  final String _address2;
  final LatLng _pos;

  LocationPoint(this._name, this._address1, this._address2, this._pos);

  LatLng get pos => _pos;

  String get address2 => _address2;

  String get address1 => _address1;

  String get name => _name;
}