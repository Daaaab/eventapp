import 'package:event_app/model/app/location_point.dart';
import 'package:event_app/model/app/module.dart';

class LocationMapModule extends Module{

  final String mapFile;

  LocationMapModule(String type, int order, String label, this.mapFile) : super(type, order, label);

  String get locationPoints => mapFile;

}