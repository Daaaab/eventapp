import 'package:event_app/model/app/agenda_list_item.dart';

class Group implements AgendaListItem, Comparable<Group>{
  String _name;
  int _order;
  String _groupId;

  Group(this._name, this._order, this._groupId);

  factory Group.empty() => Group("", -1, null);

  String get name => _name;
  int get order => _order;
  String get groupId => _groupId;

  @override
  int compareTo(Group other) {
      if(this.order < 0 && other.order < 0){
        return 0;
      }else if(this.order < 0 && other.order >= 0){
        return -1;
      }else if(this.order >= 0 && other.order < 0){
        return 1;
      }else if (this.order > other.order)
        return 1;
      else
        return -1;
    }
  }