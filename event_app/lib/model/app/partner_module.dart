import 'package:event_app/model/app/module.dart';
import 'package:event_app/model/app/partner.dart';
import 'package:event_app/model/net/partner_doc.dart';

class PartnerModule extends Module{

  Map<String, List<Partner>> _partnersList;

  PartnerModule(String type, int order, String label, List<PartnerDoc> partnersDocList) : super(type, order, label){
    _partnersList = Map();

    partnersDocList.sort((a,b){
      if(a.order < b.order){
        return -1;
      }else if(a.order == b.order){
        return 0;
      }else if(a.order > b.order){
        return 1;
      }
    });

    partnersDocList.forEach((partnerDoc){
      final key = partnerDoc.type;
      final value = Partner.fromPartnerDoc(partnerDoc);

      if(_partnersList.containsKey(key)){
        _partnersList[key].add(value);
      }else{
        _partnersList.putIfAbsent(key, () => [value]);
      }
    });
  }

  Map<String, List<Partner>> get partnersList => _partnersList;
}