import 'package:event_app/model/app/article_module.dart';
import 'package:event_app/model/app/gps_module.dart';
import 'package:event_app/model/app/link.dart';
import 'package:event_app/model/app/location_point.dart';
import 'package:event_app/model/app/partner_module.dart';
import 'package:event_app/model/net/article_module_doc.dart';
import 'package:event_app/model/net/gps_module_doc.dart';
import 'package:event_app/model/net/location_map_doc.dart';
import 'package:event_app/model/net/module_doc.dart';
import 'package:event_app/model/net/partners_module_doc.dart';

import 'location_map_module.dart';

class Module implements Comparable<Module> {
  final String _label;
  //TODO Nie potrzebuję tutaj typu ?
  final String _type;
  final int _order;

  Module(this._type, this._order, this._label);

  factory Module.fromModuleDoc(ModuleDoc moduleDoc) {
    Module result;

    switch (moduleDoc.runtimeType) {
      case ArticleModuleDoc:
        {
          final temp = moduleDoc as ArticleModuleDoc;
          result = ArticleModule(
            temp.order,
            temp.type,
            temp.label,
            temp.content,
            temp.title,
            temp.photo,
            temp.links.map((doc) => Link(doc.text, doc.link)).toList(),
          );
          break;
        }
      case PartnersModuleDoc:
        {
          final temp = moduleDoc as PartnersModuleDoc;
          result = PartnerModule(temp.type, temp.order, temp.label, temp.partnersList);
          break;
        }
      case LocationMapDoc:
        {
          final temp = moduleDoc as LocationMapDoc;
          result = LocationMapModule(temp.type, temp.order, temp.label, temp.mapFile);
          break;
        }
      case GpsModuleDoc:
        {
          final temp = moduleDoc as GpsModuleDoc;
          result = GpsModule(
            temp.type,
            temp.order,
            temp.label,
            temp.locationPoints
                .map((locationPointDoc) => LocationPoint(
                      locationPointDoc.name,
                      locationPointDoc.address1,
                      locationPointDoc.address2,
                      locationPointDoc.pos,
                    ))
                .toList(),
          );
          break;
        }
    }

    return result;
  }

  int get order => _order;

  String get type => _type;

  String get label => _label;

  //todo wspólna klasa ComparableByOrder
  @override
  int compareTo(Module other) {
    if (this.order > 0 && other.order < 0 || this.order > other.order) {
      return 1;
    } else if (this.order < 0 && other.order > 0) {
      return -1;
    } else if (this.order < 0 && other.order < 0 || this.order == other.order) {
      return 0;
    } else {
      return -1;
    }
  }
}
