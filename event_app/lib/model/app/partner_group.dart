class PartnerGroup{
  final String _typeName;

  PartnerGroup(this._typeName);

  String get typeName => _typeName;
}