import 'dart:io';

import 'package:event_app/bloc/splash/splash_bloc.dart';
import 'package:event_app/common/architecture/bloc_provider.dart';
import 'package:event_app/common/config/routes_config.dart';
import 'package:event_app/common/design/colors/design_colors.dart';
import 'package:event_app/common/di/kiwi_injector.dart';
import 'package:event_app/infrastructure/navigate/navigate.dart';
import 'package:event_app/pages/splash/splash_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';

void main() {
  if (!isInDebugMode) {
    debugPrint = (String message, {int wrapWidth}) {};
  }else{
    debugPrint('App run in Debug Mode');
  }

  setupInjector();

  debugPaintSizeEnabled = false;
  debugPaintBaselinesEnabled = false;
  debugPaintPointersEnabled = false;

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then((_) => runApp(MyApp()));

  final colors = getContainer()<DesignColors>();

  if (Platform.isAndroid) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: colors.statusBarColor,
    ));
  }
}

bool get isInDebugMode {
  bool inDebugMode = false;
  assert(inDebugMode = true);
  return inDebugMode;
}

class MyApp extends StatefulWidget {
  MyApp() {
    Navigate.registerRoutes(routes: RoutesConfig().route, defualtTransactionType: TransactionType.fromRight);
  }

  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<StatefulWidget> {
  // This widget is the root of your application.

  final colors = getContainer()<DesignColors>();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Event App',
      theme: ThemeData(
        backgroundColor: colors.backgroundColor,
        scaffoldBackgroundColor: colors.backgroundColor,
        appBarTheme: AppBarTheme(
          elevation: 0,
          color: colors.appBarBackgroundColor,
        ),
        primaryColor: colors.primaryColor,
        accentColor: colors.accentColor,
      ),
      home: _buildHomePage(),
    );
  }

  Widget _buildHomePage() => BlocProvider(bloc: SplashBlock(), child: SplashPage());
}
