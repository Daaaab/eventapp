import 'package:event_app/common/architecture/bloc_provider.dart';
import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';

class MainBloc implements BlocBase{
  BehaviorSubject<int> _indexStream;
  
  MainBloc(){
    _indexStream = BehaviorSubject.seeded(0);
  }

  Stream<int> get indexStream => _indexStream.stream;
  
  void updateIndex(int newIndex) {
    _indexStream.value = newIndex;
  }
  
  @override
  void dispose() {
    debugPrint("MainBloc: disposing");
    _indexStream.close();

  }
}