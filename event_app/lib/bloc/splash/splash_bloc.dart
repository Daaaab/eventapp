import 'dart:async';

import 'package:event_app/common/architecture/bloc_provider.dart';
import 'package:event_app/common/di/kiwi_injector.dart';
import 'package:event_app/infrastructure/data/repository/repository.dart';
import 'package:event_app/infrastructure/security/user_id/user_id.dart';
import 'package:event_app/model/net/agenda_doc.dart';
import 'package:event_app/model/net/module_doc.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:rxdart/rxdart.dart' as rx;
import 'package:flutter/foundation.dart';

class SplashBlock implements BlocBase{

  Repository _repo;
  rx.BehaviorSubject<bool> _initSubject;
  UserId _userId;

  SplashBlock(){
    _repo = getContainer()<Repository>();
    _userId = getContainer()<UserId>();
    _initSubject = rx.BehaviorSubject();
  }

  Stream<bool> get initStream => rx.Observable.zip5(_repo.downloadAgenda, _repo.downloadModules, _initFcm(), _initUUID(), _isFirstRun(), _zipper).doOnError((){
    debugPrint('INIT ERROR');
  });

  Stream<bool> _isFirstRun() =>
      _repo.isFirstRun.then((result){
        if(result){
          _repo.setIsFirstRun(false);
        }

        //return true;
        return result;
      }).asStream();


  Stream<bool> _initUUID() => rx.Observable.zip2(_repo.uuid.asStream(), _userId.getUUID().asStream(), (dbUUID, uuid){
    if(dbUUID == null){
      debugPrint('new UUID: $uuid');
      _repo.setUUID(uuid);
    }else{
      debugPrint('UUID already exists: $uuid');
    }
    return true;
  });

  Stream<bool> _initFcm() => rx.Observable.zip2(
      FirebaseMessaging().getToken().asStream(),
      _repo.fcmToken.asStream(),
          (String fcmToken, String dbToken){
            if(fcmToken == dbToken){
              debugPrint('Fcm token already sent. Token: $dbToken');
            }else{
              debugPrint('Sending new token. Token: $fcmToken');
              _repo.setToken(fcmToken)
                  .catchError((err) => debugPrint(err.toString()));
            }
            return true;
      });

  bool _zipper(AgendaDoc agenda, List<ModuleDoc> modules, bool tokenSend, bool uuidSet, bool isFirstRun){
    debugPrint("SplashBlock: agenda: ${agenda.events.length} modules: ${modules.length}");
    _repo.updateAgenda(agenda);
    _repo.updateModules(modules);
    return isFirstRun;
  }

  @override
  void dispose() {
    debugPrint("SplashBlock: disposing");
    _initSubject.close();
  }
}