import 'dart:async';

import 'package:event_app/common/architecture/bloc_provider.dart';
import 'package:event_app/common/config/config.dart';
import 'package:event_app/common/di/kiwi_injector.dart';
import 'package:event_app/infrastructure/data/repository/repository.dart';
import 'package:event_app/model/app/agenda_list_item.dart';
import 'package:event_app/model/app/agenda_page_data.dart';
import 'package:event_app/model/app/day.dart';
import 'package:event_app/model/app/event.dart';
import 'package:event_app/model/app/filter.dart';
import 'package:event_app/model/app/group.dart';
import 'package:event_app/model/app/category.dart';
import 'package:event_app/model/net/agenda_doc.dart';
import 'package:event_app/model/net/module_doc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:uuid/uuid.dart';
import 'package:flutter/foundation.dart' as foundation;

class AgendaBloc implements BlocBase {
  Repository _repo;
  StreamSubscription _daysSub;
  StreamSubscription _agendaSub;

  BehaviorSubject<Map<Group, List<Event>>> _agendaSubject;
  BehaviorSubject<List<Day>> _daysSubject;
  BehaviorSubject<List<Filter>> _filtersSubject;

  BehaviorSubject<bool> _showFiltersSubject;

  AgendaBloc() {
    initBloc();
  }

  void initBloc() {
    foundation.debugPrint("EventsListBloc: creating");
    _repo = getContainer()<Repository>();
    _daysSubject = BehaviorSubject.seeded([Day.empty()]);
    _agendaSubject = BehaviorSubject();
    _showFiltersSubject = BehaviorSubject.seeded(false);
    _filtersSubject = BehaviorSubject.seeded(List<Filter>());

    _daysSub = _repo.daysList.listen(_updateDays);
    _agendaSub = _agendaChangesObservable().listen(_updateListItems);
  }

  Stream<List<Filter>> get categoryFiltersStream =>
      _filtersSubject.map((filters) => filters.where((filter) => filter.type == Filter.categoryFilter).toList());

  Stream<List<Filter>> get groupFiltersStream =>
      _filtersSubject.map((filters) => filters.where((filter) => filter.type == Filter.groupFilter).toList());

  Stream<List<Filter>> get filtersStream => _filtersSubject.stream.map((filters) => filters.where((filter) => filter.isEnabled).toList());

  Future refreshAgenda() =>
    Observable.zip2(_repo.downloadAgenda, _repo.downloadModules, (AgendaDoc agenda, List<ModuleDoc> modules) {
      _repo.updateAgenda(agenda);
      _repo.updateModules(modules);
    }).first;

  void setFilter(String filterId) {
    foundation.debugPrint('Filter: $filterId');
    final filters = _filtersSubject.value;
    final index = filters.indexWhere((filter) => filter.id == filterId);
    final isEnabled = !filters[index].isEnabled;
    filters[index].isEnabled = isEnabled;
    _repo.setFilter(filterId, isEnabled);
    _filtersSubject.value = filters;
  }

  void toggleFiltersMenu() {
    _showFiltersSubject.value = !_showFiltersSubject.value;
  }

  AgendaPageData get initialAgendaPageData => AgendaPageData(_daysSubject.value, _showFiltersSubject.value);

  bool get initialShowFiltersData => _showFiltersSubject.value;
  Stream<bool> get showFiltersStream => _showFiltersSubject.stream;

  List<Day> get initialDaysData => _daysSubject.value;

  List<AgendaListItem> getInitialGroupedEventsForDay(String dayId) {
    if (dayId == Day.favouritesDayId) {
      return List();
    } else {
      return _filterEvents(dayId, _agendaSubject.value, _filtersSubject.value);
    }
  }

  Stream<AgendaPageData> get agendaPageDataStream => Observable.combineLatest2(
      dayStream, _showFiltersSubject, (List<Day> daysList, bool showFilters) => AgendaPageData(daysList, showFilters));

  Stream<List<Day>> get dayStream => _daysSubject.stream.map<List<Day>>((daysList) {
        daysList.sort();
        return daysList;
      });

  Stream<List<AgendaListItem>> getEventsStreamForFavourites() =>
      Observable.combineLatest2(_repo.favsList, _agendaSubject, (List<String> favsList, Map<Group, List<Event>> groupedEvents) {
        final List<AgendaListItem> result = List();

        groupedEvents.forEach((group, events) {
          var favEvents = events.where((event) => favsList.contains(event.id));

          if (favEvents.isNotEmpty) {
            if (Config.showGroupsInFavourites) {
              result.add(group);
            }
            result.addAll(favEvents);
          }
        });

        return result;
      });

  Stream<List<AgendaListItem>> getGroupedEventsStreamForDay(String dayId) {
    if (dayId == Day.favouritesDayId) {
      return getEventsStreamForFavourites();
    } else {
      return Observable.combineLatest2(_agendaSubject, _filtersSubject,
              (Map<Group, List<Event>> groupedEvents, List<Filter> filters) =>
                  _filterEvents(dayId, groupedEvents, filters))
          .doOnError((error, stack) {
        foundation.debugPrint(error);
      });
    }
  }

  List<AgendaListItem> _filterEvents(String dayId, Map<Group, List<Event>> groupedEvents, List<Filter> filters) {
    List<AgendaListItem> result = List();
    if (groupedEvents != null) {
      final activeFilters = filters.where((filter) => filter.isEnabled).toList();
      final filteredEvents = Map<Group, List<Event>>.from(groupedEvents);

      if (filters.length > 0) {
        filteredEvents.updateAll((group, events) {
          return _applyFilters(events, activeFilters);
        });
      }
      result = _filterItemsForDay(dayId, filteredEvents);
    }

    return result;
  }

  //todo na 100% da sięto jakoś inaczej zrobić
  List<Event> _applyFilters(List<Event> events, List<Filter> filters) {
    if (filters.isEmpty) {
      return events;
    }

    final categoryFilters = filters.where((filter) => filter.type == Filter.categoryFilter);
    final groupFilters = filters.where((filter) => filter.type == Filter.groupFilter);

    List<Event> groupFiltered = List();
    if (groupFilters.isNotEmpty) {
      groupFilters.forEach((filter) {
        groupFiltered.addAll(filter.apply(events));
      });
    } else {
      groupFiltered.addAll(events);
    }

    List<Event> result = List();
    if (categoryFilters.isNotEmpty) {
      categoryFilters.forEach((filter) {
        result.addAll(filter.apply(groupFiltered));
      });
    } else {
      result.addAll(groupFiltered);
    }

    return result;
  }

  //todo przetestować eventy bez grup
  List<AgendaListItem> _filterItemsForDay(String dayId, Map<Group, List<Event>> groupedEvents) {
    final List<AgendaListItem> result = List();

    if (groupedEvents != null) {
      groupedEvents.forEach((group, events) {
        final eventsForDay = events.where((event) => event.dayId == dayId);

        if (eventsForDay.isNotEmpty) {
          result.add(group);
          result.addAll(eventsForDay);
        }
      });
    }

    return result;
  }

  void _updateListItems(Map<Group, List<Event>> agendaItems) {
    _agendaSubject.value = agendaItems;
    _updateFilters(agendaItems);
  }

  void _updateFilters(Map<Group, List<Event>> agendaItems) {
    List<Category> categories = List();
    List<Filter> filters = List();

    agendaItems.forEach((_, events) {
      events.forEach((event) {
        event.category?.forEach((category) {
          if (!categories.contains(category)) {
            categories.add(category);
          }
        });
      });
    });

    _repo.filtersList.then((activeFilters) {
      agendaItems.keys.forEach((group) {
        final id = group.name + group.groupId + 'group' + group.order.toString();
        filters.add(Filter(
          id,
          group.name,
          Filter.groupFilter,
          (event) => event.groupId == group.groupId,
          isEnabled: activeFilters.contains(id),
        ));
      });

      categories?.forEach((category) {
        final id = category.name + category.id + 'category' + category.color;
        filters.add(Filter(
          id,
          category.name,
          Filter.categoryFilter,
          (event) => event.category != null && event.category.isNotEmpty && event.category.contains(category),
          color: category.color,
          isEnabled: activeFilters.contains(id),
        ));
      });

      _filtersSubject.value = filters;
    });

    //return result;
  }

  Observable<Map<Group, List<Event>>> _agendaChangesObservable() =>
      Observable.combineLatest2(_repo.eventsList, _repo.groupsList, (List<Event> eventsList, List<Group> groupsList) {
        // Sort groups by order
        // add empty group
        // get events for given day and given group
        // sort by start time
        // add group and corresponding events to result list

        final Map<Group, List<Event>> groupedEvents = Map();

        groupsList.sort();

        groupsList.forEach((group) {
          final eventsOfGroup = eventsList.where((event) => event.groupId == group.groupId).toList();
          eventsOfGroup.sort();

          if (eventsOfGroup.isNotEmpty) {
            groupedEvents.putIfAbsent(group, () => eventsOfGroup);
          }
        });

        return groupedEvents;
      });

  void _updateDays(List<Day> daysList) {
    foundation.debugPrint("Update days: ${daysList.length}");
    // Inject favourites tab
    List<Day> daysWithFav = List.from(daysList);
    daysWithFav.add(Day.favourite());

    _daysSubject.value = daysWithFav;
  }

  void dispose() {
    foundation.debugPrint("EventsListBloc: disposing");

    _daysSub.cancel();
    _agendaSub.cancel();
    _agendaSubject.close();
    _daysSubject.close();
    _filtersSubject.close();
    _showFiltersSubject.close();
  }
}
