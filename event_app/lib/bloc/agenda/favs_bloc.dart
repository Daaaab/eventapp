import 'dart:async';

import 'package:event_app/common/architecture/bloc_provider.dart';
import 'package:event_app/common/di/kiwi_injector.dart';
import 'package:event_app/infrastructure/data/repository/repository.dart';
import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';

class FavsBloc extends BlocBase{

  Repository _repo;
  BehaviorSubject<List<String>> _favsListStream;
  StreamSubscription _favsSub;
  String _eventId;

  FavsBloc(this._eventId){
    _repo = getContainer()<Repository>();
    _favsListStream = BehaviorSubject();

    _favsSub = _repo.favsList.listen(_updateFavsList);
  }

  bool initialIsFav(String eventId) {
    var result = _checkIfFavourite(_favsListStream.value, eventId);
    return result;
  }

  Stream<bool> isFav(String eventId) => _favsListStream.map((favsList) => _checkIfFavourite(favsList, eventId));

  bool _checkIfFavourite(List<String> favsList, String eventId){
    return favsList != null ? favsList.indexOf(eventId) >= 0 : false;
  }

  void setFav(String eventId, bool isFav){
    _repo.setFav(eventId, isFav);
  }

  void _updateFavsList(List<String> favsList){
    if(!_favsListStream.isClosed){
      _favsListStream.value = favsList;
    }
  }

  @override
  void dispose() {
    //todo sprawdzić skąd leci pojedyńczy dispose przy odpalaniu
  // debugPrint("FavsBloc: disposing");
    _favsSub.cancel();
    _favsListStream.close();
  }
}