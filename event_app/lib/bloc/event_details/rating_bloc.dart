import 'dart:async';

import 'package:event_app/common/architecture/bloc_provider.dart';
import 'package:event_app/common/di/kiwi_injector.dart';
import 'package:event_app/infrastructure/data/repository/repository.dart';
import 'package:event_app/model/app/event.dart';
import 'package:event_app/model/app/rating.dart';
import 'package:rxdart/rxdart.dart';

class RatingBloc extends BlocBase {
  Repository _repo;

  StreamSubscription _ratingSub;
  BehaviorSubject<List<Rating>> _ratingStream;

  RatingBloc() {
    _repo = getContainer()<Repository>();
    _ratingStream = BehaviorSubject();

    _ratingSub = _repo.ratingList.listen(_updateRatings);
  }

  Stream<Rating> getRatingStreamForEvent(String eventId) =>
      _ratingStream.map((ratingList) => _filterRatingForEvent(ratingList, eventId));

  Rating getCurrentRatingForEvent(String eventId) => _filterRatingForEvent(_ratingStream.value, eventId);

  Rating _filterRatingForEvent(List<Rating> ratingList, String eventId) {
    if(ratingList == null){
      return null;
    }else{
      final result = ratingList.singleWhere((rating) => rating.eventId == eventId, orElse: () => null);
      return result;
    }
  }

  bool isAfterEventStart(Event event){
    final startTime = event.startTime;
    return startTime.isAfter(DateTime.now());
  }

  void removeBlocked(){
    _repo.removeBlocked();
  }

  void setRating(Event _event, int rate, String comment) {
    _repo.addRating(Rating(_event.id, comment, rate, Rating.unsent));
  }

  void _updateRatings(List<Rating> ratingsList) {
    _ratingStream.value = ratingsList;
  }

  @override
  void dispose() {
    _ratingSub.cancel();
    _ratingStream.close();
  }
}
