import 'dart:async';

import 'package:event_app/common/architecture/bloc_provider.dart';
import 'package:event_app/common/di/kiwi_injector.dart';
import 'package:event_app/infrastructure/data/repository/repository.dart';
import 'package:event_app/model/app/module.dart';
import 'package:rxdart/rxdart.dart';

class MenuBloc extends BlocBase{

  BehaviorSubject<List<Module>> _modulesStream;
  StreamSubscription _sub;
  Repository _repo;

  MenuBloc(){
    _repo = getContainer()<Repository>();
    _modulesStream = BehaviorSubject();

    _sub = _repo.modulesList.listen(_updateModules);
  }

  List<Module> get initialData => _modulesStream.value;

  Stream<List<Module>> get moduleStream => _modulesStream.stream.map((modules){
    modules.sort();
    return modules.where((module) => module.order >= 0).toList();
  });

  void _updateModules(List<Module> modules){
    _modulesStream.value = modules;
  }

  @override
  void dispose() {
    _sub.cancel();
    _modulesStream.close();
  }
}