import 'dart:async';

import 'package:event_app/common/architecture/bloc_provider.dart';
import 'package:event_app/common/di/kiwi_injector.dart';
import 'package:event_app/infrastructure/data/repository/repository.dart';
import 'package:event_app/model/app/gps_module.dart';
import 'package:event_app/model/app/module.dart';
import 'package:rxdart/rxdart.dart';

class GpsModuleBloc extends BlocBase{

  BehaviorSubject<GpsModule> _gpsModuleStream;
  StreamSubscription _sub;
  Repository _repo;

  GpsModuleBloc(){
    _repo = getContainer()<Repository>();
    _gpsModuleStream = BehaviorSubject();

    _sub = _repo.modulesList.listen(_updateModules);
  }

  GpsModule get initialData => _gpsModuleStream.value;

  Stream<GpsModule> get moduleStream => _gpsModuleStream.stream;

  void _updateModules(List<Module> modules){

    if(modules != null && modules.length > 0){
      final gpsModule = modules.firstWhere((module) => module is GpsModule);

      _gpsModuleStream.value = gpsModule;
    }
  }
  
  @override
  void dispose() {
    _sub.cancel();
    _gpsModuleStream.close();
  }
}