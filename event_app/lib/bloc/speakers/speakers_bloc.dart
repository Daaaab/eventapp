import 'dart:async';

import 'package:event_app/common/architecture/bloc_provider.dart';
import 'package:event_app/common/di/kiwi_injector.dart';
import 'package:event_app/infrastructure/data/repository/repository.dart';
import 'package:event_app/model/app/event.dart';
import 'package:event_app/model/app/speaker.dart';
import 'package:event_app/model/app/speaker_type.dart';
import 'package:event_app/model/app/speakers_list_item.dart';
import 'package:rxdart/rxdart.dart';
import 'package:flutter/foundation.dart';

class SpeakersBloc extends BlocBase {
  Repository _repo;

  StreamSubscription _speakersSub;
  StreamSubscription _eventsSub;

  BehaviorSubject<List<Speaker>> _speakersSubject;
  BehaviorSubject<List<Event>> _eventsSubject;

  SpeakersBloc() {
    _speakersSubject = BehaviorSubject();
    _eventsSubject = BehaviorSubject();
    _repo = getContainer()<Repository>();
    _speakersSub = _repo.speakersList.listen(_updateSpeakers);
    _eventsSub = _repo.eventsList.listen(_updateEvents);
  }

  List<SpeakersListItem> get initialData => _mapToSpeakerItems(_speakersSubject.value);

  Stream<List<SpeakersListItem>> get speakersStream => _speakersSubject.map<List<SpeakersListItem>>(_mapToSpeakerItems);

  Stream<List<Event>>  getEventsForSpeakerStream(Speaker speaker) =>
    _eventsSubject.map((events) =>
      events.where((event) => (event.speakers != null && event.speakers.isNotEmpty && event.speakers.contains(speaker))).toList()
    );

  List<SpeakersListItem> _mapToSpeakerItems(List<Speaker> speakers) {
    final result = List<SpeakersListItem>();

    final featured = speakers.where(_checkIfFeatured).toList();

    if (featured != null && featured.isNotEmpty) {
      featured.sort();
      result.add(SpeakerType.featured());
      result.addAll(featured);
    }

    final normal = speakers.where(_checkIfNotFeatured).toList();
    normal.sort();
    result.add(SpeakerType.normal());
    result.addAll(normal);

    return result;
  }

  bool _checkIfNotFeatured(Speaker speaker) {
    if (speaker.isFeatured == null || !speaker.isFeatured) {
      return true;
    } else {
      return false;
    }
  }

  bool _checkIfFeatured(Speaker speaker) {
    if (speaker.isFeatured == null || !speaker.isFeatured) {
      return false;
    } else {
      return true;
    }
  }

  _updateEvents(List<Event> eventsList) {
    _eventsSubject.value = eventsList;
  }

  _updateSpeakers(List<Speaker> speakersList) {
    _speakersSubject.value = speakersList;
  }

  @override
  void dispose() {
    debugPrint('SpeakersBloc: disposing');
    _speakersSub.cancel();
    _eventsSub.cancel();
    _speakersSubject.close();
    _eventsSubject.close();
  }
}
