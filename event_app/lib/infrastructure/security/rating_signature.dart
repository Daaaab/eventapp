import 'dart:convert' as convert;
import 'package:crypto/crypto.dart' as crypto;
import 'package:flutter/foundation.dart';

class RatingSignature{

  static const _modulo = 748;
  static const _substr = 5;
  static const _maxSum = 2147483647;
  static const _moduloResult = 1;
  
  static int calculate(String eventId, String uuid, String comment, int rate){

    var i = 3;
    
    final eventHash = _generateMd5(eventId.toLowerCase());
    //debugPrint('eventHash: $eventHash');

    final rateHash = _generateMd5(rate.toString().toLowerCase());
    //debugPrint('rateHash: $rateHash');

    final combined = rateHash + uuid.toLowerCase() + eventHash + comment.toLowerCase();
    //debugPrint('combined: $combined');

    final combinedHash = _generateMd5(combined);
    //debugPrint('combinedHash: $combinedHash');

    final digest = int.parse(combinedHash.substring(0, _substr), radix: 16);
    //debugPrint('digest: $digest');

    var sum = 0;
    while(true){
      sum  = digest + i;
      if((sum % _modulo == _moduloResult) || sum >= _maxSum){
        break;
      }

      i += 1;
    }

    //debugPrint('sum: $sum');
    return sum;
  }

  static String _generateMd5(String data) {
    var content = new convert.Utf8Encoder().convert(data);
    var md5 = crypto.md5;
    var digest = md5.convert(content);

    return digest.toString();
  }
}
