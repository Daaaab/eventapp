import 'package:event_app/infrastructure/security/user_id/user_id.dart';
import 'package:flutter/foundation.dart';
import 'package:unique_identifier/unique_identifier.dart';

class UserIdImpl implements UserId{

  Future<String> getUUID() async {
    String identifier;
    try {
      identifier = await UniqueIdentifier.serial;
    } catch(e) {
      //todo zrobić co innego - jednorazowy uuid
      debugPrint('UserId error: ${e.toString()}');
      identifier = null;
    }

    debugPrint("Generated user id: $identifier");
    return identifier;
  }
}