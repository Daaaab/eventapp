abstract class UserId{
  Future<String> getUUID();
}