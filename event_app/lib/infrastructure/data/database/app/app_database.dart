import 'dart:async';
import 'dart:io';

import 'package:event_app/common/utils/db_column.dart';
import 'package:event_app/model/app/rating.dart';
import 'package:event_app/model/db/RatingEntity.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/foundation.dart';

class AppDatabase {
  static const int _version = 3;
  static const String dbName = 'event_app_db.db';

  static Database _database;

  Future<Database> get database async {
    if (_database == null) {
      _database = await _initializeDatabase();
    }
    return _database;
  }

  Future<Database> _initializeDatabase() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, dbName);

    if(await Directory(directory.path).exists()){
    }else{
      try{
        await Directory(directory.path).create(recursive: true);
      }catch(e){
        debugPrint(e);
      }
    }

    var netCacheDb = await openDatabase(path, version: _version, onCreate: _createDb, onUpgrade: _upgradeDb).catchError((e) => debugPrint(e.toString()));
    return netCacheDb;
  }

  void _upgradeDb(Database db, int oldVersion, int newVersion) async {
    await db.execute(_ConfigStructure.dropConfigTableQuery);
    await db.execute(_FavouritesStructure.dropDavsTable);
    await db.execute(_RatingStructure.dropRatingTable);
    await db.execute(_FilterStructure.dropFiltersTable);
    _createDb(db, newVersion);
  }

  void _createDb(Database db, int newVersion) async {
    await db.execute(_ConfigStructure.createConfigTableQuery);
    await db.execute(_FavouritesStructure.createFavsTable);
    await db.execute(_RatingStructure.createRatingTable);
    await db.execute(_FilterStructure.createFiltersTable);
  }

/////////////////////////////////////////////////
  Future<List<RatingEntity>> getRatings() async {
    var db = await database;

    var queryResult = await db.query(_RatingStructure.ratingTable);

    if (queryResult.isNotEmpty) {
      List<RatingEntity> result = List();
      queryResult.forEach((record) {
        var eventId = record[_RatingStructure.ratingIdCol.name] as String;
        var rate = record[_RatingStructure.ratingCol.name] as int;
        var comment = record[_RatingStructure.commentCol.name] as String;
        var status = record[_RatingStructure.statusCol.name] as String;
        result.add(RatingEntity(eventId, comment, rate, status));
      });

      return result;
    } else {
      return List<RatingEntity>();
    }
  }

  Future<int> insertRating(RatingEntity rating) async {
    var db = await this.database;

    Map<String, dynamic> ratingMap = {
      _RatingStructure.ratingIdCol.name: rating.eventId,
      _RatingStructure.ratingCol.name: rating.rate,
      _RatingStructure.commentCol.name: rating.comment,
      _RatingStructure.statusCol.name: rating.status,
    };

    var result = await db.insert(_RatingStructure.ratingTable, ratingMap, conflictAlgorithm: ConflictAlgorithm.replace);
    return result;
  }

  Future<int> removeBlocked() async {
    var db = await this.database;

    return db.delete(_RatingStructure.ratingTable, where: "${_RatingStructure.statusCol.name} = '${Rating.disabled}'");
  }

/////////////////////////////////////////////////
  Future<List<String>> getFilters() async {
    var db = await this.database;

    var queryResult = await db.query(_FilterStructure.filtersTable);

    if (queryResult.isNotEmpty) {
      return queryResult.map((record) => record[_FilterStructure.filtersIdsCol.name] as String).toList();
    } else {
      return List<String>();
    }
  }

  Future<int> removeFilter(String filterId) async {
    var db = await this.database;

    var result = await db.delete(_FilterStructure.filtersTable, where: "${_FilterStructure.filtersIdsCol.name} = '$filterId'");

    return result;
  }

  Future<int> insertFilter(String filterId) async {
    var db = await this.database;

    Map<String, dynamic> filtersMap = {_FilterStructure.filtersIdsCol.name: filterId};

    var result = await db.insert(_FilterStructure.filtersTable, filtersMap, conflictAlgorithm: ConflictAlgorithm.abort);

    return result;
  }

/////////////////////////////////////////////////
  Future<List<String>> getFavs() async {
    var db = await this.database;

    var queryResult = await db.query(_FavouritesStructure.favTable);

    if (queryResult.isNotEmpty) {
      return queryResult.map((record) => record[_FavouritesStructure.favIdsCol.name] as String).toList();
    } else {
      return List<String>();
    }
  }

  Future<int> removeFav(String dayId) async {
    var db = await this.database;

    var result = await db.delete(_FavouritesStructure.favTable, where: "${_FavouritesStructure.favIdsCol.name} = '$dayId'");

    return result;
  }

  Future<int> insertFav(String dayId) async {
    var db = await this.database;

    Map<String, dynamic> favsMap = {_FavouritesStructure.favIdsCol.name: dayId};

    var result = await db.insert(_FavouritesStructure.favTable, favsMap, conflictAlgorithm: ConflictAlgorithm.abort);

    return result;
  }

  /////////////////////////////////////////////////
  Future<int> insertIsFirstRun(bool isFirstRun) async => _insertConfigValue(_ConfigStructure.configFieldIsFirstRun, isFirstRun.toString());

  Future<bool> getIsFirstRun() async => _getConfigValue(_ConfigStructure.configFieldIsFirstRun).then((result) {
        if (result == null) {
          return true;
        }
        return bool.fromEnvironment(result);
      });
  /////////////////////////////////////////////////
  Future<int> insertUserId(String userId) async => _insertConfigValue(_ConfigStructure.configFieldUserId, userId);

  Future<String> getUserId() async => _getConfigValue(_ConfigStructure.configFieldUserId);
/////////////////////////////////////////////////
  Future<int> insertFcmToken(String token) async => _insertConfigValue(_ConfigStructure.configFieldFcmToken, token);

  Future<String> getFcmToken() async => _getConfigValue(_ConfigStructure.configFieldFcmToken);
/////////////////////////////////////////////////

  Future<String> _getConfigValue(String fieldName) async {
    var db = await database;

    var queryResult = await db.query(_ConfigStructure.configTable,
        columns: [_ConfigStructure.configValueCol.name], where: "${_ConfigStructure.configKeyCol.name} = '$fieldName'");

    if (queryResult.isNotEmpty) {
      var result = queryResult[0][_ConfigStructure.configValueCol.name] as String;
      return result;
    } else {
      return null;
    }
  }

  Future<int> _insertConfigValue(String fieldName, String value) async {
    var db = await this.database;

    Map<String, dynamic> tokenMap = {_ConfigStructure.configKeyCol.name: fieldName, _ConfigStructure.configValueCol.name: value};

    var result = await db.insert(_ConfigStructure.configTable, tokenMap, conflictAlgorithm: ConflictAlgorithm.replace);
    return result;
  }
}

class _FilterStructure {
  static final String filtersTable = "filtrs";
  static final DbColumn<String, String> filtersIdsCol = DbColumn("filtrId", "TEXT PRIMARY KEY");

  static final String createFiltersTable = "CREATE TABLE $filtersTable( ${filtersIdsCol.name} ${filtersIdsCol.type} )";
  static final String dropFiltersTable = "DROP TABLE IF EXISTS $filtersTable";
}

class _RatingStructure {
  static final String ratingTable = "ratings";
  static final DbColumn<String, String> ratingIdCol = DbColumn("eventId", "TEXT PRIMARY KEY");
  static final DbColumn<String, String> ratingCol = DbColumn("rating", "INTEGER");
  static final DbColumn<String, String> commentCol = DbColumn("comment", "TEXT");
  static final DbColumn<String, String> statusCol = DbColumn("status", "TEXT");

  static final String createRatingTable = "CREATE TABLE $ratingTable("
      "${ratingIdCol.name} ${ratingIdCol.type},"
      "${ratingCol.name} ${ratingCol.type},"
      "${commentCol.name} ${commentCol.type},"
      "${statusCol.name} ${statusCol.type}"
      ")";

  static final String dropRatingTable = "DROP TABLE IF EXISTS $ratingTable";
}

class _FavouritesStructure {
  static final String favTable = "favs";
  static final DbColumn<String, String> favIdsCol = DbColumn("eventId", "TEXT PRIMARY KEY");

  static final String createFavsTable = "CREATE TABLE $favTable( ${favIdsCol.name} ${favIdsCol.type} )";
  static final String dropDavsTable = "DROP TABLE IF EXISTS $favTable";
}

class _ConfigStructure {
  static final String configTable = "config";
  static final DbColumn<String, String> configKeyCol = DbColumn("config_key", "TEXT PRIMARY KEY");
  static final DbColumn<String, String> configValueCol = DbColumn("config_value", "TEXT");

  static final String configFieldFcmToken = 'fcm_token';
  static final String configFieldUserId = 'uuid';
  static final String configFieldIsFirstRun = 'is_first_run';

  static final String dropConfigTableQuery = "DROP TABLE IF EXISTS $configTable";
  static final String createConfigTableQuery = "CREATE TABLE $configTable( "
      "${configKeyCol.name} ${configKeyCol.type},"
      "${configValueCol.name} ${configValueCol.type}"
      ") ";
}
