import 'dart:io';

import 'package:event_app/common/utils/db_column.dart';
import 'package:flutter/foundation.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class NetworkCache {
  static const int _version = 3;
  static const String dbName = 'net_cache.db';

  static Database _database;

  Future<Database> get database async {
    if (_database == null) {
      _database = await _initializeDatabase();
    }
    return _database;
  }

  Future<Database> _initializeDatabase() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, dbName);

    if(await Directory(directory.path).exists()){
    }else{
      try{
        await Directory(directory.path).create(recursive: true);
      }catch(e){
        debugPrint(e);
      }
    }

    var netCacheDb = await openDatabase(path, version: _version, onCreate: _createDb, onUpgrade: _upgradeDb).catchError((e) => debugPrint(e.toString()));
    return netCacheDb;
  }

  void _upgradeDb(Database db, int oldVersion, int newVersion) async {
    await db.execute(_NetworkCacheStructure.dropNetCacheTableQuery);
    _createDb(db, newVersion);
  }

  void _createDb(Database db, int newVersion) async {
    await db.execute(_NetworkCacheStructure.createNetCacheQuery);
  }

  /////////////////////////////////////////////////
  Future<int> insertModules(String modules) async => _insertCacheValue(_NetworkCacheStructure.cacheFieldModules, modules);

  Future<String> getModules() async => _getCacheValue(_NetworkCacheStructure.cacheFieldModules);
  /////////////////////////////////////////////////
  Future<int> insertAgenda(String agenda) async => _insertCacheValue(_NetworkCacheStructure.cacheFieldAgenda, agenda);

  Future<String> getAgenda() async => _getCacheValue(_NetworkCacheStructure.cacheFieldAgenda);
  /////////////////////////////////////////////////

  Future<String> _getCacheValue(String fieldName) async {
    var db = await this.database;

    var queryResult = await db.query(_NetworkCacheStructure.cacheTable,
        columns: [_NetworkCacheStructure.valueCol.name], where: "${_NetworkCacheStructure.idCol.name} = '$fieldName'");

    if (queryResult.isNotEmpty) {
      var result = queryResult[0][_NetworkCacheStructure.valueCol.name] as String;
      return result;
    } else {
      return null;
    }
  }

  Future<int> _insertCacheValue(String fieldName, String value) async {
    var db = await this.database;

    Map<String, dynamic> tokenMap = {_NetworkCacheStructure.idCol.name: fieldName, _NetworkCacheStructure.valueCol.name: value};

    var result = await db.insert(_NetworkCacheStructure.cacheTable, tokenMap, conflictAlgorithm: ConflictAlgorithm.replace);
    return result;
  }
}

class _NetworkCacheStructure {
  static final String cacheTable = "NetCache";
  static final DbColumn<String, String> idCol = DbColumn("cache_id", "TEXT PRIMARY KEY");
  static final DbColumn<String, String> valueCol = DbColumn("cache_value", "TEXT");

  static final String cacheFieldAgenda = 'agenda';
  static final String cacheFieldModules = 'modules';

  static final String dropNetCacheTableQuery = "DROP TABLE IF EXISTS $cacheTable";
  static final String createNetCacheQuery = "CREATE TABLE $cacheTable( "
      "${idCol.name} ${idCol.type},"
      "${valueCol.name} ${valueCol.type}"
      ") ";
}
