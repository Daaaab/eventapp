import 'package:event_app/infrastructure/data/net/network.dart';
import 'package:event_app/model/net/day_doc.dart';
import 'package:event_app/model/net/event_doc.dart';
import 'package:http/http.dart';
import 'package:rxdart/rxdart.dart';

class MockNetwork extends Network{

  BehaviorSubject<List<EventDoc>> _eventsStream;
  BehaviorSubject<List<DayDoc>> _daysStream;

  MockNetwork(){

//    List<EventDoc> initialEvents = [
//      EventDoc("Mock Event 1", "a"),
//      EventDoc("Mock Event 2", "b"),
//      EventDoc("Mock Event 3", "a"),
//      EventDoc("Mock Event 4", "a"),
//      EventDoc("Mock Event 5", "b"),
//    ];
//
//    List<DayDoc> initialDays = [
//      DayDoc("Mock Day 1", 0, "a"),
//      DayDoc("Mock Day 2", 1, "b")
//    ];
//
//    _eventsStream = BehaviorSubject(seedValue: initialEvents);
//    _daysStream = BehaviorSubject(seedValue:  initialDays);

    _eventsStream = BehaviorSubject();
    _daysStream = BehaviorSubject();
  }

  @override
  Future<String> get getAgendaFuture => null;

  dispose(){
    _eventsStream.close();
    _daysStream.close();
  }

  @override
  Future<Response> sendToken(String token) {
    return null;
  }

  @override
  Future<Response> sendRating(String uuid, String eventId, String comment, int rate, int version) {
    return null;
  }

  @override
  // TODO: implement getModulesFuture
  Future<String> get getModulesFuture => null;
}