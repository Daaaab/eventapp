import 'package:http/http.dart' as http;

abstract class Network{

    Future<String> get getModulesFuture;

    Future<String> get getAgendaFuture;

    Future<http.Response> sendToken(String token);

    Future<http.Response> sendRating(String uuid, String eventId, String comment, int rate, int version);
}