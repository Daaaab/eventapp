import 'dart:async';

import 'package:event_app/infrastructure/data/database/cache/network_cache.dart';
import 'package:event_app/infrastructure/data/net/backend/rating_send_queue.dart';
import 'package:event_app/infrastructure/data/net/fcm_service.dart';
import 'package:event_app/infrastructure/data/net/backend/network_handler.dart';
import 'package:event_app/infrastructure/data/net/rest/rest_network.dart';
import 'package:event_app/model/app/rating.dart';
import 'package:event_app/model/net/agenda_doc.dart';
import 'package:event_app/model/net/module_doc.dart';
import 'package:event_app/model/net/rating_doc.dart';

//managers all network stuff
class Backend {
  NetworkHandler _networkHandler;
  RatingSendQueue _ratingQueue;
  FCMService _fcmService;

  Backend() {
    this._networkHandler = NetworkHandler(RestNetwork(), NetworkCache());
    this._ratingQueue = RatingSendQueue(_networkHandler);
    this._fcmService = FCMService();
  }

  Stream<Map<String, dynamic>> get pushMessagesStream => _fcmService.pushMessagesStream;

  Stream<Rating> setRatingStream(Stream<List<RatingDoc>> ratings) => _ratingQueue.setRatingStream(ratings);

  Future<void> sendRating(String uuid, String eventId, String comment, int rate, int version) =>
      _networkHandler.sendRating(uuid, eventId, comment, rate, version);

  Future<void> sendToken(String token) => _networkHandler.sendToken(token);

  Stream<AgendaDoc> getAgenda() => _networkHandler.getAgenda();

  Stream<List<ModuleDoc>> getModules() => _networkHandler.getModules();

  void dispose() {
    _ratingQueue.dispose();
  }
}
