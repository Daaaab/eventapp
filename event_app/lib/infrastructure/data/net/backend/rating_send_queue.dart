import 'dart:async';

import 'package:event_app/infrastructure/data/net/backend/network_handler.dart';
import 'package:event_app/infrastructure/security/rating_signature.dart';
import 'package:event_app/model/app/rating.dart';
import 'package:event_app/model/net/rating_doc.dart';
import 'package:flutter/foundation.dart';
import 'package:rxdart/subjects.dart';

//todo stream dla błędów
//todo Zaprząc connectivity w repo do ponownego wysyłania ocen
//todo wyświetlić błędy
class RatingSendQueue {
  NetworkHandler _network;
  PublishSubject<Rating> _statusUpdateStream;
  RatingSendQueue(this._network);



  Stream<Rating> setRatingStream(Stream<List<RatingDoc>> ratings) {
    _subscribeToUnsentRatings(ratings);
    _statusUpdateStream = PublishSubject();


    return _statusUpdateStream;
  }

  void _subscribeToUnsentRatings(Stream<List<RatingDoc>> ratings) => ratings
      .map((ratingsList) => ratingsList.where((rating) => rating.status == Rating.unsent || rating.status == Rating.resend).toList())
      .where((ratingsList) => ratingsList.isNotEmpty)
      .listen((unsentRatings) => _sendRatings(unsentRatings));

  Iterable<Rating> _filterRatings(List<Rating> previous, List<Rating> current) sync* {
    debugPrint('queueu - filtering ratings from size: ${previous.length}');
    for (var currentRate in current) {
      if (!previous.contains(currentRate)) {
        yield currentRate;
      }
    }
  }

  void _mockSend(List<RatingDoc> ratingsList) {
    ratingsList.forEach((rating) {
      if(rating.status == Rating.resend){
        _sendStatusUpdate(rating, Rating.resending);
      }else{
        _sendStatusUpdate(rating, Rating.pending);
      }


      final signature = RatingSignature.calculate(rating.eventId, rating.uuid, rating.comment, rating.rate);

      debugPrint('RatingSendQueue: seding: ${rating.eventId} ${rating.rate} ${rating.uuid} $signature ${rating.comment}');

      Future.delayed(Duration(milliseconds: 5500), ()=> _sendStatusUpdate(rating, Rating.sent)).catchError((err) {
        debugPrint('RatingSendQueue error: ${err.toString()}');
        _sendStatusUpdate(rating, Rating.failed);
      });
    });
  }

  void _sendRatings(List<RatingDoc> ratingsList) {
    ratingsList.forEach((rating) {
      if(rating.status == Rating.resend){
        _sendStatusUpdate(rating, Rating.resending);
      }else{
        _sendStatusUpdate(rating, Rating.pending);
      }

      final signature = RatingSignature.calculate(rating.eventId, rating.uuid, rating.comment, rating.rate);
      //todo obsługa faili
      _network
          .sendRating(rating.uuid, rating.eventId, rating.comment, rating.rate, signature)
          .then((_) {
        _sendStatusUpdate(rating, Rating.sent);
      })
          .catchError((err) {
        debugPrint('RatingSendQueue error: ${err.toString()}');
        if(err.toString().contains('Rating disabled')){
          debugPrint('Rating Blocked');
          _sendStatusUpdate(rating, Rating.disabled);
        }else{
          debugPrint('Rating failed');
          _sendStatusUpdate(rating, Rating.failed);
        }
      });
    });
  }

  void _sendStatusUpdate(RatingDoc rating, String newStatus) => _statusUpdateStream.add(Rating.withUpdatedStatus(rating, newStatus));

  //todo ogarnąć disposy w całej apce
  void dispose() {
    _statusUpdateStream.close();

  }
}
