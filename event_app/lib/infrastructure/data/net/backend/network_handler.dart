import 'dart:async';
import 'dart:convert';

import 'package:event_app/infrastructure/data/database/cache/network_cache.dart';
import 'package:event_app/infrastructure/data/net/network.dart';
import 'package:event_app/model/net/agenda_doc.dart';
import 'package:event_app/model/net/module_doc.dart';
import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart' as rx;

// Handles networks calls
// Caches responses
// Pushes data further
class NetworkHandler {
  Network _network;
  NetworkCache _cache;

  NetworkHandler(this._network, this._cache);

  Future<void> sendRating(String uuid, String eventId, String comment, int rate, int version) {
    return _network.sendRating(uuid, eventId, comment, rate, version).then((response) {

      debugPrint('Backend: rating send response : ${response.statusCode} - ${response.body}');
      if(response.statusCode == 405){
        throw Exception('Rating disabled');
      }
    }).then((response){
      return;
    }).catchError((err) {
      //todo przechwytywanie wyjątków
      debugPrint('Backend: error while sending rating: $err');
      throw Exception('Błąd połączenia');
    });
  }

  Future<void> sendToken(String token) {
    return _network.sendToken(token).then((_) {
      return;
    }).catchError((err) {
      debugPrint('Backend: error while sending token: $err');
      return;
    });
  }

  Stream<List<ModuleDoc>> getModules(){
    debugPrint("Backend: refreshing Modules");

    return rx.Observable.fromFuture(_network.getModulesFuture).doOnData((modulesResponseBody) {
      _cache.insertModules(modulesResponseBody).catchError((err) => debugPrint("Backend: Error while inserting modules response to DB: $err"));
    }).onErrorResume((err) {
      debugPrint('Backend: Connection error - retrieving from cache: $err');
      return _cache.getModules().asStream();
    }).map((modulesResponseBody) {
      if (modulesResponseBody == null || modulesResponseBody.isEmpty) {
        return List<ModuleDoc>();
      } else {

        var decoded = json.decode(modulesResponseBody);

        var decodedList = decoded['modules'] as List<dynamic>;
        var mappedEventsList = decodedList.map((json) => ModuleDoc.fromJSON(json)).toList();

        return mappedEventsList;
      }
    });
  }
  
  Stream<AgendaDoc> getAgenda() {
    debugPrint("Backend: refreshing Agenda");

    return rx.Observable.fromFuture(_network.getAgendaFuture).doOnData((agendaResponseBody) {
      _cache.insertAgenda(agendaResponseBody).catchError((err) => debugPrint("Backend: Error while inserting agenda response to DB: $err"));
    }).onErrorResume((err) {
      debugPrint('Backend: Connection error - retrieving from cache: $err');
      return _cache.getAgenda().asStream();
    }).map((agendaResponseBody) {
      if (agendaResponseBody == null || agendaResponseBody.isEmpty) {
        return AgendaDoc.empty();
      } else {
        var decoded = json.decode(agendaResponseBody);
        return AgendaDoc.fromJSON(decoded);
      }
    });
  }
}
