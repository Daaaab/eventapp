import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';

class FCMService {
  FirebaseMessaging _messaging = FirebaseMessaging();

  PublishSubject<Map<String, dynamic>> _pushMessagesStream;

  FCMService() {
    _pushMessagesStream = PublishSubject();
    if (Platform.isIOS) {
      _messaging.requestNotificationPermissions(IosNotificationSettings(sound: true, badge: true, alert: true));
      _messaging.onIosSettingsRegistered.listen((IosNotificationSettings settings) {
        print("Settings registered: $settings");
      });
    }

    _registerListeners();
  }

  void _registerListeners() {
    debugPrint('registering listners');
    _messaging.configure(
      onMessage: _onMessage,
      onResume: _onResume,
      onLaunch: _onLaunch,
    );
  }

  Stream<Map<String, dynamic>> get pushMessagesStream => _pushMessagesStream;

  Future<dynamic> _onMessage(Map<String, dynamic> message) async {
    debugPrint('DOSZŁA WIADOMOŚC');
    debugPrint('$message');
    _pushMessagesStream.add(message);
  }

  Future<dynamic> _onResume(Map<String, dynamic> message) async {
    debugPrint('DOSZŁA WIADOMOŚC2');
    debugPrint('$message');
    _pushMessagesStream.add(message);
  }

  Future<dynamic> _onLaunch(Map<String, dynamic> message) async {
    debugPrint('DOSZŁA WIADOMOŚC3');
    debugPrint('$message');
    _pushMessagesStream.add(message);
  }

  void dispose() {
    _pushMessagesStream.close();
  }
}
