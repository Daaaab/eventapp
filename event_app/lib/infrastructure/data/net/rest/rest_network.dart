import 'dart:io';

import 'package:event_app/common/config/rest_config.dart';
import 'package:event_app/infrastructure/data/net/network.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/foundation.dart';

// Provides network calls
class RestNetwork extends Network {
  static const getAgendaUrl = RestConfig.backendUrl + RestConfig.getAgenda;
  static const getModulesUrl = RestConfig.backendUrl + RestConfig.getModules;
  static const addFcmTokenUrl = RestConfig.backendUrl + RestConfig.addFcmToken;
  static const setRatingUrl = RestConfig.backendUrl + RestConfig.setRating;


  @override
  Future<String> get getAgendaFuture => _getAgendaResponse();

  @override
  Future<String> get getModulesFuture => _getModulesResponse();

  @override
  Future<http.Response> sendRating(String uuid, String eventId, String comment, int rate, int version) {
    debugPrint("Sending rating");
    return http.post(
      setRatingUrl,
      body: {
        'id': uuid,
        'event': eventId,
        'rating': rate.toString(),
        'comment': comment,
        'version': version.toString(),
      },
    );
  }

  @override
  Future<http.Response> sendToken(String token) {
    debugPrint("Sending token: " + token);
    return http.post(
      addFcmTokenUrl,
      body: {
        'fcmToken': token,
      },
    ).then((response) {
      debugPrint("Send token response: ${response.statusCode} - ${response.body}");
    });
  }

  Future<String> _getAgendaResponse() => _getAgendaRequest().then((response) {
        var responseString = response.body;
        debugPrint("Agenda Response: ${response.statusCode} - $responseString");
        return responseString;
      });

  Future<String> _getModulesResponse() => _getModulesRequest().then((response) {
    var responseString = response.body;
    debugPrint("Modules Response: ${response.statusCode} - $responseString");
    return responseString;
  });

  Future<http.Response> _getAgendaRequest() {
    return http.get(getAgendaUrl);
  }

  Future<http.Response> _getModulesRequest() {
    return http.get(getModulesUrl);
  }
}
