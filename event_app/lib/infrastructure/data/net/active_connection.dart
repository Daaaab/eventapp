import 'dart:async';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:rxdart/rxdart.dart';

class ActiveConnection {
  StreamSubscription _connectivitySub;

  BehaviorSubject _connectivityStatus;

  ActiveConnection() {
    _connectivityStatus = BehaviorSubject<ConnectivityResult>.seeded(ConnectivityResult.none);

    Connectivity().onConnectivityChanged.listen((connectivityResult) => _connectivityStatus.value = connectivityResult);
  }

  Stream<ActiveConnectionStatus> get statusStream => Observable.combineLatest2(
        _connectivityStatus,
        Stream.periodic(Duration(seconds: 3), (tick) => tick),
        (connectivityResult, tick) {
          return connectivityResult != ConnectivityResult.none ? ActiveConnectionStatus.connected : ActiveConnectionStatus.noConnection;
        },
      );

  void dispose() {
    _connectivityStatus.close();
    _connectivitySub.cancel();
  }
}

enum ActiveConnectionStatus {
  noConnection,
  connected,
}
