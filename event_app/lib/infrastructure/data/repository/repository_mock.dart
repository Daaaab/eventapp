import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:event_app/common/di/kiwi_injector.dart';
import 'package:event_app/infrastructure/data/database/app/app_database.dart';
import 'package:event_app/infrastructure/data/net/active_connection.dart';
import 'package:event_app/infrastructure/data/net/backend/backend.dart';
import 'package:event_app/infrastructure/data/repository/repository.dart';
import 'package:event_app/model/app/day.dart';
import 'package:event_app/model/app/event.dart';
import 'package:event_app/model/app/group.dart';
import 'package:event_app/model/app/module.dart';
import 'package:event_app/model/app/rating.dart';
import 'package:event_app/model/app/speaker.dart';
import 'package:event_app/model/db/RatingEntity.dart';
import 'package:event_app/model/mapping/day_map.dart';
import 'package:event_app/model/mapping/event_map.dart';
import 'package:event_app/model/mapping/group_map.dart';
import 'package:event_app/model/mapping/speaker_map.dart';
import 'package:event_app/model/net/agenda_doc.dart';
import 'package:event_app/model/net/module_doc.dart';
import 'package:event_app/model/net/rating_doc.dart';
import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';

//provides interface for app data
// should be disposed on app end
// all data should flow through this

class RepositoryMock implements Repository {
  ActiveConnection _connectionStatus = ActiveConnection();
  AppDatabase _appDb = AppDatabase();
  Backend _backend;

  BehaviorSubject<List<Event>> _eventsStream;
  BehaviorSubject<List<Day>> _daysStream;
  BehaviorSubject<List<Speaker>> _speakersStream;
  BehaviorSubject<List<Group>> _groupsStream;
  BehaviorSubject<List<String>> _favsStream;
  BehaviorSubject<List<String>> _filtersStream;
  BehaviorSubject<List<Rating>> _ratingStream;
  BehaviorSubject<List<RatingDoc>> _ratingSendStream;
  BehaviorSubject<List<Module>> _modulesStream;

  StreamSubscription _connectivitySub;

  RepositoryMock() {
    _backend = Backend();

    _eventsStream = BehaviorSubject();
    _daysStream = BehaviorSubject();
    _speakersStream = BehaviorSubject();
    _groupsStream = BehaviorSubject();
    _favsStream = BehaviorSubject();
    _ratingStream = BehaviorSubject();
    _ratingSendStream = BehaviorSubject();
    _modulesStream = BehaviorSubject();
    _filtersStream = BehaviorSubject();

    _backend.setRatingStream(_ratingSendStream).listen(_updateRatingsStatus);

    _backend.pushMessagesStream.listen((message) {
      downloadAgenda.listen((agendaDoc) {
        updateAgenda(agendaDoc);
      }, onError: (err) {
        debugPrint('RestNetwork: GetAgenda request error: $err');
      });
    });

    _ratingStream.flatMap((ratingList) {
      return uuid.asStream().map((uuid) {
        List<RatingDoc> ratingSendList = List();
        ratingList.forEach((rating) {
          ratingSendList.add(RatingDoc.fromRating(rating, uuid));
        });
        return ratingSendList;
      });
    }).listen((ratingSendList) => _ratingSendStream.value = ratingSendList);
    _getNotSend().then((ratings) => _resendPending(ratings));
    _requestFavsUpdate();
    _requestRatingsUpdate();
    _subscribeConnectivity();
  }

  void _subscribeConnectivity() {
    _connectivitySub = _connectionStatus.statusStream.listen((result) {
      debugPrint('Connection status: $result}');
      if (result != ActiveConnectionStatus.noConnection) {
        _getFailedRates().then((ratings){
          if(ratings != null && ratings.isNotEmpty){
            _resendPending(ratings);
          }
        });
      }
    });
  }

  @override
  Stream<List<Day>> get daysList => _daysStream.stream;

  @override
  Stream<AgendaDoc> get downloadAgenda => _backend.getAgenda();

  @override
  Stream<List<ModuleDoc>> get downloadModules => _backend.getModules();

  @override
  Stream<List<Event>> get eventsList => _eventsStream.stream;

  @override
  Stream<List<String>> get favsList => _favsStream;

  @override
  Future<List<String>> get filtersList => _appDb.getFilters();

  @override
  Future<String> get fcmToken => _appDb.getFcmToken();

  @override
  Stream<List<Group>> get groupsList => _groupsStream.stream;

  @override
  Future<bool> get isFirstRun => _appDb.getIsFirstRun();

  @override
  Stream<List<Module>> get modulesList => _modulesStream.stream;

  @override
  Stream<List<Rating>> get ratingList => _ratingStream.stream;

  @override
  Stream<List<Speaker>> get speakersList => _speakersStream.stream;

  @override
  Future<String> get uuid => _appDb.getUserId();

  @override
  void addRating(Rating rating) => _appDb.insertRating(RatingEntity.fromRating(rating)).then((_) => _requestRatingsUpdate());

  @override
  void removeBlocked() => _appDb.removeBlocked().then((_) => _requestRatingsUpdate());

  @override
  dispose() {
    debugPrint('Disposing repository');
    _modulesStream.close();
    _ratingStream.close();
    _eventsStream.close();
    _daysStream.close();
    _speakersStream.close();
    _groupsStream.close();
    _favsStream.close();
    _ratingSendStream.close();
    _backend.dispose();
    _filtersStream.close();
    _connectivitySub.cancel();
    _connectionStatus.dispose();
  }

  @override
  setFilter(String filterId, bool isEnabled) {
    Future<int> result;

    if (isEnabled) {
      result = _appDb.insertFilter(filterId);
    } else {
      result = _appDb.removeFilter(filterId);
    }

    result.then((_) => _requestFavsUpdate());
  }

  @override
  setFav(String dayId, bool isFav) {
    Future<int> result;

    if (isFav) {
      result = _appDb.insertFav(dayId);
    } else {
      result = _appDb.removeFav(dayId);
    }

    result.then((_) => _requestFavsUpdate());
  }

  @override
  setIsFirstRun(bool isFirstRun) => _appDb.insertIsFirstRun(isFirstRun);

  @override
  setToken(String token) => _backend.sendToken(token).then((_) => _appDb.insertFcmToken(token));

  @override
  setUUID(String uuid) => _appDb.insertUserId(uuid);

  @override
  updateAgenda(AgendaDoc agenda) {
    updateEvents(agenda.events.map((event) => EventMap.mapDocToEvent(event)).toList());
    updateDays(agenda.days.map((day) => DayMap.mapDocToDay(day)).toList());
    updateSpeakers(agenda.speakers.map((speaker) => SpeakerMap.mapDocToEvent(speaker)).toList());
    updateGroups(agenda.groups.map((group) => GroupMap.mapDocToGroup(group)).toList());
  }

  @override
  updateDays(List<Day> daysList) {
    debugPrint('updateDays: ${daysList.length}');
    _daysStream.value = daysList;
  }

  @override
  updateEvents(List<Event> eventsList) {
    debugPrint('updateEvents: ${eventsList.length}');
    _eventsStream.value = eventsList;
  }

  @override
  updateFavs(List<String> favsList) {
    debugPrint('updateFavs: ${favsList.length}');
    _favsStream.value = favsList;
  }

  @override
  updateGroups(List<Group> groupsList) {
    debugPrint('updateGroups: ${groupsList.length}');
    _groupsStream.value = groupsList;
  }

  @override
  updateModules(List<ModuleDoc> modulesList) {
    debugPrint('updateModules: ${modulesList.length}');
    _modulesStream.value = modulesList.map((moduleDoc) => Module.fromModuleDoc(moduleDoc)).toList();
  }

  @override
  updateRatings(List<Rating> ratingsList) {
    //debugPrint('updateRatings: ${ratingsList.length}');
    _ratingStream.value = ratingsList;
  }

  @override
  updateSpeakers(List<Speaker> speakersList) {
    debugPrint('updateSpeakers: ${speakersList.length}');
    _speakersStream.value = speakersList;
  }

  void _requestFavsUpdate() => _appDb.getFavs().then(updateFavs);

  void _requestRatingsUpdate() => _appDb.getRatings().then((ratings) {
        return ratings.map((rating) => Rating.fromRatingEntity(rating)).toList();
      }).then(updateRatings);

  Future<List<Rating>> _getNotSend() => _appDb.getRatings().then((ratings) {
    debugPrint('Resend not send');
    return ratings.map((rating) => Rating.fromRatingEntity(rating)).toList();
  }).then((ratings) => ratings.where((rating) => (rating.status == Rating.failed || rating.status == Rating.pending || rating.status == Rating.resend || rating.status == Rating.resending)).toList());

  Future<List<Rating>> _getFailedRates() => _appDb.getRatings().then((ratings) {
        debugPrint('Resend failed');
        return ratings.map((rating) => Rating.fromRatingEntity(rating)).toList();
      }).then((ratings) => ratings.where((rating) => (rating.status == Rating.failed)).toList());

//  void _resendPending() => _getPendingAndFailedRates().then((ratings) {
//        ratings.forEach((rating) => _appDb.insertRating(RatingEntity.fromRating(Rating.updateStatus(rating, Rating.unsent))));
//        debugPrint('Resending ${ratings.length}');
//      }).then((_) => _requestRatingsUpdate());

  void _resendPending(List<Rating> ratings){
    debugPrint('Resending ${ratings.length}');
    ratings.forEach((rating) => _appDb.insertRating(RatingEntity.fromRating(Rating.updateStatus(rating, Rating.resend))));
    _requestRatingsUpdate();
  }

  void _updateRatingsStatus(Rating updatedRating) => addRating(updatedRating);
}
