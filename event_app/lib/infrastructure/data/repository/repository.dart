import 'dart:async';

import 'package:event_app/model/app/day.dart';
import 'package:event_app/model/app/event.dart';
import 'package:event_app/model/app/group.dart';
import 'package:event_app/model/app/module.dart';
import 'package:event_app/model/app/rating.dart';
import 'package:event_app/model/app/speaker.dart';
import 'package:event_app/model/net/agenda_doc.dart';
import 'package:event_app/model/net/module_doc.dart';
import 'package:rxdart/rxdart.dart';

abstract class Repository {

  updateEvents(List<Event> eventsList);
  updateDays(List<Day> daysList);
  updateSpeakers(List<Speaker> speakersList);
  updateGroups(List<Group> groupsList);
  updateFavs(List<String> favsList);
  setFav(String dayId, bool isFav);
  void addRating(Rating rating);
  updateRatings(List<Rating> ratingsList);
  updateAgenda(AgendaDoc agenda);
  setToken(String token);
  setUUID(String uuid);
  updateModules(List<ModuleDoc> modulesList);
  setIsFirstRun(bool isFirstRun);
  setFilter(String filterId, bool isEnabled);
  void removeBlocked();

  Stream<List<Rating>> get ratingList;
  Stream<List<Group>> get groupsList;
  Stream<List<Event>> get eventsList;
  Stream<List<Day>> get daysList;
  Stream<List<Speaker>> get speakersList;
  Stream<List<String>> get favsList;
  Stream<AgendaDoc> get downloadAgenda;
  Stream<List<ModuleDoc>> get downloadModules;
  Future<String> get fcmToken;
  Future<String> get uuid;
  Stream<List<Module>> get modulesList;
  Future<bool> get isFirstRun;
  Future<List<String>> get filtersList;

  dispose();
}