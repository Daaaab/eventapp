import 'dart:async';

import 'package:event_app/bloc/modules/gps_module_bloc.dart';
import 'package:event_app/common/architecture/bloc_provider.dart';
import 'package:event_app/common/design/colors/design_colors.dart';
import 'package:event_app/common/design/margin.dart';
import 'package:event_app/common/design/text_styles.dart';
import 'package:event_app/common/di/kiwi_injector.dart';
import 'package:event_app/model/app/gps_module.dart';
import 'package:event_app/model/app/location_point.dart';
import 'package:event_app/pages/common/separator_widget.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart' as lurl;
import 'dart:io' show Platform;

class NavigationPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _NavigationPageState();
}

class _NavigationPageState extends State<NavigationPage> {
  final _defaultZoom = 15.0;
  final _itemsToDisplay = 2;
  final List<GlobalKey> _itemKeys = List();
  final List<Marker> _markers = List();
  double _listHeight = 150.0;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback(_afterLayout);
    super.initState();
  }

  void _afterLayout(_) {
    if (_itemKeys.length > 0) {
      double newHeight = 0;

      for (int i = 0; i < _itemsToDisplay; i++) {
        newHeight += _itemKeys[i].currentContext.size.height;
      }

      if (_itemKeys.length > _itemsToDisplay) {
        newHeight = newHeight + _itemKeys[0].currentContext.size.height * 0.3;
      }

      _listHeight = newHeight;
    }else{
      _listHeight = 0;
    }
  }

  CameraPosition _getEventLocation(List<LocationPoint> locationPoints) => CameraPosition(
        target: locationPoints[0].pos,
        zoom: _defaultZoom,
      );

  Set<Marker> _getMarkers(List<LocationPoint> locationPoints, Completer<GoogleMapController> controller) {
    _markers.clear();

    locationPoints.forEach((locationPoint) {
      _markers.add(
        Marker(
          onTap: () {
            controller.future.then((control) {
              return control.animateCamera(
                CameraUpdate.newCameraPosition(
                  CameraPosition(
                    target: locationPoint.pos,
                    zoom: _defaultZoom,
                  ),
                ),
              );
            });
          },
          markerId: MarkerId(locationPoint.name),
          position: locationPoint.pos,
          infoWindow: InfoWindow(
            title: locationPoint.name,
            snippet: '${locationPoint.address1} ${locationPoint.address2}',
          ),
        ),
      );
    });

    return _markers.toSet();
  }

  void _onItemTap(Completer<GoogleMapController> controller, LocationPoint locationPoint) {
    controller.future.then((control) {
      return control.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(
            target: locationPoint.pos,
            zoom: _defaultZoom,
          ),
        ),
      );
    });
  }

  Widget _buildListItem(BuildContext context, List<LocationPoint> points, int index, DesignColors colors, Completer<GoogleMapController> controller) {
    final point = points[index];

    if (_itemKeys.length <= index) {
      _itemKeys.add(GlobalKey());
    }

    return InkWell(
      onTap: () => _onItemTap(controller, points[index]),
      child: Padding(
        key: _itemKeys[index],
        padding: EdgeInsets.symmetric(vertical: Margin.small, horizontal: Margin.edgeMargin),
        child: Row(
          children: <Widget>[
            Icon(
              Icons.location_on,
              color: colors.separatorColor,
            ),
            SizedBox(
              width: Margin.medium,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    point.name,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyles.title,
                  ),
                  Text(
                    point.address1,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyles.description,
                  ),
                  Text(
                    point.address2,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyles.description,
                  ),
                ],
              ),
            ),
            IconButton(
              icon: Icon(
                Icons.map,
                color: colors.accentColor,
              ),
              /// Google Maps app on an iOS mobile device : "comgooglemaps://?q=<lat>,<lng>"
              /// Google Maps app on Android : "geo:<lat>,<lng>?z=<zoom>"
              onPressed: () {
                final pos = point.pos;

                if(Platform.isAndroid){
                  //geo:0,0?q=latitude,longitude(label)
                  //geo:0,0?q=1600 Amphitheatre Parkway, Mountain+View, California
                  lurl.launch("geo:0,0?q=${pos.latitude},${pos.longitude}");
                }else if (Platform.isIOS){
                  //todo przetestować i sprawdzić czy czy można jakoś dodać label.
                  //lurl.launch("comgooglemaps://?q=${pos.latitude},${pos.longitude}");
                  lurl.launch("http://maps.apple.com/?q=${pos.latitude},${pos.longitude}");
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Completer<GoogleMapController> _controller = Completer();
    final gpsBloc = BlocProvider.of<GpsModuleBloc>(context);
    final colors = getContainer()<DesignColors>();

    return Scaffold(
      appBar: AppBar(
        title: Text('Dojazd'),
       leading: Platform.isAndroid ?  Padding(
            padding: EdgeInsets.only(left: 10, top: 15, bottom: 15),
            child: SvgPicture.asset(
              'assets/images/logo_no_opacity.svg',
              color: colors.logoColor
            ),
          ) : null,
        centerTitle: true,
      ),
      body: StreamBuilder<GpsModule>(
        initialData: gpsBloc.initialData,
        stream: gpsBloc.moduleStream,
        builder: (context, snapshot) {
          final gpsModule = snapshot.data;
          if (gpsModule != null) {
            return LayoutBuilder(
              builder: (context, constraints) => Container(
                    width: constraints.maxWidth,
                    height: constraints.maxHeight,
                    child: Stack(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(bottom: _listHeight),
                          child: //Container(),
                          GoogleMap(
                            myLocationEnabled: false,
                            markers: _getMarkers(gpsModule.locationPoints, _controller),
                            mapType: MapType.normal,
                            initialCameraPosition: _getEventLocation(gpsModule.locationPoints),
                            onMapCreated: (GoogleMapController controller) {
                              _controller.complete(controller);
                            },
                          ),
                        ),
                        Positioned(
                          bottom: _listHeight,
                          width: constraints.maxWidth,
                          child: SeparatorWidget(),
                        ),
                        Positioned(
                          bottom: 0,
                          left: 0,
                          height: _listHeight,
                          width: constraints.maxWidth,
                          child: ListView.builder(
                            itemCount: gpsModule.locationPoints.length,
                            itemBuilder: (context, index) => _buildListItem(context, gpsModule.locationPoints, index, colors, _controller),
                          ),
                        ),
                      ],
                    ),
                  ),
            );
          } else {
            return Container();
          }
        },
      ),
    );
  }
}
