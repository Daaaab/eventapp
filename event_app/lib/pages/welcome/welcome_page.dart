import 'package:event_app/common/design/colors/design_colors.dart';
import 'package:event_app/common/design/margin.dart';
import 'package:event_app/common/design/text_styles.dart';
import 'package:event_app/common/di/kiwi_injector.dart';
import 'package:event_app/infrastructure/navigate/navigate.dart';
import 'package:event_app/pages/common/bold_special_text.dart';
import 'package:event_app/pages/welcome/widgets/page_indicator.dart';
import 'package:extended_text/extended_text.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class WelcomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  int _currentPage;

  final images = [
    'assets/images/agenda.svg',
    'assets/images/filtry.svg',
    'assets/images/oceny_i_komentarze.svg',
  ];

  final titles = [
    'Przeglądaj agendę i informacje o prelegentach [Quality Excites].',
    '',
    '',
  ];

  final texts = [
    'Bądź na bieżąco dzięki [push notyfikacjom].',
    'Buduj swoją ścieżkę konferencji – korzystaj z [filtrów] i dodawaj prelekcje do ulubionych.',
    'Twój feedback jest ważny! [Oceniaj] prelekcje i dodaj jej swój [komentarz].',
  ];

  @override
  void initState() {
    setState(() {
      _currentPage = 0;
    });

    super.initState();
  }

  GlobalKey _pageKey = GlobalKey();
  double pageHeight;

  @override
  Widget build(BuildContext context) {
    final colors = getContainer()<DesignColors>();
    return Scaffold(
      body: Stack(
        children: <Widget>[
          SafeArea(
            child: Align(
              alignment: Alignment.topLeft,
              child: SvgPicture.asset('assets/images/background.svg'),
            ),
          ),
          Center(
            child: Container(
              key: _pageKey,
              child: PageView(
                onPageChanged: _pageChanged,
                controller: PageController(
                  initialPage: _currentPage,
                ),
                children: <Widget>[
                  _buildPage(context, 0),
                  _buildPage(context, 1),
                  _buildPage(context, 2),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                PageIndicator(
                  Colors.grey,
                  Colors.red,
                  3,
                  _currentPage,
                ),
                SizedBox(height: Margin.medium),
                RaisedButton(
                  color: colors.accentColor,
                  child: Text(
                    'Ruszamy !'.toUpperCase(),
                    style: TextStyles.highlightContrast,
                  ),
                  onPressed: () => _onNextPress(context),
                ),
                SizedBox(height: Margin.edgeMargin),
              ],
            ),
          )
        ],
      ),
    );
  }

  void _pageChanged(int currentPage) {
    setState(() {
      this._currentPage = currentPage;
    });
  }

  Widget _buildPage(BuildContext context, int index) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: Margin.edgeMargin),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            LayoutBuilder(
              builder: (context, constraints) {
                return Align(
                  alignment: Alignment.topCenter,
                  child: SvgPicture.asset(
                    images[index],
                    width: constraints.maxWidth,
                    fit: BoxFit.contain,
                  ),
                );
              },
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: Margin.edgeMargin),
              child: titles.length > 0
                  ? ExtendedText(
                      titles[index],
                      style: TextStyles.welcome,
                      specialTextSpanBuilder: BoldSpecialTextBuilder(),
                    )
                  : Container(),
            ),
            SizedBox(height: Margin.small),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: Margin.edgeMargin),
              child: ExtendedText(
                texts[index],
                style: TextStyles.welcome,
                specialTextSpanBuilder: BoldSpecialTextBuilder(),
              ),
            ),
            SizedBox(
              height: Margin.medium,
            ),
          ],
        ),
      ),
    );
  }

  void _openNextPage() {
    Navigate.navigate(
      context,
      'main',
      replaceRoute: ReplaceRoute.all,
    );
  }

  void iOS_Permission() {
    FirebaseMessaging().requestNotificationPermissions(IosNotificationSettings(sound: true, badge: true, alert: true));
    FirebaseMessaging().onIosSettingsRegistered.listen((IosNotificationSettings settings) {
      _openNextPage();
      print("Settings registered: $settings");
    });
  }

  void _onNextPress(BuildContext context) {
    _openNextPage();
  }
}
