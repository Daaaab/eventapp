import 'package:event_app/pages/welcome/widgets/circle_widget.dart';
import 'package:flutter/material.dart';

class PageIndicator extends StatelessWidget{

  final Color _normalColor;
  final Color _selectedColor;
  final int _size;
  final int _selectedPage;

  PageIndicator(this._normalColor, this._selectedColor, this._size, this._selectedPage) : assert(_selectedPage <= _size);

  List<Widget> _buildDotsList(){
    List<Widget> _dots = List();

    for(int i = 0 ; i < _size ; i++){
      _dots.add(CircleWidget(7.0, _normalColor, _selectedColor, i == _selectedPage));
    }

    return _dots;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: _buildDotsList(),
    );
  }
}