import 'package:flutter/material.dart';

class CircleWidget extends StatefulWidget {
  final double _diameter;
  final Color _color;
  final Color _selectedColor;
  final bool _isSelected;

  CircleWidget(this._diameter, this._color, this._selectedColor, this._isSelected);

  @override
  State<StatefulWidget> createState() => _CircleWidgetState();
}

class _CircleWidgetState extends State<CircleWidget> with TickerProviderStateMixin{
  double _selectionDiameter;

  AnimationController _animControl;
  Animation _selectAnimation;

  _CircleWidgetState(){
    _buildAnimation();
  }

  void _buildAnimation(){
    _animControl = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 500),
    );

    _selectAnimation = Tween<double>(begin: .0, end: 1).animate(CurvedAnimation(parent: _animControl, curve: Curves.fastOutSlowIn))
      ..addListener(() {
        setState(() {
          _selectionDiameter = widget._diameter * _selectAnimation.value;
        });
      });
  }

  @override
  void initState() {
    setState(() {
      if (widget._isSelected) {
        _selectionDiameter = widget._diameter;
      } else {
        _selectionDiameter = 0;
      }
    });

    super.initState();
  }

  @override
  void didUpdateWidget(CircleWidget oldWidget) {
    if (!oldWidget._isSelected && widget._isSelected) {
      //select
      _animControl.forward(from: 0);
    } else if (oldWidget._isSelected && !widget._isSelected) {
      //deselect
      _animControl.reverse(from: 1);
    }

//    setState(() {
//      if (widget._isSelected) {
//        _selectionDiameter = widget._diameter;
//      } else {
//        _selectionDiameter = 0;
//      }
//    });

    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) => Padding(
        padding: EdgeInsets.symmetric(horizontal: 2),
        child: ClipOval(
          child: Container(
            color: widget._color,
            child: Stack(
              children: <Widget>[
                SizedBox(
                  height: widget._diameter,
                  width: widget._diameter,
                ),
                CustomPaint(
                  painter: _SelectRevealPainter(_selectionDiameter, widget._selectedColor),
                  size: Size.square(widget._diameter),
                ),
              ],
            ),
          ),
        ),
      );

  @override
  void dispose() {
    _animControl.dispose();
    super.dispose();
  }
}

class _SelectRevealPainter extends CustomPainter {
  final double _diameter;
  final Color _color;

  _SelectRevealPainter(this._diameter, this._color);

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = _color
      ..style = PaintingStyle.fill;

    canvas.drawCircle(Offset(size.height / 2, size.width / 2), _diameter / 2, paint);
  }

  @override
  bool shouldRepaint(_SelectRevealPainter oldDelegate) {
    return _diameter != oldDelegate._diameter;
  }
}
