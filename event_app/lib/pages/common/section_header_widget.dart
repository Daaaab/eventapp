import 'package:event_app/common/design/margin.dart';
import 'package:event_app/common/design/text_styles.dart';
import 'package:event_app/pages/common/separator_widget.dart';
import 'package:flutter/material.dart';

class SectionHeaderWidget extends StatelessWidget {
  final String _text;
  final double bottomPadding;

  SectionHeaderWidget(this._text, {this.bottomPadding = Margin.medium});

  @override
  Widget build(BuildContext context) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(horizontal: Margin.edgeMargin),
            child: Text(
              _text.toUpperCase(),
              style: TextStyles.header,
            ),
          ),
          SizedBox(height: Margin.medium),
          Padding(
            padding: EdgeInsets.only(bottom: bottomPadding),
            child: SeparatorWidget(),
          ),
        ],
      );
}
