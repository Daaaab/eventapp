import 'package:flutter/material.dart';

//todo check sliver app bar
class MyAppBar extends AppBar {
  MyAppBar({String title, PreferredSizeWidget bottom})
      : super(
          title: Center(child: Text(title)),
          bottom: bottom,
        );
}
