import 'package:event_app/common/design/colors/design_colors.dart';
import 'package:event_app/common/di/kiwi_injector.dart';
import 'package:flutter/material.dart';

class SeparatorWidget extends StatelessWidget{
  @override
  Widget build(BuildContext context){

    final colors = getContainer()<DesignColors>();

    return Container(
      width: MediaQuery.of(context).size.width,
      height: 1,
      color: colors.separatorColor,
    );
  }
}