import 'package:event_app/common/design/margin.dart';
import 'package:event_app/common/design/text_styles.dart';
import 'package:flutter/material.dart';

class GroupHeaderItemWidget extends StatelessWidget{

  final String _groupName;

  GroupHeaderItemWidget(this._groupName);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: Margin.small, horizontal: Margin.edgeMargin),
      child: Text(
        _groupName.toUpperCase(),
        style: TextStyles.group,
      ),
    );
  }
}