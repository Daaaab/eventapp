import 'package:event_app/common/design/text_styles.dart';
import 'package:extended_text/extended_text.dart';
import 'package:flutter/material.dart';

class BoldSpecialTextBuilder extends SpecialTextSpanBuilder {

  @override
  SpecialText createSpecialText(String flag, {TextStyle textStyle, SpecialTextGestureTapCallback onTap, int index}) {
    if (flag == null || flag == "") {
      return null;
    }
    if (isStart(flag, BoldSpecialText.startTag)) {
      return BoldSpecialText();
    }

    return null;
  }
}

class BoldSpecialText extends SpecialText {
  static const String startTag = '[';
  static const String endTag = ']';

  BoldSpecialText() : super(
    startTag,
    endTag,
    TextStyles.welcomeHilight,
  );

  @override
  TextSpan finishText() {
    return TextSpan(
        text: getContent(),
        style: textStyle,
    );
  }
}
