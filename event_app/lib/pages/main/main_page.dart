import 'package:event_app/bloc/agenda/agenda_bloc.dart';
import 'package:event_app/bloc/main/main_bloc.dart';
import 'package:event_app/bloc/menu/menu_bloc.dart';
import 'package:event_app/bloc/modules/gps_module_bloc.dart';
import 'package:event_app/bloc/speakers/speakers_bloc.dart';
import 'package:event_app/common/architecture/bloc_provider.dart';
import 'package:event_app/common/design/colors/design_colors.dart';
import 'package:event_app/common/di/kiwi_injector.dart';
import 'package:event_app/pages/agenda/agenda_page.dart';
import 'package:event_app/pages/menu/menu_page.dart';
import 'package:event_app/pages/navigation/navigation_page.dart';
import 'package:event_app/pages/speakers/speakers_page.dart';
import 'package:flutter/material.dart';

class MainPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MainPageState();
  }
}

class _MainPageState extends State<MainPage> {
  final AgendaBloc _agendaBloc = AgendaBloc();
  final SpeakersBloc _speakersBloc = SpeakersBloc();
  final GpsModuleBloc _gpsModuleBloc = GpsModuleBloc();
  final MenuBloc _menuBloc = MenuBloc();

  List<Widget> _pages;

  int pageIndex;
  
  Widget _getCurrentPage(int index) => _pages[index];

  @override
  void initState() {
    pageIndex = 0;
    _pages = [
      BlocProvider(bloc: _agendaBloc, child: AgendaPage(), shouldHandleDismiss: false),
      BlocProvider(bloc: _speakersBloc, child: SpeakersPage(), shouldHandleDismiss: false),
      BlocProvider(bloc: _gpsModuleBloc, child: NavigationPage(), shouldHandleDismiss: false),
      BlocProvider(bloc: _menuBloc, child: MenuPage(), shouldHandleDismiss: false),
    ];
    super.initState();
  }

  @override
  void dispose() {
    debugPrint("MainPage: dispose()");
    _agendaBloc.dispose();
    _speakersBloc.dispose();
    _gpsModuleBloc.dispose();
    _menuBloc.dispose();
    super.dispose();
  }

  void changePage(int newPageIndex){
    if(newPageIndex != pageIndex){
      setState(() {
        pageIndex = newPageIndex;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final mainBloc = BlocProvider.of<MainBloc>(context);
    final colors = getContainer()<DesignColors>();

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: _getCurrentPage(pageIndex),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: pageIndex,
        fixedColor: colors.accentColor,
        onTap: changePage,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.assignment),
            title: Text('Agenda'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.group),
            title: Text('Prelegenci'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.room),
            title: Text('Dojazd'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.menu),
            title: Text('Menu'),
          ),
        ],
      ),
    );
  }
}
