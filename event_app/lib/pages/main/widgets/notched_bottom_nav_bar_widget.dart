import 'package:flutter/material.dart';

class NotchedBottomNavBarWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _NotchedBottomNavBarWidgetState();
}

class _NotchedBottomNavBarWidgetState extends State<NotchedBottomNavBarWidget> {
  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      shape: CircularNotchedRectangle(),
      child: Container(
        width: double.infinity,
        height: 60,
        child: Row(
          children: <Widget>[],
        ),
      ),
    );
  }
}

//              BottomNavigationBar(
//                type: BottomNavigationBarType.fixed,
//                currentIndex: snapshot.data,
//                fixedColor: colors.accentColor,
//                onTap: (index) {
//                  mainBloc.updateIndex(index);
//                },
//                items: <BottomNavigationBarItem>[
//                  BottomNavigationBarItem(
//                    icon: Icon(Icons.assignment),
//                    title: Text('Agenda'),
//                  ),
//                  BottomNavigationBarItem(
//                    icon: Icon(Icons.group),
//                    title: Text('Prelegenci'),
//                  ),
//                  BottomNavigationBarItem(
//                    icon: Icon(Icons.room),
//                    title: Text('Dojazd'),
//                  ),
//                  BottomNavigationBarItem(
//                    icon: Icon(Icons.menu),
//                    title: Text('Menu'),
//                  ),
//                ],
//              ),
