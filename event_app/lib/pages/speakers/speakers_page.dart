import 'dart:io';

import 'package:event_app/bloc/speakers/speakers_bloc.dart';
import 'package:event_app/common/architecture/bloc_provider.dart';
import 'package:event_app/common/design/colors/design_colors.dart';
import 'package:event_app/common/di/kiwi_injector.dart';
import 'package:event_app/infrastructure/navigate/navigate.dart';
import 'package:event_app/model/app/speaker.dart';
import 'package:event_app/model/app/speaker_type.dart';
import 'package:event_app/model/app/speakers_list_item.dart';
import 'package:event_app/pages/common/group_header_item_widget.dart';
import 'package:event_app/pages/speakers/speaker_item/speaker_item_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SpeakersPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SpeakersPageState();
  }
}

class _SpeakersPageState extends State<SpeakersPage> {

  @override
  Widget build(BuildContext context) {
    final speakersBloc = BlocProvider.of<SpeakersBloc>(context);
    final colors = getContainer()<DesignColors>();

    return Scaffold(

      appBar: AppBar(
        title: Text('Prelegenci'),
       leading: Platform.isAndroid ?  Padding(
            padding: EdgeInsets.only(left: 10, top: 15, bottom: 15),
            child: SvgPicture.asset(
              'assets/images/logo_no_opacity.svg',
              color: colors.logoColor
            ),
          ) : null,
        centerTitle: true,
      ),
      body: _buildSpeakersList(speakersBloc),
    );
  }

  void _openDetails(BuildContext context, Speaker speaker) {

    final speakersBloc = BlocProvider.of<SpeakersBloc>(context);

    speakersBloc.getEventsForSpeakerStream(speaker)
    .listen((events) =>
        Navigate.navigate(
          context,
          'speakers/details',
          replaceRoute: ReplaceRoute.none,
          arg: {'speaker': speaker, 'events' : events},
        )
    );
  }

  Widget _buildSpeakersList(SpeakersBloc speakersBloc) => StreamBuilder<List<SpeakersListItem>>(
      initialData: speakersBloc.initialData,
      stream: speakersBloc.speakersStream,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return ListView.builder(
            itemCount: snapshot.data.length,
            itemBuilder: (context, index) => _buildListItem(snapshot.data[index]),
          );
        } else {
          //todo pozbyć się tego
          return Text('No data');
        }
      });

  Widget _buildListItem(SpeakersListItem item) {
    if (item is Speaker) {
      return SpeakerItemWidget(
        item,
        onClick: _openDetails,
      );
    } else {
      assert(item is SpeakerType);
      return GroupHeaderItemWidget((item as SpeakerType).type);
    }
  }
  @override
  void dispose() {
    debugPrint('Disposing Speakers Widget');
    super.dispose();
  }
}
