import 'package:event_app/common/design/margin.dart';
import 'package:event_app/common/design/text_styles.dart';
import 'package:event_app/model/app/speaker.dart';
import 'package:event_app/pages/speakers/speaker_item/speaker_photo_widget.dart';
import 'package:flutter/material.dart';

//todo rozwiązać problem pobierania zdjęć, security firebase storage, zbadać temat zbyt dużej il .requestów
//todo ujednolicić marginesy i wygląd SpeakersItem i EventItem

typedef void OnSpeakerItemClick(BuildContext context, Speaker speaker);

class SpeakerItemWidget extends StatelessWidget {
  final Speaker _speaker;
  final bool isClickable;
  final OnSpeakerItemClick onClick;
  final BoxConstraints constraints;

  SpeakerItemWidget(this._speaker, {this.isClickable = true, this.onClick, this.constraints});

  //todo czy czasem wszystkie opcje (agenda + prelegenci) nie są rysowane na raz ????!
  Widget _speakerItemContent(BuildContext context) {
    return Container(
      //width: MediaQuery.of(context).size.width - 2* Margin.edgeMargin,
      child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              '${_speaker.name} ${_speaker.surname}',
              style: TextStyles.title,
            ),
            SizedBox(height: Margin.small),
            Text(
              _speaker.title,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyles.highlight,
            ),
            Text(
              _speaker.company,
              style: TextStyles.highlight,
            )
          ],
        ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final content = Padding(
      padding: EdgeInsets.symmetric(vertical: Margin.medium, horizontal: Margin.edgeMargin),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          FittedBox(
            fit: BoxFit.contain,
            child: SpeakerPhotoWidget(_speaker, 55, 55),
          ),
          SizedBox(
            width: Margin.small,
          ),
          Container(
            width: MediaQuery.of(context).size.width - 2* Margin.edgeMargin - 55 - Margin.small - 8,
              child: _speakerItemContent(context)),
        ],
      ),
    );

    if (isClickable) {
      return InkWell(
        splashColor: Theme.of(context).accentColor,
        //todo error przy pacnięciu w prelegenta z poziomu detali ewentu
        onTap: () => onClick(context, _speaker),
        child: content,
      );
    } else {
      return content;
    }
  }
}
