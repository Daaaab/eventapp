import 'package:event_app/common/config/rest_config.dart';
import 'package:event_app/common/design/margin.dart';
import 'package:event_app/model/app/speaker.dart';
import 'package:flutter/material.dart';

import 'package:flutter_advanced_networkimage/provider.dart';

//todo zbadać kiedy leci request o nowe foto.
// todo nie zaciągać nowych photo jeśli są one w cachu
class SpeakerPhotoWidget extends StatelessWidget {
  final Speaker _speaker;
  final double _height;
  final double _width;

  SpeakerPhotoWidget(this._speaker, this._height, this._width);

  @override
  Widget build(BuildContext context) {
    final photoUri = Uri.https(RestConfig.backendPath, RestConfig.getImage, {'type': 'speakers', 'name': _speaker.photo});

    return Padding(
      padding: EdgeInsets.only(right: Margin.small),
      child: SizedBox(
        height: _height,
        width: _width,
        child: ClipOval(
          child: FadeInImage(
            placeholder: AssetImage('assets/images/speaker_placeholder.png'),
            image: AdvancedNetworkImage(
              photoUri.toString(),
              useDiskCache: true,

            ),
          ),
        ),
      ),
    );
  }
}
