import 'dart:io';

import 'package:event_app/common/design/colors/design_colors.dart';
import 'package:event_app/common/design/margin.dart';
import 'package:event_app/common/design/text_styles.dart';
import 'package:event_app/common/di/kiwi_injector.dart';
import 'package:event_app/model/app/event.dart';
import 'package:event_app/model/app/speaker.dart';
import 'package:event_app/pages/agenda/widgets/event_item/event_item_widget.dart';
import 'package:event_app/pages/common/section_header_widget.dart';
import 'package:event_app/pages/speakers/speaker_item/speaker_item_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SpeakerDetailsPage extends StatelessWidget{

  final Speaker _speaker;
  final List<Event> _events;

  SpeakerDetailsPage(this._speaker, this._events);

  List<Widget> _buildEventsList() {

    final List<Widget> result = List();

    _events.forEach((event) => result.add(EventItemWidget(event)));

    return result;
  }

  @override
  Widget build(BuildContext context) {

    final colors = getContainer()<DesignColors>();

    return Scaffold(
        appBar: AppBar(
          title: Text('Prelegent'),
         leading: Platform.isAndroid ?  Padding(
            padding: EdgeInsets.only(left: 10, top: 15, bottom: 15),
            child: SvgPicture.asset(
              'assets/images/logo_no_opacity.svg',
              color: colors.logoColor
            ),
          ) : null,
          centerTitle: true,
        ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SpeakerItemWidget(_speaker, isClickable: false),
            Padding(
              padding: EdgeInsets.all(Margin.edgeMargin),
              child: Text(
                _speaker.description,
                style: TextStyles.description,
              ),
            ),
            SectionHeaderWidget('Wydarzenia'),
            Column(
              children: _buildEventsList(),
            )
          ],
        ),
      )
    );
  }
}