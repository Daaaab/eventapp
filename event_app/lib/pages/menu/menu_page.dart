import 'dart:io';

import 'package:event_app/bloc/menu/menu_bloc.dart';
import 'package:event_app/common/architecture/bloc_provider.dart';
import 'package:event_app/common/design/colors/design_colors.dart';
import 'package:event_app/common/di/kiwi_injector.dart';
import 'package:event_app/model/app/article_module.dart';
import 'package:event_app/model/app/location_map_module.dart';
import 'package:event_app/model/app/module.dart';
import 'package:event_app/model/app/partner_module.dart';
import 'package:event_app/pages/modules/article/article_page.dart';
import 'package:event_app/pages/modules/map/location_map_page.dart';
import 'package:event_app/pages/modules/partners/partners_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class MenuPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MenuPageState();
  }
}

class _MenuPageState extends State<MenuPage> with TickerProviderStateMixin {
  TabController _tabController;

  List<Widget> _buildTabs(List<Module> modules) {
    var tabs = modules.map<Tab>((module) {
      return Tab(text: module.label);
    }).toList();

    return tabs;
  }

  List<Widget> _buildPages(List<Module> modules) {
    List<Widget> result = List();
    modules.forEach((module) => result.add(_resolveModuleType(module)));
    return result;
  }

  Widget _resolveModuleType(Module module) {
    switch (module.runtimeType) {
      case ArticleModule:{
          return ArticlePage(module);
        }
      case LocationMapModule: {
        return LocationMapPage(module: module);
      }
      case PartnerModule:{
        return PartnersPage(module);
      }

      default:
        return Text('Module not implemented');
    }
  }

  Widget _buildTabView(BuildContext context, List<Module> modules) {
    _tabController = TabController(length: modules.length, vsync: this, initialIndex: 0);

    final colors = getContainer()<DesignColors>();

    return Scaffold(
      appBar: AppBar(
          title: Text('Menu'),
          leading: Platform.isAndroid ?  Padding(
            padding: EdgeInsets.only(left: 10, top: 15, bottom: 15),
            child: SvgPicture.asset(
              'assets/images/logo_no_opacity.svg',
              color: colors.logoColor
            ),
          ) : null,
          centerTitle: true,
          bottom: TabBar(
            isScrollable: true,
            controller: _tabController,
            tabs: _buildTabs(modules),
          )),
      body: TabBarView(
        controller: _tabController,
        children: _buildPages(modules),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    debugPrint("MenuPage: building");

    final moduleBloc = BlocProvider.of<MenuBloc>(context);

    return StreamBuilder<List<Module>>(
        initialData: moduleBloc.initialData,
        stream: moduleBloc.moduleStream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return _buildTabView(context, snapshot.data);
          } else {
            return Text('No data');
          }
        });
  }
}
