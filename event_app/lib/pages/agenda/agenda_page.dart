import 'dart:io';

import 'package:event_app/bloc/agenda/agenda_bloc.dart';
import 'package:event_app/common/architecture/bloc_provider.dart';
import 'package:event_app/common/config/config.dart';
import 'package:event_app/common/design/colors/design_colors.dart';
import 'package:event_app/common/design/margin.dart';
import 'package:event_app/common/design/text_styles.dart';
import 'package:event_app/common/di/kiwi_injector.dart';
import 'package:event_app/common/utils/hex_utils.dart';
import 'package:event_app/model/app/day.dart';
import 'package:event_app/model/app/filter.dart';
import 'package:event_app/pages/agenda/widgets/agenda_list_widget.dart';
import 'package:event_app/pages/agenda/widgets/filters/filters_widget.dart';
import 'package:event_app/pages/common/separator_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AgendaPage extends StatefulWidget {
  AgendaPage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _AgendaPageState();
  }
}

// todo dodać ukrywanie appbara przy scrolu
// todo przetestować zachowanie tabów po dodaniu nowego dnia
// todo sprawdzić wartości początkowe dla StreamBuildera
// todo ogarnąć pusty widok
// todo ogarnąć przechowywanie danych listy żeby się nie ładowały za każdym razem
// todo dorobić animacje
// todo sprawdzić nieklikalne eventy jak np rejestracja i przerwa kawowa
// todo dodać filtry
class _AgendaPageState extends State<AgendaPage> with TickerProviderStateMixin {
  AgendaBloc _eventsBloc;
  TabController _tabController;

  @override
  void initState() {
    _eventsBloc = BlocProvider.of<AgendaBloc>(context);
    //_eventsBloc.initBloc();
    debugPrint('AgendaPage: initState');
    super.initState();
  }

  List<Widget> _buildTabs(List<Day> days) {
    var tabs = days.map<Tab>((day) {
      return Tab(text: Config.upperCaseTabs ? day.name.toUpperCase() : day.name);
    }).toList();

    return tabs;
  }

  //todo sprawdzić czy to nie pierdoli performencu
  List<Widget> _buildAgendaLists(List<Day> days) {
    var agendaLists = days.map<AgendaListWidget>((day) {
      return AgendaListWidget(day.dayId);
    }).toList();

    return agendaLists;
  }

  @override
  void dispose() {
    debugPrint('AgendaPage: dispose');
    super.dispose();
  }

  Widget _buildTabView(BuildContext context, List<Day> days, AgendaBloc bloc, DesignColors colors) {
    _tabController = TabController(
      length: days.length,
      vsync: this,
      initialIndex: _tabController == null ? 0 : _tabController.index,
    );

    if (days.length == 1 && days[0].dayId == Day.favouritesDayId) {
      return Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SvgPicture.asset('assets/images/wi-fi.svg'),
              Text('Błąd połączenia z internetem', style: TextStyles.title),
              SizedBox(height: Margin.medium),
              RaisedButton(
                color: colors.accentColor,
                child: Text('Odśwież'.toUpperCase(), style: TextStyles.highlightContrast),
                onPressed: () {
                  bloc.refreshAgenda();
                },
              )
            ],
          ),
        ),
      );
    }

    return Scaffold(
      floatingActionButton: StreamBuilder<List<Filter>>(
        initialData: List<Filter>(),
        stream: bloc.filtersStream,
        builder: (context, snapshot) {
          final filters = snapshot.hasData ? snapshot.data : List<Filter>();

          if (filters.isNotEmpty) {
            return Container();
          }

          return FloatingActionButton(
            onPressed: () => bloc.toggleFiltersMenu(),
            backgroundColor: colors.accentColor,
            child: Icon(Icons.filter_list),
          );
        },
      ),
      appBar: AppBar(
        title: Text('Agenda'),
       leading: Platform.isAndroid ?  Padding(
            padding: EdgeInsets.only(left: 10, top: 15, bottom: 15),
            child: SvgPicture.asset(
              'assets/images/logo_no_opacity.svg',
              color: colors.logoColor
            ),
          ) : null,
        centerTitle: true,
        bottom: TabBar(
          isScrollable: true,
          controller: _tabController,
          tabs: _buildTabs(days),
        ),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: TabBarView(
              controller: _tabController,
              children: _buildAgendaLists(days),
            ),
          ),
          SeparatorWidget(),
          StreamBuilder<List<Filter>>(
              stream: bloc.filtersStream,
              initialData: List<Filter>(),
              builder: (context, snapshot) {
                final filters = snapshot.hasData ? snapshot.data : List<Filter>();

                if (filters == null || filters.isEmpty) {
                  return Container();
                }

                return GestureDetector(
                  onTap: () => bloc.toggleFiltersMenu(),
                  child: Container(
                    color: colors.backgroundColor,
                    height: 50,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: filters.length,
                      itemBuilder: (context, index) {
                        final filter = filters[index];
                        return Padding(
                          padding: EdgeInsets.symmetric(horizontal: Margin.half),
                          child: Chip(
                            label: Text(filter.text),
                            backgroundColor: HexUtils.convert(filter.color),
                          ),
                        );
                      },
                    ),
                  ),
                );
              }),
        ],
      ),
    );
  }

  Widget _buildFiltersInfo(DesignColors colors, AgendaBloc bloc) => StreamBuilder<List<Filter>>(
        stream: bloc.filtersStream,
        initialData: List<Filter>(),
        builder: (context, snapshot) {
          final filters = snapshot.hasData ? snapshot.data : List<Filter>();

          if (filters.isEmpty) {
            return Container();
          }

          return GestureDetector(
            onTap: () => bloc.toggleFiltersMenu(),
            child: Align(
              alignment: Alignment.bottomLeft,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SeparatorWidget(),
                  Container(
                    color: colors.backgroundColor,
                    height: 50,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: filters.length,
                      itemBuilder: (context, index) {
                        final filter = filters[index];
                        return Padding(
                          padding: EdgeInsets.symmetric(horizontal: Margin.half),
                          child: Chip(
                            label: Text(filter.text),
                            backgroundColor: HexUtils.convert(filter.color),
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      );

  @override
  Widget build(BuildContext context) {
    debugPrint("AgendaPage: building");

    final colors = getContainer()<DesignColors>();

    return StreamBuilder<List<Day>>(
      stream: _eventsBloc.dayStream,
      initialData: _eventsBloc.initialDaysData,
      builder: (context, snapshot) {
        final days = snapshot.hasData ? snapshot.data : List<Day>();

        List<Widget> widgets = List();
        widgets.add(_buildTabView(context, days, _eventsBloc, colors));
        widgets.add(LayoutBuilder(
          builder: (context, constraints) => FiltersWidget(
                constraints.maxHeight,
                constraints.maxWidth,
              ),
        ));
        return Stack(
          children: widgets,
        );
      },
    );
  }
}
