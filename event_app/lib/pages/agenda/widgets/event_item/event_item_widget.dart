import 'package:event_app/common/config/config.dart';
import 'package:event_app/common/design/colors/design_colors.dart';
import 'package:event_app/common/design/margin.dart';
import 'package:event_app/common/di/kiwi_injector.dart';
import 'package:event_app/infrastructure/navigate/navigate.dart';
import 'package:event_app/model/app/event.dart';
import 'package:event_app/pages/agenda/widgets/event_item/event_fav_button_widget.dart';
import 'package:event_app/pages/agenda/widgets/event_item/event_item_content_widget.dart';
import 'package:event_app/pages/agenda/widgets/event_item/event_item_hours_widget.dart';
import 'package:flutter/material.dart';

class EventItemWidget extends StatelessWidget{

  final Event _event;
  final Event prevEvent;

  EventItemWidget(this._event, {this.prevEvent});

  //todo sprawdzić czy content jest wyrównany
  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Theme.of(context).accentColor,
      onTap: () => _openDetails(context),
      child: StreamBuilder<int>(
        stream: Stream.periodic(Duration(minutes: 1)),
        builder: (context, snapshot) {
          return Container(
            color: _getItemBackground(),
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: Margin.medium, horizontal: Margin.edgeMargin),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(right: Margin.large),
                    child: EventItemHoursWidget(
                        _event.startTime,
                        _event.endTime,
                        _shouldDisplayHours(),
                        _isNow(),
                    ),
                  ),
                  Expanded(
                    child: EventItemContentWidget(_event),
                  ),
                 EventFavButtonWidget(_event.id),
                ],
              ),
            ),
          );
        }
      ),
    );
  }

  bool _isNow(){
    var date = DateTime.now();
    final startTime = _event.startTime;
    final endTime = _event.endTime;

    return date.isAfter(startTime) && date.isBefore(endTime);
  }

  Color _getItemBackground(){

    final colors = getContainer()<DesignColors>();

    if(_isNow()){
      return colors.statusBarColor;
    }

    return null;
  }

  //todo sprawdzić czy godziny są wyświetlane odpowiednio dla różnych grup
  bool _shouldDisplayHours(){
    if(Config.collapsableStartHours && prevEvent != null && _event.groupId == prevEvent.groupId){
      if(_event.startTime.isAtSameMomentAs(prevEvent.startTime) && _event.endTime.isAtSameMomentAs(prevEvent.endTime)){
        return false;
      }else{
        return true;
      }
    }else{
      return true;
    }
  }

  void _openDetails(BuildContext context){
    if(_event.description != null && _event.description.isNotEmpty){
      Navigate.navigate(
        context,
        'agenda/details',
        replaceRoute: ReplaceRoute.none,
        arg: {'event' : _event},
      );
    }
  }
}