import 'package:event_app/common/config/config.dart';
import 'package:event_app/common/design/margin.dart';
import 'package:event_app/common/design/text_styles.dart';
import 'package:event_app/common/utils/format.dart';
import 'package:flutter/material.dart';

class EventItemHoursWidget extends StatelessWidget {
  final DateTime _fromHour;
  final DateTime _toHour;
  final bool _shoudShowTime;
  final bool isNow;

  EventItemHoursWidget(this._fromHour, this._toHour, this._shoudShowTime, this.isNow);

  @override
  Widget build(BuildContext context) {
    var viewList = List<Widget>();

    if (Config.showFromHours) {
      viewList.add(_buildHoursSection(_fromHour, context));
    }

    if (Config.showToHours) {
      viewList.add(_buildHoursSection(_toHour, context));
    }

    if (viewList.length > 1) {
      viewList.insert(1, SizedBox(height: Margin.half));
    }

    if (viewList.isEmpty) {
      return Container();
    }

    return Column(
      children: viewList,
    );
  }
  //todo sprawdzić czy visibility nie powoduje problemów z performancem
  Widget _buildHoursSection(DateTime dateTime, BuildContext context) =>
      //todo dont return visibility if something should be visible
      Visibility(
        maintainSize: true,
        maintainAnimation: true,
        maintainState: true,
        visible: _shoudShowTime,
        child: _getHoursTextView(dateTime, context),
      );

  Widget _getHoursTextView(DateTime dateTime, BuildContext context) =>
      isNow ? Text(
        'Teraz',
        style: TextStyles.hour,
      ) :Text(
        Format.hourMinutes(dateTime),
        style: TextStyles.hour,
      );
}
