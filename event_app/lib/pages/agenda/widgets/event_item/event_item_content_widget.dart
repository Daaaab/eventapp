import 'package:event_app/common/config/config.dart';
import 'package:event_app/common/design/margin.dart';
import 'package:event_app/common/design/text_styles.dart';
import 'package:event_app/model/app/category.dart';
import 'package:event_app/model/app/event.dart';
import 'package:event_app/pages/agenda/widgets/event_item/category/chips_category_widget.dart';
import 'package:event_app/pages/agenda/widgets/event_item/category/dot_category_widget.dart';
import 'package:flutter/material.dart';

// todo wyciągnąć to jako element wspólny event/speaker item
class EventItemContentWidget extends StatelessWidget {
  final Event _event;
  final bool showSpeakers;

  EventItemContentWidget(this._event, {this.showSpeakers = true});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          _event.title,
          style: TextStyles.title,
          maxLines: Config.titleEllipsis ? 2 : null,
          overflow: Config.titleEllipsis ? TextOverflow.ellipsis : null,
        ),
        _getSpeakersWidget(),
        _getLocationDurationWidget(),
        _event.category != null ? SizedBox(height: Margin.small) : Container(),
        _getCategoryRow(),
      ],
    );
  }

  Widget _getCategoryRow() {
    List<Widget> categoryWidgets = List();

    if (_event.category != null) {
      for (int i = 0; i < _event.category.length; i++) {
        categoryWidgets.add(_getCategoryWidget(_event.category[i]));
        if (i < _event.category.length - 1) {
          categoryWidgets.add(SizedBox(width: Margin.medium));
        }
      }
    } else {
      return Container();
    }

    return Row(
      children: categoryWidgets,
    );
  }

  Widget _getCategoryWidget(Category category) {
    if (Config.chipAsCategory) {
      return ChipsCategoryWidget(category);
    } else {
      return DotCategoryWidget(category);
    }
  }

  Widget _getSpeakersWidget() {
    var speakersList = _event.speakers;

    if (speakersList != null && speakersList.isNotEmpty && showSpeakers) {
      var speakersText = "";

      for (int i = 0; i < speakersList.length; i++) {
        var speaker = speakersList[i];
        speakersText += "${speaker.name} ${speaker.surname}";
        if (i < speakersList.length - 1) {
          speakersText += "\n";
        }
      }

      return Padding(
        padding: const EdgeInsets.only(top: Margin.medium),
        child: Text(
          speakersText,
          style: TextStyles.highlight,
        ),
      );
    } else {
      return Container();
    }
  }

  Widget _getLocationDurationWidget() {
    if (!Config.showDuration && !Config.showLocation) {
      return Container();
    }

    String resultText = "";

    if (Config.showDuration &&
        _event.id != '4d1a56555123774a3457771379766d14' &&
        _event.id != '09790d07766e4e740d273b216f584e0f' &&
        _event.id != '1a3941026f6523556c62083b201f3855' &&
        _event.id != '7155355316467871162039347620121a') {
      resultText += _event.endTime.difference(_event.startTime).inMinutes.toString() + " minut";
    }

    if (Config.showLocation && _event.location != null && _event.location.isNotEmpty) {
      if (resultText.isNotEmpty) {
        resultText += " | ";
      }

      resultText += _event.location;
    }

    return resultText.isNotEmpty ? Padding(
      padding: EdgeInsets.only(top: Margin.small),
      child: Text(
        resultText,
        style: TextStyles.normal,
      ),
    ) : Container();
  }
}
