import 'package:event_app/common/design/margin.dart';
import 'package:event_app/common/design/text_styles.dart';
import 'package:event_app/common/utils/hex_utils.dart';
import 'package:event_app/model/app/category.dart';
import 'package:flutter/material.dart';

class DotCategoryWidget extends StatelessWidget {
  final Category _category;

  DotCategoryWidget(this._category);

  @override
  Widget build(BuildContext context) => Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ClipOval(
            child: Container(
              color: HexUtils.convert(_category.color),
              child: SizedBox(
                height: 10,
                width: 10,
              ),
            ),
          ),
          SizedBox(
            width: Margin.half,
          ),
          Text(
            _category.name,
            style: TextStyles.extraDescription,
          )
        ],
      );
}
