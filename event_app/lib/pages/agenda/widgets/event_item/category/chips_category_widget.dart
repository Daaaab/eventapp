import 'package:event_app/common/design/text_styles.dart';
import 'package:event_app/common/utils/hex_utils.dart';
import 'package:event_app/model/app/category.dart';
import 'package:flutter/material.dart';

class ChipsCategoryWidget extends StatelessWidget {
  final Category _category;

  ChipsCategoryWidget(this._category);

  @override
  Widget build(BuildContext context) => ClipRRect(
        borderRadius: BorderRadius.circular(15),
        child: Container(
          color: HexUtils.convert(_category.color),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 3.0, horizontal: 8.0),
            child: Text(
              _category.name,
              style: TextStyles.extraDescription,
            ),
          ),
        ),
      );
}
