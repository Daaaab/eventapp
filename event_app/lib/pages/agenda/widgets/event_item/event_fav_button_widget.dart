import 'package:event_app/bloc/agenda/favs_bloc.dart';
import 'package:event_app/common/design/colors/design_colors.dart';
import 'package:event_app/common/di/kiwi_injector.dart';
import 'package:flutter/material.dart';

class EventFavButtonWidget extends StatefulWidget {
  final String _eventId;

  EventFavButtonWidget(this._eventId);

  @override
  State<StatefulWidget> createState() => _EventFavButtonWidgetState();
}

class _EventFavButtonWidgetState extends State<EventFavButtonWidget> {

  FavsBloc _favsBloc;
  
  @override
  void initState() {
    _favsBloc = FavsBloc(widget._eventId);
    super.initState();
  }
  
  @override
  Widget build(BuildContext context) {

    final colors = getContainer()<DesignColors>();

    return StreamBuilder(
      initialData: _favsBloc.initialIsFav(widget._eventId),
      stream: _favsBloc.isFav(widget._eventId),
      builder: (context, snapshot) {
        final isFav = snapshot.hasData ? snapshot.data : false;
        final starIcon = isFav ? Icons.star : Icons.star_border;

        return IconButton(
          onPressed: () => _favsBloc.setFav(widget._eventId, !isFav),
          icon: Icon(starIcon),
          color: isFav ? colors.accentColor : Colors.black,
        );
      },
    );
  }
  
  @override
  void dispose() {
    _favsBloc.dispose();
    super.dispose();
  }
}