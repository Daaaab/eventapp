import 'package:event_app/bloc/agenda/agenda_bloc.dart';
import 'package:event_app/common/architecture/bloc_provider.dart';
import 'package:event_app/common/design/margin.dart';
import 'package:event_app/common/design/text_styles.dart';
import 'package:event_app/model/app/agenda_list_item.dart';
import 'package:event_app/model/app/day.dart';
import 'package:event_app/model/app/event.dart';
import 'package:event_app/model/app/group.dart';
import 'package:event_app/pages/agenda/widgets/event_item/event_item_widget.dart';
import 'package:event_app/pages/common/group_header_item_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class AgendaListWidget extends StatefulWidget {
  final String _dayId;

  AgendaListWidget(this._dayId);

  @override
  _AgendaListWidgetState createState() {
    return new _AgendaListWidgetState();
  }
}

//todo ogarnąć początkowego null w snapshocie. Wrzucić tam jakieś puste dane czy coś
// todo sprawdzic jak sprawuje sie mixin. Odpalic flutter performance + rebuild info
class _AgendaListWidgetState extends State<AgendaListWidget> with AutomaticKeepAliveClientMixin<AgendaListWidget> {
  AgendaBloc _agendaBloc;
  var keepAlive = true;

  @override
  void initState() {
    _agendaBloc = BlocProvider.of<AgendaBloc>(context);
    debugPrint(widget._dayId);
    super.initState();
  }

  Widget _buildList(BuildContext context) {
    var result = StreamBuilder<List<AgendaListItem>>(
      initialData: _agendaBloc.getInitialGroupedEventsForDay(widget._dayId),
      stream: _agendaBloc.getGroupedEventsStreamForDay(widget._dayId),
      builder: (BuildContext context, AsyncSnapshot<List<AgendaListItem>> snapshot) {
        final data = snapshot.hasData ? snapshot.data : List<AgendaListItem>();

        if (data.isEmpty && widget._dayId == Day.favouritesDayId) {
          return SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: Margin.edgeMargin,),
                Center(child: Text( 'Pusto tu... dodaj interesujące Cię prelekcje do ulubionych!', textAlign: TextAlign.center , style: TextStyles.highlight,)),
                SvgPicture.asset(
                  'assets/images/ulubione.svg',
                  width: MediaQuery.of(context).size.width,
                  fit: BoxFit.contain,
                ),
              ],
            ),
          );
        }

        var result = ListView.builder(
          itemCount: data.length,
          itemBuilder: (BuildContext context, int index) => _buildItem(context, data, index),
        );

        return result;
      },
    );

    return result;
  }

  Widget _buildItem(BuildContext context, List<AgendaListItem> events, int index) {
    Widget result;

    if (events[index] is Event) {
      var prevEvent = index == 0 ? null : events[index - 1];
      //check if previous item was a Group or Event
      prevEvent = prevEvent is Event ? prevEvent : null;

      result = EventItemWidget(events[index], prevEvent: prevEvent);
    } else {
      final group = events[index] as Group;
      result = GroupHeaderItemWidget(group.name);
    }

    if(index == events.length -1){
      return Padding(
        padding: EdgeInsets.only(bottom: 50),
        child: result,
      );
    }

    return result;
  }

  @override
  Widget build(BuildContext context) {
    return _buildList(context);
  }

  @override
  void dispose() {
    debugPrint('Disposing Agenda List Widget');
    super.dispose();
  }

  @override
  bool get wantKeepAlive => keepAlive;
}
