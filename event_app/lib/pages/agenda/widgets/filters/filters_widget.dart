import 'package:event_app/bloc/agenda/agenda_bloc.dart';
import 'package:event_app/common/architecture/bloc_provider.dart';
import 'package:event_app/common/design/colors/design_colors.dart';
import 'package:event_app/common/design/margin.dart';
import 'package:event_app/common/design/text_styles.dart';
import 'package:event_app/common/di/kiwi_injector.dart';
import 'package:event_app/common/utils/hex_utils.dart';
import 'package:event_app/model/app/filter.dart';
import 'package:event_app/pages/agenda/widgets/filters/filters_item.dart';
import 'package:event_app/pages/common/section_header_widget.dart';
import 'package:flutter/material.dart';

class FiltersWidget extends StatefulWidget {
  final double _maxHeight;
  final double _maxWidth;
  final double _heightPadding = 5;
  final double _borderRadius = 30;
  final double _headerHeight = 50;
  final int _animationTime = 400;

  FiltersWidget(this._maxHeight, this._maxWidth);

  @override
  State<StatefulWidget> createState() => _FiltersWidgetState();
}

class _FiltersWidgetState extends State<FiltersWidget> {
  double get _maxHeight => widget._maxHeight - widget._heightPadding;

  Future<bool> _onBackPressed(AgendaBloc bloc, bool isShown) async {
    if (isShown) {
      bloc.toggleFiltersMenu();
      return false;
    } else {
      return true;
    }
  }

  void _onFilterClick(String filterName, AgendaBloc bloc) {
    bloc.setFilter(filterName);
  }

  List<Widget> _buildFilterItems(List<Filter> filters, AgendaBloc bloc) {
    List<Widget> result = List();

    for (int i = 0; i < filters.length; i++) {

      final filter = filters[i];
      final filterWidget = FiltersItem(
        filter.id,
        filter.text,
        HexUtils.convert(filter.color),
        (filterId) => _onFilterClick(filterId, bloc),
        isSelected: filter.isEnabled,
      );

      result.add(filterWidget);
      if (i < filters.length - 1) {
        result.add(SizedBox(height: Margin.small));
      }
    }
    return result;
  }

  Widget _buildGroupsFilterSection(AgendaBloc bloc) => StreamBuilder<List<Filter>>(
        stream: bloc.groupFiltersStream,
        builder: (context, snapshot) {
          final filters = snapshot.hasData ? snapshot.data : List<Filter>();

          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: _buildFilterItems(filters, bloc),
          );
        },
      );

  Widget _buildCategoryFilterSection(AgendaBloc bloc) => StreamBuilder<List<Filter>>(
        stream: bloc.categoryFiltersStream,
        builder: (context, snapshot) {
          final filters = snapshot.hasData ? snapshot.data : List<Filter>();

          final pivotIndex = (filters.length / 2).ceil();
          final columnA = filters.getRange(0, pivotIndex).toList();
          final columnB = filters.getRange(pivotIndex, filters.length).toList();

          return Row(
            children: <Widget>[
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: _buildFilterItems(columnA, bloc),
                ),
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: _buildFilterItems(columnB, bloc),
                ),
              ),
            ],
          );
        },
      );

  @override
  Widget build(BuildContext context) {
    final agendaBloc = BlocProvider.of<AgendaBloc>(context);
    final colors = getContainer()<DesignColors>();

    return StreamBuilder<bool>(
      initialData: agendaBloc.initialShowFiltersData,
      stream: agendaBloc.showFiltersStream,
      builder: (context, snapshot) {
        final show = snapshot.hasData ? snapshot.data : false;

        return WillPopScope(
          onWillPop: () => _onBackPressed(agendaBloc, show),
          child: SafeArea(
            right: false,
            left: false,
            bottom: false,
              child: AnimatedContainer(
                padding: EdgeInsets.only(top: widget._maxHeight - (show ? _maxHeight : 0)),
                duration: Duration(milliseconds: widget._animationTime),
                curve: Curves.fastOutSlowIn,
                child: Container(
                  color: Colors.transparent,
                  child: Container(
                    width: widget._maxWidth,
                    height: widget._maxHeight,
                    decoration: ShapeDecoration(
                      color: colors.backgroundColor,
                      shape: RoundedRectangleBorder(
                        side: BorderSide(
                          width: 1,
                          color: colors.separatorColor,
                        ),
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(widget._borderRadius),
                          topRight: Radius.circular(widget._borderRadius),
                        ),
                      ),
                    ),
                    child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      /// <<<<<<<<<<<<<<<<     CONTENT
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          /// <<<<<<<<<<<<<<<<     HEADER
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: Margin.edgeMargin),
                            child: LayoutBuilder(
                              builder: (context, constraints) => SizedBox(
                                width: constraints.maxWidth,
                                height: widget._headerHeight,
                                child: Stack(
                                  children: <Widget>[
                                    Align(
                                      alignment: Alignment.center,
                                      child: Text(
                                        'Filtry'.toUpperCase(),
                                        style: TextStyles.title,
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.centerRight,
                                      child: GestureDetector(
                                        behavior: HitTestBehavior.translucent,
                                        onTap: () => agendaBloc.toggleFiltersMenu(),
                                        child: Padding(
                                          padding: EdgeInsets.only(top: Margin.small, bottom: Margin.small, left: Margin.large),
                                          child: Icon(Icons.expand_more),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: Margin.medium),
                          SectionHeaderWidget('Kategorie'),
                          // <<<<<<<<<<<<<<<<     ACTUAL CONTENT
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: Margin.edgeMargin),
                            child: _buildCategoryFilterSection(agendaBloc),
                          ),
                          SizedBox(height: Margin.medium),
                          SectionHeaderWidget('Bloki'),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: Margin.edgeMargin),
                            child: _buildGroupsFilterSection(agendaBloc),
                          ),
                          SizedBox(height: Margin.medium),
                          Row(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.only(left: Margin.edgeMargin, right: Margin.edgeMargin, bottom: Margin.small),
                                  child: RaisedButton(
                                    color: colors.accentColor,
                                    child: Text('OK'.toUpperCase(), style: TextStyles.highlightContrast,),
                                    onPressed: () => _onBackPressed(agendaBloc, show),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
        );
      },
    );
  }
}
