import 'package:event_app/common/design/colors/design_colors.dart';
import 'package:event_app/common/design/margin.dart';
import 'package:event_app/common/di/kiwi_injector.dart';
import 'package:event_app/common/utils/hex_utils.dart';
import 'package:event_app/model/app/filter.dart';
import 'package:flutter/material.dart';

typedef void FilterClickAction(String filterName);

class FiltersItem extends StatefulWidget {
  final String _filterId;
  final String _filterName;
  final Color _color;
  final FilterClickAction _onClick;
  final bool isSelected;

  FiltersItem(this._filterId, this._filterName, this._color, this._onClick, {this.isSelected = false});

  factory FiltersItem.fromFilter(Filter filter,  {FilterClickAction onClick, bool isSelected = false}) => FiltersItem(
    filter.id,
    filter.text,
    HexUtils.convert(filter.color),
    onClick,
    isSelected: isSelected,
  );

  @override
  State<StatefulWidget> createState() => _FiltersItemState();
}

class _FiltersItemState extends State<FiltersItem> with TickerProviderStateMixin {
  final GlobalKey _containerKey = GlobalKey();

  Size _containerSize;

  double _minDiameter = 13;
  double _maxDiameter = 400;
  double _currentDiameter;

  double _minFactor = 1;
  double _maxFactor = 0.7;
  Color _currentColor;

  double _maxOpacity = 1;
  double _minOpacity = 0;
  double _currentOpacity;

  AnimationController _animControl;
  Animation _selectAnimation;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback(_afterLayout);

    _currentDiameter = widget.isSelected ? _maxDiameter : _minDiameter;
    _currentColor = widget._color;
    _currentOpacity = widget.isSelected ? _maxOpacity : _minOpacity;

    super.initState();
  }

  @override
  void didUpdateWidget(FiltersItem oldWidget) {
    if (!oldWidget.isSelected && widget.isSelected) {
      // select
      _animControl.forward();
    } else if (oldWidget.isSelected && !widget.isSelected) {
      //deselect
        _animControl.reverse();
    }

    super.didUpdateWidget(oldWidget);
  }

  void buildAnimation() {
    _animControl = AnimationController(
      value: widget.isSelected ? 1 : 0,
      vsync: this,
      duration: Duration(milliseconds: 300),
    );

    final curve = CurvedAnimation(parent: _animControl, curve: Curves.fastOutSlowIn);

    _selectAnimation = Tween<double>(begin: 0, end: 1).animate(curve)
      ..addListener(() {
        final value = _selectAnimation.value;

        final factor = _minFactor + (_maxFactor - _minFactor) * 2 * (value > 0.5 ? 1 - value : value);
        final color = HexUtils.setBrightness(widget._color, factor);
        final diameter = _minDiameter + (_maxDiameter - _minDiameter) * value;

        setState(() {
          _currentDiameter = diameter;
          _currentColor = color;
          _currentOpacity = value;
        });
      });
  }

  void _afterLayout(_) {
    _containerSize = _containerKey.currentContext.size;
    _maxDiameter = (_containerSize.width - Margin.small) * 2;
    buildAnimation();
  }

  void _onTap() {
    widget._onClick(widget._filterId);
  }

  @override
  Widget build(BuildContext context) {
    final colors = getContainer()<DesignColors>();

    if (_containerSize == null) {
      return _buildContent(colors);
    }

    return GestureDetector(
      onTap: _onTap,
      child: Container(
        width: _containerSize.width + 2,
        height: _containerSize.height + 2,
        decoration: ShapeDecoration(
          shape: StadiumBorder(
            side: BorderSide(
              color: colors.separatorColor,
              width: 1,
            ),
          ),
        ),
        child: Align(
          alignment: Alignment(-1, -1),
          child: ClipPath(
            clipper: ShapeBorderClipper(shape: StadiumBorder()),
            child: _buildContent(colors),
          ),
        ),
      ),
    );
  }

  Widget _buildContent(DesignColors colors) => Padding(
        key: _containerKey,
        padding: EdgeInsets.all(Margin.small),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              width: _minDiameter,
              height: _minDiameter,
              child: Stack(
                children: <Widget>[
                  CustomPaint(
                    size: Size.square(_minDiameter),
                    painter: _CirclePainter(_currentDiameter, _currentColor),
                  ),
                  //todo można usunąć dla opacity = 1
                  Opacity(
                    opacity: _currentOpacity,
                    child: CustomPaint(
                      size: Size.square(_minDiameter),
                      painter: _CloseCirclePainter(_minDiameter, colors),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(width: Margin.half),
            Text(widget._filterName),
          ],
        ),
      );
}

class _CirclePainter extends CustomPainter {
  final double _diameter;
  final Color _color;

  _CirclePainter(this._diameter, this._color);

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = _color
      ..style = PaintingStyle.fill;

    canvas.drawCircle(Offset(size.height / 2, size.width / 2), _diameter / 2, paint);
  }

  @override
  bool shouldRepaint(_CirclePainter oldDelegate) {
    return _diameter != oldDelegate._diameter;
  }
}

class _CloseCirclePainter extends _CirclePainter {
  final DesignColors _colors;
  final _padding = 4.0;
  final _thickness = 2.0;

  _CloseCirclePainter(double diameter, this._colors) : super(diameter, _colors.backgroundColor);

  @override
  void paint(Canvas canvas, Size size) {
    super.paint(canvas, size);

    final paint = Paint()
      ..strokeWidth = _thickness
      ..color = Color(0xFFB5B5B5)
      ..style = PaintingStyle.fill;

    canvas.drawLine(Offset(_padding, _padding), Offset(size.width - _padding, size.height - _padding), paint);
    canvas.drawLine(Offset(size.width - _padding, _padding), Offset(_padding, size.height - _padding), paint);
  }
}
