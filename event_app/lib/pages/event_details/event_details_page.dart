import 'dart:io';

import 'package:event_app/bloc/event_details/rating_bloc.dart';
import 'package:event_app/bloc/speakers/speakers_bloc.dart';
import 'package:event_app/common/architecture/bloc_provider.dart';
import 'package:event_app/common/design/colors/design_colors.dart';
import 'package:event_app/common/design/margin.dart';
import 'package:event_app/common/design/text_styles.dart';
import 'package:event_app/common/di/kiwi_injector.dart';
import 'package:event_app/infrastructure/navigate/navigate.dart';
import 'package:event_app/model/app/event.dart';
import 'package:event_app/model/app/speaker.dart';
import 'package:event_app/pages/agenda/widgets/event_item/event_item_content_widget.dart';
import 'package:event_app/pages/common/my_app_bar.dart';
import 'package:event_app/pages/common/section_header_widget.dart';
import 'package:event_app/pages/common/separator_widget.dart';
import 'package:event_app/pages/event_details/widgets/event_rating_widget.dart';
import 'package:event_app/pages/event_details/widgets/favourites_fab_widget.dart';
import 'package:event_app/pages/speakers/speaker_item/speaker_item_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class EventDetailsPage extends StatelessWidget {
  final Event _event;

  final colors = getContainer()<DesignColors>();

  EventDetailsPage(this._event);

  Widget _buildTitleSection(BuildContext context) => SizedBox(
        width: MediaQuery.of(context).size.width,
        height: 160,
        child: Stack(
          overflow: Overflow.clip,
          children: <Widget>[
            Positioned(
              top: 0,
              right: 0,
              child: SvgPicture.asset('assets/images/background.svg'),
            ),
            Positioned(
              width: MediaQuery.of(context).size.width,
              bottom: Margin.large,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: Margin.edgeMargin),
                child: EventItemContentWidget(_event, showSpeakers: false),
              ),
            ),
            Positioned(bottom: 0, child: SeparatorWidget())
          ],
        ),
      );

  void _openSpeakersPage(BuildContext context, Speaker speaker) {
    SpeakersBloc().getEventsForSpeakerStream(speaker).listen((events) => Navigate.navigate(
          context,
          'speakers/details',
          replaceRoute: ReplaceRoute.none,
          arg: {'speaker': speaker, 'events': events},
        ));
  }

  List<Widget> _buildSpeakersList() {
    final speakers = _event.speakers;
    final List<Widget> result = List();

    if (speakers == null || speakers.isEmpty) {
      result.add(Container());
    } else {
      speakers.forEach((speaker) {
        result.add(SpeakerItemWidget(
          speaker,
          onClick: _openSpeakersPage,
        ));
      });
    }

    return result;
  }

  @override
  Widget build(BuildContext context) {

    final colors = getContainer()<DesignColors>();

    return Scaffold(
      appBar: AppBar(
        title: Text('Agenda'),
       leading: Platform.isAndroid ?  Padding(
            padding: EdgeInsets.only(left: 10, top: 15, bottom: 15),
            child: SvgPicture.asset(
              'assets/images/logo_no_opacity.svg',
              color: colors.logoColor
            ),
          ) : null,
        centerTitle: true,
      ),
      floatingActionButton: FavouritesFabWidget(_event.id),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _buildTitleSection(context),
            Padding(
              padding: EdgeInsets.all(Margin.edgeMargin),
              child: Text(
                _event.description,
                style: TextStyles.description,
              ),
            ),
            SectionHeaderWidget('Ocena', bottomPadding: Margin.large),
            Center(
              child: BlocProvider(child: EventRatingWidget(_event), bloc: RatingBloc()),
            ),
            SizedBox(height: Margin.large),
            SectionHeaderWidget('Prelegenci', bottomPadding: Margin.small),
            Column(
              children: _buildSpeakersList(),
            ),
          ],
        ),
      ),
    );
  }
}
