import 'package:angles/angles.dart';
import 'package:flutter/material.dart';

class ProgressWidget extends StatefulWidget {
  final double _diameter;
  final double _lineWidth;
  final bool _isPending;
  final Color _color;

  ProgressWidget(this._diameter, this._lineWidth, this._isPending, this._color);
  @override
  State<StatefulWidget> createState() => _ProgressWidgetState();
}

class _ProgressWidgetState extends State<ProgressWidget> with TickerProviderStateMixin {
  double _startAngle;
  double _sweepAngle;

  double _lineWidth;
  double _diameter;

  AnimationController _animControl;
  Animation _sweepAnimationA;
  Animation _sweepAnimationB;
  Animation _startAngleAnimationA;
  Animation _startAngleAnimationB;

  AnimationController _exitAnimControl;
  Animation _exitAngleAnimation;
  Animation _exitSweepAnimation;

  bool _isPending;

  _ProgressWidgetState() {
    buildAnimation();
  }

  void buildExitAnimation() {
    _exitSweepAnimation =
        Tween<double>(begin: _sweepAngle, end: 360.0).animate(CurvedAnimation(parent: _exitAnimControl, curve: Curves.fastOutSlowIn))
          ..addListener(() {
            setState(() {
              _sweepAngle = _exitSweepAnimation.value;
            });
          });

    _exitAngleAnimation =
        Tween<double>(begin: _startAngle, end: 360.0).animate(CurvedAnimation(parent: _exitAnimControl, curve: Curves.fastOutSlowIn))
          ..addListener(() {
            setState(() {
              _startAngle = _exitAngleAnimation.value;
            });
          });
  }

  void buildAnimation() {
    _exitAnimControl = AnimationController(
      duration: Duration(milliseconds: 500),
      vsync: this,
      animationBehavior: AnimationBehavior.preserve,
    );

    _animControl = AnimationController(duration: Duration(milliseconds: 2000), vsync: this, animationBehavior: AnimationBehavior.preserve);

    _sweepAnimationA = Tween<double>(begin: .0, end: 360.0)
        .animate(CurvedAnimation(parent: _animControl, curve: Interval(.5, 1, curve: Curves.fastOutSlowIn)))
          ..addListener(() {
            if (_sweepAnimationA.value != .0 && _sweepAnimationA.value != 360.0) {
              setState(() {
                _sweepAngle = _sweepAnimationA.value;
              });
            }
          });

    _sweepAnimationB = Tween<double>(begin: 360.0, end: .0)
        .animate(CurvedAnimation(parent: _animControl, curve: Interval(.0, .5, curve: Curves.fastOutSlowIn)))
          ..addListener(() {
            if (_sweepAnimationB.value != .0 && _sweepAnimationB.value != 360.0) {
              setState(() {
                _sweepAngle = -_sweepAnimationB.value;
              });
            }
          });

    _startAngleAnimationA = Tween<double>(begin: .0, end: 360.0)
        .animate(CurvedAnimation(parent: _animControl, curve: Interval(.5, 1, curve: Curves.fastOutSlowIn)))
          ..addListener(() {
            if (_startAngleAnimationA.value != .0 && _startAngleAnimationA.value != 360.0) {
              setState(() {
                _startAngle = _startAngleAnimationA.value;
              });
            }
          });

    _startAngleAnimationB = Tween<double>(begin: .0, end: 360.0)
        .animate(CurvedAnimation(parent: _animControl, curve: Interval(.0, .5, curve: Curves.fastOutSlowIn)))
          ..addListener(() {
            if (_startAngleAnimationB.value != .0 && _startAngleAnimationB.value != 360.0) {
              setState(() {
                _startAngle = _startAngleAnimationB.value;
              });
            }
          });
  }

  Future<void> startExitAnimation() async {
    try {
      await _exitAnimControl.forward(from: 0).orCancel;
    } on TickerCanceled {
      // the animation got canceled, probably because we were disposed
    }
  }

  Future<void> startAnimation() async {
    try {
      await _animControl.repeat().orCancel;
    } on TickerCanceled {
      // the animation got canceled, probably because we were disposed
    }
  }

  void stopAnimation() {
    _isPending = false;
    _animControl.stop();
  }

  void updateWidgetParameters() {
    _isPending = widget._isPending;
    _diameter = widget._diameter;
    _lineWidth = widget._lineWidth;
  }

  @override
  void didUpdateWidget(ProgressWidget oldWidget) {
    final oldIsPending = oldWidget._isPending;
    final newIsPending = widget._isPending;

    //debugPrint("PROGRESS WIDGET: $oldIsPending --- $newIsPending");

    if (newIsPending && !oldIsPending) {
      startAnimation();
    } else if (oldIsPending && !newIsPending) {
      stopAnimation();
      buildExitAnimation();
      startExitAnimation();
    }

    setState(() {
      updateWidgetParameters();
    });

    super.didUpdateWidget(oldWidget);
  }

  @override
  void initState() {
    setState(() {
      _startAngle = 270;
      _sweepAngle = 360;
      updateWidgetParameters();
      if (_isPending) {
        startAnimation();
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    stopAnimation();
    _exitAnimControl.stop();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: _ProgressPaint(_startAngle, _sweepAngle, _lineWidth, widget._color),
      size: Size(widget._diameter, widget._diameter),
    );
  }
}

class _ProgressPaint extends CustomPainter {
  Angle _startAngle;
  Angle _sweepAngle;
  final double _width;
  final Color _color;

  _ProgressPaint(double startAngle, double sweepAngle, this._width, this._color) {
    _startAngle = Angle.fromDegrees(startAngle);
    _sweepAngle = Angle.fromDegrees(sweepAngle);
  }

  @override
  void paint(Canvas canvas, Size size) {
    final _paint = Paint()
      ..color = _color
      ..strokeWidth = _width
      ..style = PaintingStyle.stroke;

    final rect = Rect.fromLTWH(0, 0, size.width, size.height);
    canvas.drawArc(rect, _startAngle.radians, _sweepAngle.radians, false, _paint);
  }

  @override
  bool shouldRepaint(_ProgressPaint oldDelegate) {
    return _startAngle != oldDelegate._startAngle || _sweepAngle != oldDelegate._sweepAngle || _width != oldDelegate._width;
  }
}
