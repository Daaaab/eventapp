import 'package:event_app/common/design/text_styles.dart';
import 'package:event_app/pages/event_details/widgets/progress_widget.dart';
import 'package:flutter/material.dart';

typedef void RatingClickAction(int rate);

class RatingButtonWidget extends StatefulWidget {
  final int _rate;

  final bool _isSelected;
  final bool _isPending;

  final RatingClickAction _action;

  final Color _backgroundColor;
  final Color _selectedColor;

  final double maxProgressDiameter;
  final double buttonDiameter;

  RatingButtonWidget(
    this._rate,
    this._isSelected,
    this._isPending,
    this._backgroundColor,
    this._selectedColor,
    this._action, {
    this.maxProgressDiameter = 50,
    this.buttonDiameter = 50,
  });

  @override
  State<StatefulWidget> createState() => _RatingButtonWidgetState();
}

class _RatingButtonWidgetState extends State<RatingButtonWidget> with TickerProviderStateMixin {
  bool _isPressedDown;
  //todo pending i selected chyba nie powinno tu być
  bool _isPending;
  bool _isSelected;
  double _progressDiameter;

  double _selectedBackgroundDiameter;

  AnimationController _animControl;
  Animation _shrinkProgressAnim;

  void buildAnimation() {
    _animControl = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 500),
    );

    _shrinkProgressAnim = Tween<double>(begin: .0, end: 1).animate(CurvedAnimation(parent: _animControl, curve: Curves.fastOutSlowIn))
      ..addListener(() {
        setState(() {
          _selectedBackgroundDiameter = widget.buttonDiameter * _shrinkProgressAnim.value;
        });
      });
  }

  void isPressedDown(bool value) {
    setState(() {
      _isPressedDown = value;
    });
  }

  void onRatingTap() {
    setState(() {
      isPressedDown(false);
      if (!_isPending) {
        widget._action(widget._rate);
      }
    });
  }

  Future finishLoading() async {
    await _animControl.forward(from: 0);
  }

  @override
  void didUpdateWidget(RatingButtonWidget oldWidget) {
    setState(() {
      _isSelected = widget._isSelected;
      _isPending = widget._isPending;

      //Selected -> Unselected
      if (oldWidget._isSelected && !_isSelected) {
        resetProgress();
      }

      //Pending -> Selected
      if (oldWidget._isPending && !oldWidget._isSelected && widget._isSelected) {
        finishLoading();
      } else if (!oldWidget._isSelected && widget._isSelected) {
        _selectedBackgroundDiameter = widget.buttonDiameter;
      }
    });

    super.didUpdateWidget(oldWidget);
  }

  void resetProgress() {
    _progressDiameter = widget.maxProgressDiameter;
    _selectedBackgroundDiameter = _isSelected ? widget.buttonDiameter : 0;
  }

  @override
  void dispose() {
    _animControl.dispose();
    super.dispose();
  }

  @override
  void initState() {
    setState(() {
      _isSelected = widget._isSelected;
      _isPending = widget._isPending;
      _isPressedDown = false;
      resetProgress();
    });
    buildAnimation();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return PhysicalModel(
      color: widget._backgroundColor,
      shape: BoxShape.circle,
      elevation: _isPressedDown ? _RaitingButtonElevation.pressed : _RaitingButtonElevation.rised,
      child: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTapDown: (_) => isPressedDown(true),
        onTap: onRatingTap,
        onTapCancel: () => isPressedDown(false),
        child: SizedBox(
          width: widget.buttonDiameter,
          height: widget.buttonDiameter,
          child: Stack(
            children: <Widget>[
              Center(
                child: CustomPaint(
                  painter: _SelectRevealPainter(_selectedBackgroundDiameter, widget._selectedColor),
                  size: Size(widget.buttonDiameter, widget.buttonDiameter),
                ),
              ),
              Center(
                child: Text(
                  widget._rate.toString(),
                  style: _isSelected ? TextStyles.ratingSelected : TextStyles.rating,
                ),
              ),
              Center(
                child: ProgressWidget(_progressDiameter, 2, _isPending, widget._selectedColor),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class _SelectRevealPainter extends CustomPainter {
  final double _diameter;
  final Color _color;

  _SelectRevealPainter(this._diameter, this._color);

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = _color
      ..style = PaintingStyle.fill;

    canvas.drawCircle(Offset(size.height / 2, size.width / 2), _diameter / 2, paint);
  }

  @override
  bool shouldRepaint(_SelectRevealPainter oldDelegate) {
    return _diameter != oldDelegate._diameter;
  }
}

class _RaitingButtonElevation {
  static const rised = .0;
  static const pressed = .0;
}
