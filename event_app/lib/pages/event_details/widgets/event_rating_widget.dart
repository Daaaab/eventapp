import 'dart:async';

import 'package:event_app/bloc/event_details/rating_bloc.dart';
import 'package:event_app/common/architecture/bloc_provider.dart';
import 'package:event_app/common/design/colors/design_colors.dart';
import 'package:event_app/common/design/margin.dart';
import 'package:event_app/common/di/kiwi_injector.dart';
import 'package:event_app/model/app/event.dart';
import 'package:event_app/model/app/rating.dart';
import 'package:event_app/pages/event_details/widgets/rating_button_widget.dart';
import 'package:flutter/material.dart';

class EventRatingWidget extends StatefulWidget {
  final Event _event;

  final Color _buttonBackground = getContainer()<DesignColors>().backgroundColor;
  final Color _selectedButton = getContainer()<DesignColors>().accentColor;

  final double _buttonsSize = 45;
  final double _progressSize = 45;

  final double _buttonsPadding = 15;

  EventRatingWidget(this._event);

  @override
  State<StatefulWidget> createState() => _EventRatingWidgetState();
}

class _EventRatingWidgetState extends State<EventRatingWidget> {
  RatingBloc _ratingBloc;
  final _commentControl = TextEditingController();

  void _onRatingClick(int rate) {
    if (_ratingBloc.isAfterEventStart(widget._event)) {
      _popRatingUnavailableDialog();
      return;
    }

    final currentRate = _ratingBloc.getCurrentRatingForEvent(widget._event.id);

    _commentControl.text = currentRate?.comment;

    _popDialog(rate).then((resultComment) {
      final comment = resultComment;
      if (comment != null &&
          (currentRate == null || currentRate.status != Rating.pending) &&
          (rate != currentRate?.rate || comment != currentRate?.comment)) {
        _ratingBloc.setRating(widget._event, rate, comment ?? "");
      }
    });
  }

  Future<String> _popDialog(int rate) => showDialog<String>(
        context: context,
        builder: (context) => AlertDialog(
              title: Text('Ocena: ${rate.toString()}'),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text('Twój Feedback:'),
                  TextField(
                    controller: _commentControl,
                  )
                ],
              ),
              actions: <Widget>[
                FlatButton(
                  child: const Text('Anuluj'),
                  onPressed: () {
                    Navigator.pop(context, null);
                  },
                ),
                FlatButton(
                  child: const Text('Wyślij'),
                  onPressed: () {
                    Navigator.pop(context, _commentControl.text);
                  },
                ),
              ],
            ),
      );

  Future<String> _popRatingUnavailableDialog() {
    if (!_ratingBloc.isAfterEventStart(widget._event)) {
      _ratingBloc.removeBlocked();
    }

    return Future.delayed(Duration.zero).then((_) => showDialog<String>(
        context: context,
        builder: (context) => AlertDialog(
              title: Text('Ocenianie niedostępne'),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text('Możliwość oceny tego wydarzenia będzie dostępna po jego rozpoczęciu.'),
                ],
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text('OK'),
                  onPressed: () {
                    Navigator.pop(context, null);
                  },
                )
              ],
            )));
  }

  List<Widget> _buildButtons(Rating rating) {
    List<Widget> result = List();

    if (rating != null && rating.status == Rating.disabled) {
      _popRatingUnavailableDialog();
    }

    for (int i = 0; i < 5; i++) {
      final rate = i + 1;
      final isSelected = rating != null && rating.rate == i + 1 && rating.status == Rating.sent;
      final isPending = rating != null && rating.rate == i + 1 && (rating.status == Rating.resend || rating.status == Rating.resending || rating.status == Rating.pending || rating.status == Rating.failed);

      result.add(
        RatingButtonWidget(
          rate,
          isSelected,
          isPending,
          widget._buttonBackground,
          widget._selectedButton,
          _onRatingClick,
          buttonDiameter: widget._buttonsSize,
          maxProgressDiameter: widget._progressSize,
        ),
      );
      if (i != 5) {
        result.add(
          SizedBox(width: widget._buttonsPadding),
        );
      }
    }
    return result;
  }

  bool _showError(Rating rating)=> rating != null && (rating.status == Rating.failed || rating.status == Rating.resend || rating.status == Rating.resending);

  @override
  void initState() {
    _ratingBloc = BlocProvider.of<RatingBloc>(context);
    super.initState();
  }

  @override
  void dispose() {
    debugPrint("_EventRatingWidgetState dispose()");
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final colors = getContainer()<DesignColors>();

    return StreamBuilder<Rating>(
      initialData: _ratingBloc.getCurrentRatingForEvent(widget._event.id),
      stream: _ratingBloc.getRatingStreamForEvent(widget._event.id),
      builder: (context, snapshot) {
        final rating = snapshot.data;
        return Center(
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: _buildButtons(rating),
              ),
              _showError(rating) ? Column(
                children: <Widget>[
                  SizedBox(height: Margin.medium),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: Margin.edgeMargin),
                    child: Text(
                      'Błąd połączenia :(\nWkrótce ponowimy próbę wysłania oceny.',
                      textAlign: TextAlign.center,
                      style: TextStyle(color: colors.textErrorColor),
                    ),
                  ),
                ],
              ) : Container(),
            ],
          ),
        );
      },
    );
  }
}
