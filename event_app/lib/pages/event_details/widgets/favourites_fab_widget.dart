import 'package:event_app/bloc/agenda/favs_bloc.dart';
import 'package:event_app/common/design/colors/design_colors.dart';
import 'package:event_app/common/design/margin.dart';
import 'package:event_app/common/di/kiwi_injector.dart';
import 'package:flutter/material.dart';

class FavouritesFabWidget extends StatefulWidget{

  final String _eventId;

  FavouritesFabWidget(this._eventId);

  @override
  State<StatefulWidget> createState() => _FavouritesFabWidgetState();

}

class _FavouritesFabWidgetState extends State<FavouritesFabWidget>{

  FavsBloc _favsBloc;

  @override
  void initState() {

    _favsBloc = FavsBloc(widget._eventId);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    final colors = getContainer()<DesignColors>();

    return StreamBuilder(
      initialData: _favsBloc.initialIsFav(widget._eventId),
      stream: _favsBloc.isFav(widget._eventId),
      builder: (context, snapshot){
        final isFav = snapshot.hasData ? snapshot.data : false;
        final starIcon = isFav ? Icons.star : Icons.star_border;

        return Padding(
          padding: EdgeInsets.only(bottom: Margin.large),
          child: FloatingActionButton(
            onPressed: () => _favsBloc.setFav(widget._eventId, !isFav),
            child: Icon(starIcon),
            backgroundColor: colors.accentColor,
          ),
        );
      },
    );
  }
}