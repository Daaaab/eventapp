import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class UiTestPage extends StatefulWidget {
  @override
  _UiTestPageState createState() => _UiTestPageState();
}

class _UiTestPageState extends State<UiTestPage> {
  @override
  Widget build(BuildContext context) {
    Completer<GoogleMapController> _controller = Completer();

    return Scaffold(
      body: GoogleMap(
          myLocationEnabled: false,
          mapType: MapType.normal,
          onMapCreated: (GoogleMapController controller) {
            _controller.complete(controller);
          },
          initialCameraPosition: CameraPosition(target: LatLng(50.2903716, 19.1239551), zoom: 15)),
    );
  }
}
