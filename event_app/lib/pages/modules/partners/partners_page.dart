import 'package:event_app/common/config/rest_config.dart';
import 'package:event_app/common/design/colors/design_colors.dart';
import 'package:event_app/common/design/margin.dart';
import 'package:event_app/common/di/kiwi_injector.dart';
import 'package:event_app/model/app/partner.dart';
import 'package:event_app/model/app/partner_module.dart';
import 'package:event_app/pages/common/section_header_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:flutter_advanced_networkimage/transition.dart';
import 'package:flutter_svg/svg.dart';

class PartnersPage extends StatelessWidget {
  final PartnerModule _module;

  PartnersPage(this._module);

  double _calculateItemHeight(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final edgeMargin = Margin.edgeMargin * 2;
    final itemSpacing = Margin.medium;

    return (screenWidth - edgeMargin - itemSpacing) / 2;
  }

  List<Widget> _buildLargeItems(List<Partner> partners, BuildContext context) {
    List<Widget> result = List();

    final height = _calculateItemHeight(context);

    partners.forEach((partner) {
      //todo uncomment
      final photoUri = Uri.https(RestConfig.backendPath, RestConfig.getImage, {'type': 'partner', 'name': partner.photo});

      final imageWidget = Padding(
        padding: EdgeInsets.symmetric(horizontal: Margin.edgeMargin),
        child: TransitionToImage(
          height: height,
          image: AdvancedNetworkImage(
            photoUri.toString(),
            useDiskCache: true,
          ),
          fit: BoxFit.contain,
          transitionType: TransitionType.fade,
          enableRefresh: false,
          loadingWidget: Container(
            child: Padding(
              padding: const EdgeInsets.all(Margin.medium),
              child: SvgPicture.asset(
                'assets/images/background.svg',
              ),
            ),
          ),
        ),
      );

      result.add(imageWidget);
    });

    return result;
  }

  List<Widget> _buildSmallItems(List<Partner> partners, BuildContext context) {
    List<Widget> result = List();

    final colors = getContainer()<DesignColors>();

    final height = _calculateItemHeight(context);

    partners.forEach((partner) {
      final photoUri = Uri.https(RestConfig.backendPath, RestConfig.getImage, {'type': 'partner', 'name': partner.photo});

      final imageWidget = Container(
        decoration: BoxDecoration(
          border: Border.all(
            color: colors.separatorColor,
            width: 1,
            style: BorderStyle.solid,
          )
        ),
        child: SizedBox(
          height: height,
          width: height,
          child: Padding(
            padding: EdgeInsets.all(Margin.large),
            child: TransitionToImage(
              height: height,
              image: AdvancedNetworkImage(
                photoUri.toString(),
                useDiskCache: true,
              ),
              fit: BoxFit.contain,
              transitionType: TransitionType.fade,
              enableRefresh: false,
              loadingWidget: Container(
                child: Padding(
                  padding: const EdgeInsets.all(Margin.medium),
                  child: SvgPicture.asset(
                    'assets/images/background.svg',
                  ),
                ),
              ),
            ),
          ),
        ),
      );
      result.add(imageWidget);
    });

    return result;
  }

  bool _isLarge(List<Partner> partners) {
    final result = partners[0].isLarge;

    assert(partners.every((test) => test.isLarge == result));

    return result;
  }

  Widget _buildItem(BuildContext context, int index) {
    final item = _module.partnersList.entries.elementAt(index);

    final isLarge = _isLarge(item.value);
    final widgets = isLarge ? _buildLargeItems(item.value, context) : _buildSmallItems(item.value, context);

    if (isLarge) {
      widgets.insert(0, SectionHeaderWidget(item.key));
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: widgets,
      );
    } else {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          SectionHeaderWidget(item.key),
          GridView.count(
            padding: EdgeInsets.only(left: Margin.edgeMargin, right: Margin.edgeMargin, bottom: Margin.medium),
            crossAxisSpacing: Margin.medium,
            mainAxisSpacing: Margin.medium,
            shrinkWrap: true,
            physics: new NeverScrollableScrollPhysics(),
            crossAxisCount: 2,
            children: widgets,
          ),
        ],
      );
    }
  }

  @override
  Widget build(BuildContext context) => ListView.builder(
        primary: true,
        itemCount: _module.partnersList.length,
        itemBuilder: _buildItem,
      );
}
