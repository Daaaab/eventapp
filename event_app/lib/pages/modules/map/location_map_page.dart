import 'package:event_app/common/config/rest_config.dart';
import 'package:event_app/common/design/margin.dart';
import 'package:event_app/model/app/location_map_module.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:flutter_advanced_networkimage/transition.dart';
import 'package:flutter_svg/svg.dart';
import 'package:pinch_zoom_image/pinch_zoom_image.dart';

class LocationMapPage extends StatelessWidget{

  final LocationMapModule module;

  const LocationMapPage({Key key, this.module}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    final mapUri = Uri.https(RestConfig.backendPath, RestConfig.getImage, {'type': 'article', 'name': module.mapFile});

    return Center(
      child: PinchZoomImage(
        hideStatusBarWhileZooming: true,
          image: TransitionToImage(
            image: AdvancedNetworkImage(
              mapUri.toString(),
              useDiskCache: true,
            ),
            fit: BoxFit.contain,
            transitionType: TransitionType.fade,
            enableRefresh: false,
            loadingWidget: Container(
              height: 160,
              width: double.infinity,
              child: Padding(
                padding: const EdgeInsets.all(Margin.medium),
                child: SvgPicture.asset(
                  'assets/images/background.svg',
                ),
              ),
            ),
          ),
      ),
    );
  }
}