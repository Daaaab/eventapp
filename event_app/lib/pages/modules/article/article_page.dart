import 'package:event_app/common/config/rest_config.dart';
import 'package:event_app/common/design/margin.dart';
import 'package:event_app/common/design/text_styles.dart';
import 'package:event_app/model/app/article_module.dart';
import 'package:event_app/pages/common/section_header_widget.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_advanced_networkimage/transition.dart';
import 'package:url_launcher/url_launcher.dart';

//todo czy nie lepiej pobierać moduły dopiero po wejściu w nie ?
class ArticlePage extends StatelessWidget {
  static const type = "article";

  final ArticleModule _article;

  ArticlePage(this._article);

  Widget _getPhotoWidget() {
    if (_article.photo != null && _article.photo.isNotEmpty) {
      final photoUri = Uri.https(RestConfig.backendPath, RestConfig.getImage, {'type': 'article', 'name': _article.photo});

      return TransitionToImage(
        image: AdvancedNetworkImage(
          photoUri.toString(),
          useDiskCache: true,
        ),
        fit: BoxFit.contain,
        transitionType: TransitionType.fade,
        enableRefresh: false,
        loadingWidget: Container(
          height: 160,
          width: double.infinity,
          child: Padding(
            padding: const EdgeInsets.all(Margin.medium),
            child: SvgPicture.asset(
              'assets/images/background.svg',
            ),
          ),
        ),
      );
    } else {
      return Container();
    }
  }

  List<Widget> _buildLinks() => _article.links
      .asMap()
      .map((index, link) => MapEntry(
            index,
            Padding(
              padding: const EdgeInsets.only(bottom: Margin.small),
              child: RichText(
                text: TextSpan(
                  text: '${index + 1}. ${link.text}',
                  style: new TextStyle(color: Colors.blue),
                  recognizer: new TapGestureRecognizer()..onTap = () => launch(link.link),
                ),
              ),
            ),
          ))
      .values
      .toList();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _getPhotoWidget(),
          SizedBox(height: Margin.medium),
          SectionHeaderWidget(_article.title),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: Margin.edgeMargin),
            child: Text(_article.content, style: TextStyles.description),
          ),
          Padding(
            padding: const EdgeInsets.only(top: Margin.medium),
            child: (_article.links != null && _article.links.isNotEmpty) ? SectionHeaderWidget('Linki') : Container(),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: Margin.edgeMargin),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: _buildLinks(),
            ),
          ),
          SizedBox(height: Margin.edgeMargin),
        ],
      ),
    );
  }
}
