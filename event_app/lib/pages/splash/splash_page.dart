import 'dart:io';

import 'package:event_app/bloc/splash/splash_bloc.dart';
import 'package:event_app/common/architecture/bloc_provider.dart';
import 'package:event_app/common/config/config.dart';
import 'package:event_app/common/design/colors/design_colors.dart';
import 'package:event_app/common/di/kiwi_injector.dart';
import 'package:event_app/infrastructure/navigate/navigate.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SplashPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SplashPageState();
  }
}

class _SplashPageState extends State<SplashPage> {
  SplashBlock _splashBlock;

  //  void iOS_Permission() {
  //    FirebaseMessaging().requestNotificationPermissions(
  //        IosNotificationSettings(sound: true, badge: true, alert: true)
  //    );
  //    FirebaseMessaging().onIosSettingsRegistered
  //        .listen((IosNotificationSettings settings)
  //    {
  //      _openNextPage();
  //      print("Settings registered: $settings");
  //    });
  //  }
  //
  //  void _onNextPress(BuildContext context) {
  //    if(Platform.isIOS){
  //      iOS_Permission();
  //    }else{
  //      _openNextPage();
  //    }
  //
  //  }

  @override
  void initState() {
    _splashBlock = BlocProvider.of<SplashBlock>(context);

   _startInit();

    super.initState();
  }

  void _startInit() {
    _splashBlock.initStream.listen((isFirstRun) {
      debugPrint('SplashPage: initComplete');
      openNextPage(isFirstRun);
    }, onError: (err) {
      debugPrint('SplashPage: init error: $err');
      openNextPage(true);
    });
  }

  void openNextPage(bool isFirstRun) {
    if (isFirstRun || Config.alwaysShowWelcome) {
      Navigate.navigate(
        context,
        'welcome',
        replaceRoute: ReplaceRoute.all,
      );
    } else {
      Navigate.navigate(
        context,
        'main',
        replaceRoute: ReplaceRoute.all,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final colors = getContainer()<DesignColors>();

    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              color: colors.accentColor,
            ),
            child: Center(
              child: SvgPicture.asset(
                'assets/images/logo.svg',
//                width: 219,
//                height: 68,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
