import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'

//todo dodać parametr Last Modified
export const getImage = functions.https.onRequest((request, response) => {
    const query = request.query
    let type = query.type
    let name = query.name as string
    const split = name.split('.')[1]
    if (split.length <= 1) {
        name += '.png'
    }

    switch (type) {
        case 'speakers': {
            type = 'speakers_photo/'
            break;
        }
        case 'article': {
            type = 'article_photo/'
            break;
        }
        case 'partner': {
            type = 'partners/'
            break;
        }
        case 'undefined':
        default: {
            type = "speakers_photo/";
            name = "photo1.png"
        };
    }

    const path = type + name;

    admin
        .storage()
        .bucket('eventaapp-5d3a4.appspot.com')
        .file(path)
        .download()
        .then((result) => {
            response.setHeader('Content-Type', 'image/png');
            response.send(result[0]);
        })
        .catch((err) => {
            response.status(500).send("error: " + err);
        });
})