// export const move = functions.https.onRequest((request, response) => {
//     Promise.all([
//         getCollectionPromise('events'),
//         getCollectionPromise('days'),
//         getCollectionPromise('speakers')
//     ])
//         .then((result) => {
//             const eventsList = result[0].docs
//             //const daysList = result[1].docs.map<DayModel>(doc => new DayModel(doc))
//             //const speakersList = result[2].docs.map<SpeakerModel>(doc => new SpeakerModel(doc))

        
//             const speakersListRef = admin.firestore().collection('agenda/events/eventsList')

//             eventsList.forEach(event => {

//                 speakersListRef.doc(event.id).set({
//                     'title': event.get('title'),
//                     'dayId': event.get('dayId'),
//                     'description': event.get('description'),
//                     'startTime': event.get('startTime'),
//                     'endTime': event.get('endTime'),
//                     'speakers': event.get('speakers')
//                 })
//             })

//             response.send('OK')
//         })
//         .catch(err => {
//             console.log(err)
//             response.status(500).send(err)
//         })
// })

// export const getEvent = functions.https.onRequest((request, response) => {
//     const eventId = request.query.eventId
    
//     admin.firestore().doc('agenda/events/eventsList/'+eventId).get()
//     .then((result) => {
//         response.send({
//             'title' : result.get('title'),
//             'description' : result.get('description'),
//             'dayId' : (result.get('dayId') as DocumentReference).path,
//             'location' : (result.get('location') as DocumentReference).path,
//             'startTime' : (result.get('startTime') as Timestamp).toDate(),
//             'endTime' : (result.get('endTime') as Timestamp).toDate(),
//             'speakers' : (result.get('speakers') as DocumentReference[]).map( ref => ref.path)
//         })
//     })
//     .catch((err) => {
//         response.status(500).send('Error: ' + err)
//     })
// })

// export const addField = functions.https.onRequest((request, response) => {
//     const body = request.body
    
//     // change field type
//     const value = admin.firestore().doc(body.fieldValue)

//     // change location
//     admin.firestore().collection('agenda/events/eventsList').get()
//         .then((eventsQuery) => {

//             const sets: Promise<WriteResult>[] = new Array()

//             eventsQuery.forEach((eventDoc) => {
//                 const promise = eventDoc.ref.set(
//                     {
//                         // change field name
//                         'group' : value
//                     }, 
//                     {merge : true}
//                 )
//                 sets.push(promise)
//             })

//             return Promise.all(sets)
//         })
//         .then(result => {
//             response.send('OK')
//         })
//         .catch((err) => {
//             response.status(500).send("Error: " + err)
//         })
// })

// export const addEvent = functions.https.onRequest((request, response) => {
//     const body = request.body

//     const requestSpeakers = body.speakers as string[]
//     const speakersRefs = requestSpeakers.map<DocumentReference>((speakerId) => 
//         admin.firestore().doc(speakerId)
//     )

//     admin.firestore().collection('agenda/events/eventsList').add(
//         {
//             'title' : body.title,
//             'description' : body.description,
//             'dayId' : admin.firestore().doc(body.dayId),
//             'location' : admin.firestore().doc(body.location),
//             'startTime' : new Date(Date.parse(body.startTime)),
//             'endTime' : new Date(Date.parse(body.endTime)),
//             'speakers' : speakersRefs
//         }
//     )
//     .then(result => {
//         response.send('New event id: ' + result.id)
//     })
//     .catch((err) => {
//         response.status(500).send("Error: " + err)
//     })
// })