import { DocumentReference, QueryDocumentSnapshot, Timestamp, QuerySnapshot } from '@google-cloud/firestore';

export abstract class Module{
    order : number
    type : string
    label: string

    constructor(docSnap : QueryDocumentSnapshot){
        this.order = docSnap.get('order')
        this.type = docSnap.get('type')
        this.label = docSnap.get('label')
    }
}

export class MapModule extends Module{

    mapFile : String

    constructor(docSnap: QueryDocumentSnapshot){
        super(docSnap)
        
        this.mapFile = docSnap.get('mapFile')
    }
}

export class LocationModule extends Module{

    points : Array<any>

    constructor(docSnap: QueryDocumentSnapshot){
        super(docSnap)
        
        this.points = docSnap.get('points')
    }
}

export class PartnersModule extends Module{
    partners: Array<any>

    constructor(docSnap: QueryDocumentSnapshot){
        super(docSnap)

        this.partners = docSnap.get('partnersList')
    }
}

export class ArticleModule extends Module{
    content : string
    title : string
    photo: string
    links: string

    constructor(docSnap : QueryDocumentSnapshot){
        super(docSnap);
        
        this.photo = docSnap.get('photo')
        this.content = docSnap.get('content')
        this.title = docSnap.get('title')
        this.links = docSnap.get('links')
    }
}

export class GetModuleResponse {
    modules : Array<Module>

    constructor(modulesSnap : QuerySnapshot){
        this.modules = new Array()

        modulesSnap.docs.forEach((docSnap) => {
            const type =  docSnap.get('type') as string

            switch(type){
                case 'article': {
                    this.modules.push(new ArticleModule(docSnap))
                    break;
                }
                case 'partners' : {
                    this.modules.push(new PartnersModule(docSnap))
                    break;
                }
                case 'location_map' :{
                    this.modules.push(new MapModule(docSnap))
                    break;
                }
                case 'gps' : {
                    this.modules.push(new LocationModule(docSnap))
                    break;
                }
            }
        })
    }
}

export class GetAgendaResponse {
    events: Array<EventModel>
    days: Array<DayModel>
    speakers: Array<SpeakerModel>
    groups: Array<GroupModel>

    constructor(events: Array<EventModel>, days: Array<DayModel>, speakers: Array<SpeakerModel>, groups: Array<GroupModel>) {
        this.events = events
        this.days = days
        this.speakers = speakers
        this.groups = groups
    }
}

export class GroupModel{
    id: string
    name: string
    order: number

    constructor(doc: QueryDocumentSnapshot){
        this.id = doc.id
        this.name = doc.get('name')
        this.order = doc.get('order')
    }
}

export class LocationModel{
    id: string
    name: string

    constructor(doc: QueryDocumentSnapshot){
        this.id = doc.id
        this.name = doc.get('name')
    }
}

export class SpeakerModel{
    name: string
    surname: string
    id: string
    title: string
    company: string
    description: string
    photo: string
    featured:boolean

    constructor(doc: QueryDocumentSnapshot){
        this.name = doc.get('name')
        this.surname = doc.get('surname')
        this.title = doc.get('title')
        this.company = doc.get('company')
        this.description = doc.get('description')
        this.photo = doc.get('photo')
        const featured = doc.get('featured')
        
        if(typeof featured === 'undefined'){
            this.featured = false
        }else{
            this.featured = featured
        }
        this.id = doc.id
    }
}

export class CategoryModel{
    name: string
    color: string
    id: string

    constructor(doc: QueryDocumentSnapshot){
        this.name = doc.get('name')
        this.color = doc.get('color')
        this.id = doc.id
    }
}

export class DayModel{
    name: string
    order: number
    id: string

    constructor(doc: QueryDocumentSnapshot){
        this.name = doc.get('name')
        this.order = doc.get('order')
        this.id = doc.id
    }
}

export class EventModel{
    id: string
    title: string
    dayId: string
    description: string
    startTime: string
    endTime: string
    location: string
    group: string
    category: CategoryModel
    speakers: Array<SpeakerModel>

    constructor(doc: QueryDocumentSnapshot, speakers: Array<SpeakerModel>, locations: Array<LocationModel>, categories: Array<CategoryModel>){
        this.id = doc.id
        this.title = doc.get('title')
        this.description = doc.get('description')
        
        this.startTime = (doc.get('startTime') as Timestamp).toDate().toISOString() 
        this.endTime = (doc.get('endTime') as Timestamp).toDate().toISOString() 

        const categoryId = (doc.get('category') as DocumentReference).id
        this.category = (categories.find(x => x.id === categoryId) as CategoryModel)

        const mapped = (doc.get('speakers') as Array<DocumentReference>).map(ref => ref.id)
        .map(speakerId => 
            speakers.find(x => x.id === speakerId)
        )
        .filter(value => {
            return value !== undefined
        })
        
        this.speakers = mapped as SpeakerModel[]

        const locationId = doc.get('location').id
        this.location = (locations.find((location) => location.id === locationId) as LocationModel).name
        
        const groupRef: DocumentReference = doc.get('group')
        this.group = groupRef.id

        const dayIdRef:DocumentReference = doc.get('dayId')
        this.dayId = dayIdRef.id
    }
}