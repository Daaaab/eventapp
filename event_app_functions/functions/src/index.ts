import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'
import * as csv from 'csvtojson'
import { EventModel, SpeakerModel, LocationModel, GetModuleResponse, CategoryModel } from './model'
import { QuerySnapshot, DocumentReference, Timestamp, WriteResult, FieldValue } from '@google-cloud/firestore';
import { calculateChecksum } from './rating_checksum'
import { Readable } from 'stream';
import firestore = require('@google-cloud/firestore');

admin.initializeApp()

/*
firebase deploy
firebase serve --only functions
npm run build
*/


// export const getRatingIntervals = functions.https.onRequest((request, response) => {

//     const options: Intl.DateTimeFormatOptions = {
//         hour12: false,
//         day: '2-digit', month: '2-digit', year: '2-digit',
//         hour: '2-digit', minute: '2-digit', second: '2-digit',
//     }

//     getCollectionPromise('ratings/ratings/ratingsList')
//     .then(result => {
//         return result.docs.map(rate => {
//             return rate.createTime.toDate()
//         })
//     })
//     .then(result => {
//         const sorted = result.sort()
//         //8.06.19, 16:58:03"
//         const first = Date.parse("8.06.19, 17:00:00")
//         do{
            
//         }while(sorted.length != 0)
//         return sorted;
//     })
//     .then(result => {
//         return result.map(time => {
//             return new Intl.DateTimeFormat('pl-PL', options).format(time)
//         })
//     })
//     .then(result => {
//         response.send(result)
//     })
// })

export const getRatings = functions.https.onRequest((request, response) => {

    Promise.all(
        [
            getCollectionPromise('agenda'),
            getCollectionPromise('ratings/ratings/ratingsList')
        ]
    )
        .then(result => {
            const events = getList(result[0], 'events', 'eventsList')
            const speakers = getList(result[0], 'speakers', 'speakersList')

            events.forEach(event => {
                const speakerIds = event.speakers as Array<string>
                if (speakerIds.length > 0) {
                    const speakersList = speakerIds.map(speakerId => findObjectById(speakers, speakerId))
                    event.speakers = speakersList
                }

                event.title = event.title.trim()

            })

            return [
                events,
                result[1]
            ]
        })
        .then(result => {
            const agenda = result[0] as Array<any>
            const ratings = result[1] as QuerySnapshot

            const options: Intl.DateTimeFormatOptions = {
                hour12: false,
                day: '2-digit', month: '2-digit', year: '2-digit',
                hour: '2-digit', minute: '2-digit', second: '2-digit',
            }

            return ratings.docs.map(rate => {

                const ratedEvent = agenda.find(event => event.id === rate.get("eventId"))
                const speakers = ratedEvent.speakers as Array<any>
                let speakersText = ""

                console.log(rate.updateTime.toDate().toLocaleDateString())

                speakers.forEach((speaker, index) => {
                    const name = speaker.name + " " + speaker.surname
                    speakersText += name
                    if (index < speakers.length - 1) {
                        speakersText += ', '
                    }
                })

                const createTime = rate.createTime.toDate()

                return {
                    rate: rate.get("rating"),
                    comment: rate.get("comment"),
                    eventId: ratedEvent.title,
                    speakers: speakersText,
                    uuid: rate.get("uuid"),
                    created: new Intl.DateTimeFormat('pl-PL', options).format(createTime),
                }
            })
        })
        .then(res => response.send(res))
        .catch((err) => {
            response.status(500).send('Error getRatings(): ' + err)
        })
})

export const getAgenda = functions.https.onRequest((request, response) => {
    getCollectionPromise('agenda')
        .then((result) => {
            const categories = getList(result, 'categories', 'categoriesList')
            const events = getList(result, 'events', 'eventsList')
            const locations = getList(result, 'locations', 'locationsList')
            const speakers = getList(result, 'speakers', 'speakersList')
            const groups = getList(result, 'groups', 'groupsList')
            const days = getList(result, 'days', 'daysList')

            events.forEach(event => {
                const categoryIds = event.category as Array<string>

                const categoriesList = categoryIds.map(categoryId => findObjectById(categories, categoryId))
                event.category = categoriesList
                const locationId = event.location as string

                if (locationId !== 'undefined' && locationId.length > 0) {
                    const location = findObjectById(locations, locationId)
                    if (location !== 'undefined') {
                        event.location = location.name
                    }
                }

                const speakerIds = event.speakers as Array<string>
                if (speakerIds.length > 0) {
                    const speakersList = speakerIds.map(speakerId => findObjectById(speakers, speakerId))
                    event.speakers = speakersList
                }

                event.title = event.title.trim()
            })

            events.map(event => {

                event.startTime = (event.startTime as Timestamp).toDate().toISOString()
                event.endTime = (event.endTime as Timestamp).toDate().toISOString()

                return event
            })


            const agenda =
            {
                'events': events,
                'groups': groups,
                'days': days,
                'speakers': speakers
            }


            response.send(agenda)
        })
        .catch(err => {
            console.log(err)
            response.status(500).send('Bad request')
        })
})

export const getModules = functions.https.onRequest((request, response) => {

    getCollectionPromise('modules').then((result) => {
        response.send(new GetModuleResponse(result))
    })
        .catch((err) => {
            response.status(500).send('Error getModules(): ' + err)
        })
})

//todo skasować wysyłanie errorów i wrzucić je do konsoli!!!
export const setRating = functions.https.onRequest((request, response) => {
    const body = request.body
    const uuid = body.id as string
    const eventId = body.event as string
    const rating = body.rating as number
    const comment = body.comment as string
    const version = body.version as string

    const calculatedVersion = calculateChecksum(eventId, uuid, comment, rating).toString()

    if (version !== calculatedVersion) {
        console.log('NIEPOPRAWNY NUMER WERSJI')
        response.status(401).send('Nie ma takiego oceniania')
        return
    }

    if (rating > 5 || rating < 0) {
        console.log('NIEPOPRAWNA OCENA')
        response.status(500).send('Niepoprawna ocena: ' + rating)
        return
    }

    const ratedEventPromise = admin.firestore().collection('agenda').doc('events').get().then(
        (eventsListSnap) => {
            const eventsList = eventsListSnap.get('eventsList') as Array<any>
            console.log("Rated event: " + eventsList.length);
            return eventsList.find(event => event.id === eventId)
        }
    )

    const configPromise = admin.firestore().doc('ratings/config').get()

    Promise.all([
        configPromise,
        ratedEventPromise
    ])
        .then((result) => {
            const config = result[0];
            const event = result[1];

            if (event === 'undefined') {
                console.log('rated event not found')
                return false;
            }

            const isEnabled = config.get('isEnabled') as boolean
            const eventStartTime = (event.startTime as Timestamp).toDate().getTime();
            const fromTime = (config.get('startDate') as Timestamp).toDate().getTime()
            const toTime = (config.get('endDate') as Timestamp).toDate().getTime()
            const now = Date.now();

            const afterFromTime = now > fromTime
            const beforToTime = now < toTime
            const afterStartTime = now > eventStartTime

            if (isEnabled && afterFromTime && beforToTime && afterStartTime) {
                console.log('Rating Enabled')
                return true
            } else {
                console.log('Rating Disabled')
                return false
            }
        })
        .then(isRatingOpen => {
            if (isRatingOpen) {
                const collection = admin.firestore().collection('ratings/ratings/ratingsList')

                collection
                    .where('uuid', '==', uuid)
                    .where('eventId', '==', eventId)
                    .get()
                    .then(result => {
                        if (!result.empty) {
                            if (result.size > 1) {
                                console.log('Filtrowanie rejtingów zwróciło więcej niź 1 wynik - WTF')
                                response.status(500).send('Błąd oceniania')
                                return
                            }

                            result.docs[0].ref.update({
                                'rating': rating,
                                'comment': comment
                            })
                                .then(_ => response.send('OK'))
                                .catch(err => response.status(500).send('setRating errorA: ' + err))

                        } else {

                            collection.add({
                                'rating': rating,
                                'comment': comment,
                                'eventId': eventId,
                                'uuid': uuid
                            })
                                .then(_ => response.send('OK'))
                                .catch(err => response.status(500).send('setRating errorB: ' + err))
                        }
                    })
                    .catch(err => {
                        console.log('Set rating error: ' + err)
                        response.status(500).send('setRating errorC: ' + err)
                    })

            } else {
                console.log('Sending 405 response')
                response.status(405).send('rating disabled')
            }
        })
        .catch(err => {
            console.log('Set rating error: ' + err)
            response.status(500).send('setRating errorD: ' + err)
        })
})

export const loadFromCsv = functions.https.onRequest((request, response) => {
    const daysPath = 'data/QE - Agenda 2019 - Dni.csv';
    const groupsPath = 'data/QE - Agenda 2019 - Grupy.csv';
    const categoriesPath = 'data/QE - Agenda 2019 - Kategorie.csv';
    const locationsPath = 'data/QE - Agenda 2019 - Lokacje.csv';
    const speakersPath = 'data/QE - Agenda 2019 - Prelegenci.csv';
    const eventsPath = 'data/QE - Agenda 2019 - Wydarzenia.csv';
    const articlesPath = 'data/QE - Agenda 2019 - Artykuły.csv';
    const partnersPath = 'data/QE - Agenda 2019 - Partnerzy.csv';

    const eventsFileStream = getFileStream(eventsPath)
    const speakersFileStream = getFileStream(speakersPath)
    const locationsFileStream = getFileStream(locationsPath)
    const categoriesFileStream = getFileStream(categoriesPath)
    const groupsFileStream = getFileStream(groupsPath)
    const daysFileStream = getFileStream(daysPath)
    const aarticleFileStream = getFileStream(articlesPath)
    const partnersFileStream = getFileStream(partnersPath)

    const partnersPromise = readObject(partnersFileStream, (json) => {
        return {
            'type': json.type,
            'photo': (json.photo as string).trim(),
            'order': Number.parseInt(json.order),
            'isLarge': json.isLarge === 'TRUE',
        }
    })

    const articlesPromise = readObject(aarticleFileStream, (json) => {
        let links = Array<String>()
        const linksList = json.links.split(', ') as Array<String>

        if (!(linksList.length === 1 && linksList[0].length === 0)) {
            links = linksList
        }

        return {
            'type': 'article',
            'title': (json.title as string).trim(),
            'label': (json.label as string).trim(),
            'photo': (json.photo as string).trim(),
            'content': json.content as string,
            'links': links,
            'order': Number.parseInt(json.order),
        }
    })

    const daysPromise = readObject(daysFileStream, (json) => {
        return {
            'name': json.name as string,
            'id': json.id as string,
            'order': Number.parseInt(json.order),
        }
    })

    const groupsPromise = readObject(groupsFileStream, (json) => {
        return {
            'name': json.name as string,
            'id': json.id as string,
            'order': Number.parseInt(json.order),
        }
    })

    const categoriesPromise = readObject(categoriesFileStream, (json) => {
        return {
            'color': json.color as string,
            'id': json.id as string,
            'name': json.name as string,
        }
    })

    const locationsPromise = readObject(locationsFileStream, (json) => {
        return {
            'id': json.id as string,
            'name': json.name as string,
        }
    })

    const speakersPromise = readObject(speakersFileStream, (json) => {
        return {
            'id': json.id as string,
            'name': json.name as string,
            'surname': json.surname as string,
            'title': json.title as string,
            'company': json.company as string,
            'photo': json.photo as string,
            'description': json.description as string,
            'featured': json.featured === 'TRUE',
        }
    })

    const eventsPromise = readObject(eventsFileStream, (json) => {

        const jsonStartTime = json.startTime as string
        const jsonEndTime = json.endTime
        const speakers = json.speakers as string
        const categories = json.category as string

        const speakersList = speakers.split(', ')
        const categoriesList = categories.split(', ')

        const startTime = firestore.Timestamp.fromDate(new Date(jsonStartTime));
        const endTime = firestore.Timestamp.fromDate(new Date(jsonEndTime));

        return {
            'id': json.id as string,
            'title': json.title as string,
            'dayId': json.dayId as string,
            'category': categoriesList,
            'group': json.group as string,
            'location': json.location as string,
            'description': json.description as string,
            'startTime': startTime,
            'endTime': endTime,
            'speakers': speakersList,
        }
    })

    Promise.all([
        clearList('agenda', 'locations', 'locationsList'),
        clearList('agenda', 'speakers', 'speakersList'),
        clearList('agenda', 'categories', 'categoriesList'),
        clearList('agenda', 'groups', 'groupsList'),
        clearList('agenda', 'days', 'daysList'),
        clearList('agenda', 'events', 'eventsList'),
        clearList('modules', 'parters', 'partnersList'),
    ])
        .then(_ => {
            return Promise.all([
                daysPromise,
                groupsPromise,
                categoriesPromise,
                locationsPromise,
                speakersPromise,
                eventsPromise,
                articlesPromise,
                partnersPromise,
            ])
        })
        .then((data) => {
            const daysSave = saveToDoc('agenda', 'days', {
                'daysList': data[0]
            })

            const groupsSave = saveToDoc('agenda', 'groups', {
                'groupsList': data[1]
            })

            const categoriesSave = saveToDoc('agenda', 'categories', {
                'categoriesList': data[2]
            })

            const locationsSave = saveToDoc('agenda', 'locations', {
                'locationsList': data[3]
            })

            const speakersSave = saveToDoc('agenda', 'speakers', {
                'speakersList': data[4]
            })

            const eventsSave = saveToDoc('agenda', 'events', {
                'eventsList': data[5]
            })

            const partnersSave = saveToDoc('modules', 'parters', {
                'partnersList': data[7]
            })

            const articlesArray = (data[6] as Array<any>)
            const articlePromises = new Array<Promise<WriteResult>>();

            articlesArray.forEach((article, index) => {
                const articlesSave = admin.firestore().collection('modules').doc('article' + index).set(article)
                articlePromises.push(articlesSave)
            })

            articlePromises.push(daysSave)
            articlePromises.push(groupsSave)
            articlePromises.push(categoriesSave)
            articlePromises.push(locationsSave)
            articlePromises.push(speakersSave)
            articlePromises.push(eventsSave)
            articlePromises.push(partnersSave)

            return Promise.all(articlePromises)
        }).then(async () => {
            const versionDoc = await admin.firestore().collection('agenda').doc('version').get();
            const currentVersion = versionDoc.get('number');
            return admin.firestore().collection('agenda').doc('version').update({
                'number': currentVersion + 1
            });

        }).then(() => response.send('OK')
        ).catch(err => {
            console.log(err)
            response.status(500).send('Error: ' + err)
        })
})

//todo dodać parametr Last Modified
export const getImage = functions.https.onRequest((request, response) => {
    const query = request.query
    let type = query.type
    let name = query.name as string
    const split = name.split('.')[1]
    if (split.length <= 1) {
        name += '.png'
    }

    switch (type) {
        case 'speakers': {
            type = 'speakers_photo/'
            break;
        }
        case 'article': {
            type = 'article_photo/'
            break;
        }
        case 'partner': {
            type = 'partners/'
            break;
        }
        case 'undefined':
        default: {
            type = "speakers_photo/";
            name = "photo1.png"
        };
    }

    const path = type + name;

    admin
        .storage()
        .bucket('eventaapp-5d3a4.appspot.com')
        .file(path)
        .download()
        .then((result) => {
            response.setHeader('Content-Type', 'image/png');
            response.send(result[0]);
        })
        .catch((err) => {
            response.status(500).send("error: " + err);
        });
})

export const onAgendaModified = functions.firestore.document('agenda/version').onUpdate((change, context) => {

    const payload = {
        "notification": {
            "content_available": 'true',
            "body": '',
            'title': '',
        },
        "data": {
            "content_available": 'true',
            "command": "/refreshAgenda",
        }
    }

    return admin.firestore().collection('tokens').get()
        .then(snap => {
            const tokens = snap.docs.map<string>(tokenSnap => {
                return tokenSnap.get('token')
            })

            return admin.messaging().sendToDevice(tokens, payload)
        })
        .then(response => {
            console.log('pushed: ' + response.successCount + ' fails: ' + response.failureCount)
            response.results.forEach(element => {
                console.log('push err: ' + element)
            });
        })
        .catch(err => {
            console.log(err)
        })
})

export const addFcmToken = functions.https.onRequest((request, response) => {
    const token = request.body.fcmToken

    const collection = admin.firestore().collection('tokens')

    collection.where('token', '==', token).get()
        .then(result => {
            if (result.empty) {
                return collection.add({ 'token': token })
            } else {
                throw Error('Already exists')
            }
        })
        .then(result => {
            response.send("OK")
        })
        .catch((err: Error) => {
            console.log(err)
            if (err.message === 'Already exists') {
                response.status(400).send('Token already exist')
            } else {
                response.status(500).send('Bad request')
            }
        })
})



function getList(agenda: QuerySnapshot, documentName: string, listName: string): Array<any> {
    const snap = agenda.docs.find((doc) => doc.id === documentName) as FirebaseFirestore.QueryDocumentSnapshot
    return snap.get(listName)
}

function getFileStream(filePath: string): Readable {
    return admin
        .storage()
        .bucket('eventaapp-5d3a4.appspot.com')
        .file(filePath)
        .createReadStream()
}

async function readObject(file: Readable, mapper: (object: any) => any): Promise<any> {
    const jsonObject = await csv().fromStream(file);
    return jsonObject.map(mapper);
}

function clearList(collectionName: string, docName: string, listName: string) {
    return admin.firestore().collection(collectionName).doc(docName).update({
        listName: FieldValue.delete()
    })
}

function saveToDoc(collectionName: string, docName: string, data: any) {
    return admin.firestore().collection(collectionName).doc(docName).set(data)
}

function findObjectById(list: Array<any>, id: string): any {
    return list.find(obj => (obj.id as string) === id)
}

function getCollectionPromise(collection: string): Promise<QuerySnapshot> {
    return admin.firestore().collection(collection).get()
}

///////////////////////////////////////////////////////////////
////////////////////////// DEV TOOLS //////////////////////////
///////////////////////////////////////////////////////////////
export const moveObjects = functions.https.onRequest((request, response) => {
    Promise.all([
        getCollectionPromise('agenda/events/eventsList'),
        getCollectionPromise('agenda/days/daysList'),
        getCollectionPromise('agenda/speakers/speakersList'),
        getCollectionPromise('agenda/locations/locationsList'),
        getCollectionPromise('agenda/groups/groupsList'),
        getCollectionPromise('agenda/categories/categoriesList')
    ])
        .then((agenda) => {
            //const groupsList = agenda[4].docs.map<GroupModel>(doc => new GroupModel(doc))
            const speakersList = agenda[2].docs.map<SpeakerModel>(doc => new SpeakerModel(doc))
            const categoriesList = agenda[5].docs.map<CategoryModel>(doc => new CategoryModel(doc))
            const locationsList = agenda[3].docs.map<LocationModel>(doc => new LocationModel(doc))
            const eventsList = agenda[0].docs.map<EventModel>(doc => new EventModel(doc, speakersList, locationsList, categoriesList))
            //const daysList = agenda[1].docs.map<DayModel>(doc => new DayModel(doc))
            //  {
            //   "id": "5l46PMZmYRmE4KUVsQZT",
            //   "title": "Event 4asdasd",
            //   "description": "Dupa",
            //   "startTime": "2019-02-28T06:00:00.000Z",
            //   "endTime": "2019-02-28T06:45:00.000Z",
            //   "category": {
            //     "name": "QA",
            //     "color": "#ff0000",
            //     "id": "gBPO2NKAyfezZ0ir7kYk"
            //   },
            //   "speakers": [
            //     {
            //       "name": "Ździsław",
            //       "surname": "Pierdziawka",
            //       "title": "Cokolwiek",
            //       "company": "Pierdziawka Co.",
            //       "description": "Nullam in metus. Curabitur lacinia quam. Aliquam interdum arcu vitae ante. Donec vitae facilisis luctus et risus neque nibh wisi, tempor tristique, sollicitudin vitae, vehicula est. Vestibulum fermentum. Vivamus elit iaculis lectus. Nam suscipit, velit a mi. Cras sed felis a ante ipsum dolor urna, pellentesque adipiscing elit. Proin porttitor magna. Sed molestie, neque. Etiam eu orci fermentum nec, nibh. Ut turpis. Morbi tincidunt. Maecenas orci dictum vel, dignissim dolor.  Suspendisse vestibulum varius. Cum sociis natoque penatibus et enim. Fusce ullamcorper ac, magna. Integer auctor auctor augue egestas quam sem, sed augue. Nulla pellentesque accumsan. Fusce eu scelerisque odio eget orci luctus sagittis. Vestibulum quam sagittis libero. Cras consectetuer elit. Mauris pretium, ipsum eleifend vel, wisi. Mauris pretium, diam ut malesuada fames ac nunc. Vestibulum non sem. Aenean congue tristique. Donec adipiscing elit. Cum sociis natoque penatibus et orci pellentesque.",
            //       "photo": "photo5",
            //       "featured": false,
            //       "id": "vPUbgM21MH67fknNNMfB"
            //     }
            //   ],
            //   "location": "Aula C",
            //   "group": "3nH3gdV6xzgBxGaapmP7",
            //   "dayId": "jxmPjrO0gP8TZfRWiywu"
            // },


            const documentData = eventsList.map((event) => {
                return {
                    'category': event.category.id,
                    'dayId': event.dayId,
                    'description': event.description,
                    'endTime': event.endTime,
                    'group': event.group,
                    'id': event.id,
                    'location': event.location,
                    'speakers': event.speakers.map(speaker => speaker.id),
                    'startTime': event.startTime,
                    'title': event.title,
                };
            })

            const test = {
                'eventsList': documentData
            }

            console.log(test)

            return admin.firestore().collection('agenda').doc('events').set(test)
        })
        .then((result) => {
            response.send('OK')
        })
        .catch(err => {
            console.log(err)
            response.status(500).send('Bad request: ' + err)
        })
})

export const getEvent = functions.https.onRequest((request, response) => {
    const eventId = request.query.eventId

    admin.firestore().doc('agenda/events/eventsList/' + eventId).get()
        .then((result) => {
            response.send({
                'title': result.get('title'),
                'description': result.get('description'),
                'dayId': (result.get('dayId') as DocumentReference).path,
                'location': (result.get('location') as DocumentReference).path,
                'group': (result.get('group') as DocumentReference).path,
                'startTime': (result.get('startTime') as Timestamp).toDate(),
                'endTime': (result.get('endTime') as Timestamp).toDate(),
                'speakers': (result.get('speakers') as DocumentReference[]).map(ref => ref.path)
            })
        })
        .catch((err) => {
            response.status(500).send('Error: ' + err)
        })
})

export const addField = functions.https.onRequest((request, response) => {
    //const body = request.body

    // change field type
    //const value = admin.firestore().doc(body.fieldValue)
    const value = admin.firestore().doc('/categories/gBPO2NKAyfezZ0ir7kYk')
    // change location
    admin.firestore().collection('agenda/events/eventsList').get()
        .then((eventsQuery) => {

            const sets: Promise<WriteResult>[] = new Array()

            eventsQuery.forEach((eventDoc) => {
                const promise = eventDoc.ref.set(
                    {
                        // change field name
                        'category': value
                    },
                    { merge: true }
                )
                sets.push(promise)
            })

            return Promise.all(sets)
        })
        .then(result => {
            response.send('OK')
        })
        .catch((err) => {
            response.status(500).send("Error: " + err)
        })
})

export const addEvent = functions.https.onRequest((request, response) => {
    const body = request.body

    const requestSpeakers = body.speakers as string[]
    const speakersRefs = requestSpeakers.map<DocumentReference>((speakerId) =>
        admin.firestore().doc(speakerId)
    )

    admin.firestore().collection('agenda/events/eventsList').add(
        {
            'title': body.title,
            'description': body.description,
            'dayId': admin.firestore().doc(body.dayId),
            'location': admin.firestore().doc(body.location),
            'group': admin.firestore().doc(body.group),
            'startTime': new Date(Date.parse(body.startTime)),
            'endTime': new Date(Date.parse(body.endTime)),
            'speakers': speakersRefs
        }
    )
        .then(result => {
            response.send('New event id: ' + result.id)
        })
        .catch((err) => {
            response.status(500).send("Error: " + err)
        })
})