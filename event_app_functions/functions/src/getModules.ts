import * as functions from 'firebase-functions'
import { getCollectionPromise } from './common';
import { GetModuleResponse } from './model';

export const getModules = functions.https.onRequest((request, response) => {

    getCollectionPromise('modules').then((result) => {
        response.send(new GetModuleResponse(result))
    })
        .catch((err) => {
            response.status(500).send('Error getModules(): ' + err)
        })
})