import {Md5} from 'ts-md5/dist/md5'

export function calculateChecksum(eventId: string, uuid: string, comment: string, rate: number): number{
    let i = 3
    const modulo = 748
    const substr = 5
    const maxSum = 2147483647
    const moduloResult = 1

    const eventHash = Md5.hashStr(eventId.toLowerCase()).toString().toLowerCase()
    console.log('eventHash: ' + eventHash)

    const rateHash = Md5.hashStr(rate.toString().toLowerCase()).toString().toLowerCase()
    console.log('rateHash: ' + rateHash)

    const combined = rateHash + uuid.toLowerCase() + eventHash + comment.toLowerCase()
    console.log('combined: ' + combined)

    const combinedHash = Md5.hashStr(combined).toString().toLowerCase()
    console.log('combinedHash: ' + combinedHash)

    const digest = Number.parseInt(combinedHash.substring(0, substr), 16)
    console.log('digest: ' + digest)

    let sum = 0

    while(true){
        sum = digest + i 

        if((sum % modulo === moduloResult) || sum >= maxSum){
            break
        }

        i += 1
    }
    console.log('sum: ' + sum)
    return sum
}