import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'

export const addFcmToken = functions.https.onRequest((request, response) => {
    const token = request.body.fcmToken

    const collection = admin.firestore().collection('tokens')

    collection.where('token', '==', token).get()
        .then(result => {
            if (result.empty) {
                return collection.add({ 'token': token })
            } else {
                throw Error('Already exists')
            }
        })
        .then(result => {
            response.send("OK")
        })
        .catch((err: Error) => {
            console.log(err)
            if (err.message === 'Already exists') {
                response.status(400).send('Token already exist')
            } else {
                response.status(500).send('Bad request')
            }
        })
})