import { Timestamp } from "@google-cloud/firestore";
import { findObjectById, getList, getCollectionPromise } from "./common";
import * as functions from 'firebase-functions'

export const getAgenda = functions.https.onRequest((request, response) => {
    getCollectionPromise('agenda')
        .then((result) => {
            const categories = getList(result, 'categories', 'categoriesList')
            const events = getList(result, 'events', 'eventsList')
            const locations = getList(result, 'locations', 'locationsList')
            const speakers = getList(result, 'speakers', 'speakersList')
            const groups = getList(result, 'groups', 'groupsList')
            const days = getList(result, 'days', 'daysList')

            events.forEach(event => {
                const categoryIds = event.category as Array<string>

                const categoriesList = categoryIds.map(categoryId => findObjectById(categories, categoryId))
                event.category = categoriesList
                const locationId = event.location as string

                if (locationId !== 'undefined' && locationId.length > 0) {
                    const location = findObjectById(locations, locationId)
                    if (location !== 'undefined') {
                        event.location = location.name
                    }
                }

                const speakerIds = event.speakers as Array<string>
                if (speakerIds.length > 0) {
                    const speakersList = speakerIds.map(speakerId => findObjectById(speakers, speakerId))
                    event.speakers = speakersList
                }

                event.title = event.title.trim()
            })

            events.map(event => {

                event.startTime = (event.startTime as Timestamp).toDate().toISOString()
                event.endTime = (event.endTime as Timestamp).toDate().toISOString()

                return event
            })


            const agenda =
            {
                'events': events,
                'groups': groups,
                'days': days,
                'speakers': speakers
            }


            response.send(agenda)
        })
        .catch(err => {
            console.log(err)
            response.status(500).send('Bad request')
        })
})