import * as admin from 'firebase-admin'
import { QuerySnapshot, FieldValue } from '@google-cloud/firestore';
import { Readable } from 'stream';
import * as csv from 'csvtojson'

export function getList(agenda: QuerySnapshot, documentName: string, listName: string): Array<any> {
    const snap = agenda.docs.find((doc) => doc.id === documentName) as FirebaseFirestore.QueryDocumentSnapshot
    return snap.get(listName)
}

export function getFileStream(filePath: string): Readable {
    return admin
        .storage()
        .bucket('eventaapp-5d3a4.appspot.com')
        .file(filePath)
        .createReadStream()
}

export async function readObject(file: Readable, mapper: (object: any) => any): Promise<any> {
    const jsonObject = await csv().fromStream(file);
    return jsonObject.map(mapper);
}

export function clearList(collectionName: string, docName: string, listName: string) {
    return admin.firestore().collection(collectionName).doc(docName).update({
        listName: FieldValue.delete()
    })
}

export function saveToDoc(collectionName: string, docName: string, data: any) {
    return admin.firestore().collection(collectionName).doc(docName).set(data)
}

export function findObjectById(list: Array<any>, id: string): any {
    return list.find(obj => (obj.id as string) === id)
}

export function getCollectionPromise(collection: string): Promise<QuerySnapshot> {
    return admin.firestore().collection(collection).get()
}