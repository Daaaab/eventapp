import { calculateChecksum } from './rating_checksum'
import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'
import { Timestamp, WriteResult } from '@google-cloud/firestore';
import { readObject, getFileStream, clearList, saveToDoc } from './common';
import firestore = require('@google-cloud/firestore');

//todo skasować wysyłanie errorów i wrzucić je do konsoli!!!
export const setRating = functions.https.onRequest((request, response) => {
    const body = request.body
    const uuid = body.id as string
    const eventId = body.event as string
    const rating = body.rating as number
    const comment = body.comment as string
    const version = body.version as string

    const calculatedVersion = calculateChecksum(eventId, uuid, comment, rating).toString()

    if (version !== calculatedVersion) {
        console.log('NIEPOPRAWNY NUMER WERSJI')
        response.status(401).send('Nie ma takiego oceniania')
        return
    }

    if (rating > 5 || rating < 0) {
        console.log('NIEPOPRAWNA OCENA')
        response.status(500).send('Niepoprawna ocena: ' + rating)
        return
    }

    const ratedEventPromise = admin.firestore().collection('agenda').doc('events').get().then(
        (eventsListSnap) => {
            const eventsList = eventsListSnap.get('eventsList') as Array<any>
            console.log("Rated event: " + eventsList.length);
            return eventsList.find(event => event.id === eventId)
        }
    )

    const configPromise = admin.firestore().doc('ratings/config').get()

    Promise.all([
        configPromise,
        ratedEventPromise
    ])
        .then((result) => {
            const config = result[0];
            const event = result[1];

            if (event === 'undefined') {
                console.log('rated event not found')
                return false;
            }

            const isEnabled = config.get('isEnabled') as boolean
            const eventStartTime = (event.startTime as Timestamp).toDate().getTime();
            const fromTime = (config.get('startDate') as Timestamp).toDate().getTime()
            const toTime = (config.get('endDate') as Timestamp).toDate().getTime()
            const now = Date.now();

            const afterFromTime = now > fromTime
            const beforToTime = now < toTime
            const afterStartTime = now > eventStartTime

            if (isEnabled && afterFromTime && beforToTime && afterStartTime) {
                console.log('Rating Enabled')
                return true
            } else {
                console.log('Rating Disabled')
                return false
            }
        })
        .then(isRatingOpen => {
            if (isRatingOpen) {
                const collection = admin.firestore().collection('ratings/ratings/ratingsList')

                collection
                    .where('uuid', '==', uuid)
                    .where('eventId', '==', eventId)
                    .get()
                    .then(result => {
                        if (!result.empty) {
                            if (result.size > 1) {
                                console.log('Filtrowanie rejtingów zwróciło więcej niź 1 wynik - WTF')
                                response.status(500).send('Błąd oceniania')
                                return
                            }

                            result.docs[0].ref.update({
                                'rating': rating,
                                'comment': comment
                            })
                                .then(_ => response.send('OK'))
                                .catch(err => response.status(500).send('setRating errorA: ' + err))

                        } else {

                            collection.add({
                                'rating': rating,
                                'comment': comment,
                                'eventId': eventId,
                                'uuid': uuid
                            })
                                .then(_ => response.send('OK'))
                                .catch(err => response.status(500).send('setRating errorB: ' + err))
                        }
                    })
                    .catch(err => {
                        console.log('Set rating error: ' + err)
                        response.status(500).send('setRating errorC: ' + err)
                    })

            } else {
                console.log('Sending 405 response')
                response.status(405).send('rating disabled')
            }
        })
        .catch(err => {
            console.log('Set rating error: ' + err)
            response.status(500).send('setRating errorD: ' + err)
        })
})

export const loadFromCsv = functions.https.onRequest((request, response) => {
    const daysPath = 'data/QE - Agenda 2019 - Dni.csv';
    const groupsPath = 'data/QE - Agenda 2019 - Grupy.csv';
    const categoriesPath = 'data/QE - Agenda 2019 - Kategorie.csv';
    const locationsPath = 'data/QE - Agenda 2019 - Lokacje.csv';
    const speakersPath = 'data/QE - Agenda 2019 - Prelegenci.csv';
    const eventsPath = 'data/QE - Agenda 2019 - Wydarzenia.csv';
    const articlesPath = 'data/QE - Agenda 2019 - Artykuły.csv';
    const partnersPath = 'data/QE - Agenda 2019 - Partnerzy.csv';

    const eventsFileStream = getFileStream(eventsPath)
    const speakersFileStream = getFileStream(speakersPath)
    const locationsFileStream = getFileStream(locationsPath)
    const categoriesFileStream = getFileStream(categoriesPath)
    const groupsFileStream = getFileStream(groupsPath)
    const daysFileStream = getFileStream(daysPath)
    const aarticleFileStream = getFileStream(articlesPath)
    const partnersFileStream = getFileStream(partnersPath)

    const partnersPromise = readObject(partnersFileStream, (json) => {
        return {
            'type': json.type,
            'photo': (json.photo as string).trim(),
            'order': Number.parseInt(json.order),
            'isLarge': json.isLarge === 'TRUE',
        }
    })

    const articlesPromise = readObject(aarticleFileStream, (json) => {
        let links = Array<String>()
        const linksList = json.links.split(', ') as Array<String>

        if(!(linksList.length === 1 && linksList[0].length === 0)){
            links = linksList
        }

        return {
            'type': 'article',
            'title': (json.title as string).trim(),
            'label': (json.label as string).trim(),
            'photo': (json.photo as string).trim(),
            'content': json.content as string,
            'links': links,
            'order': Number.parseInt(json.order),
        }
    })

    const daysPromise = readObject(daysFileStream, (json) => {
        return {
            'name': json.name as string,
            'id': json.id as string,
            'order': Number.parseInt(json.order),
        }
    })

    const groupsPromise = readObject(groupsFileStream, (json) => {
        return {
            'name': json.name as string,
            'id': json.id as string,
            'order': Number.parseInt(json.order),
        }
    })

    const categoriesPromise = readObject(categoriesFileStream, (json) => {
        return {
            'color': json.color as string,
            'id': json.id as string,
            'name': json.name as string,
        }
    })

    const locationsPromise = readObject(locationsFileStream, (json) => {
        return {
            'id': json.id as string,
            'name': json.name as string,
        }
    })

    const speakersPromise = readObject(speakersFileStream, (json) => {
        return {
            'id': json.id as string,
            'name': json.name as string,
            'surname': json.surname as string,
            'title': json.title as string,
            'company': json.company as string,
            'photo': json.photo as string,
            'description': json.description as string,
            'featured': json.featured === 'TRUE',
        }
    })

    const eventsPromise = readObject(eventsFileStream, (json) => {

        const jsonStartTime = json.startTime as string
        const jsonEndTime = json.endTime
        const speakers = json.speakers as string
        const categories = json.category as string

        const speakersList = speakers.split(', ')
        const categoriesList = categories.split(', ')

        const startTime = firestore.Timestamp.fromDate(new Date(jsonStartTime));
        const endTime = firestore.Timestamp.fromDate(new Date(jsonEndTime));

        return {
            'id': json.id as string,
            'title': json.title as string,
            'dayId': json.dayId as string,
            'category': categoriesList,
            'group': json.group as string,
            'location': json.location as string,
            'description': json.description as string,
            'startTime': startTime,
            'endTime': endTime,
            'speakers': speakersList,
        }
    })

    Promise.all([
        clearList('agenda', 'locations', 'locationsList'),
        clearList('agenda', 'speakers', 'speakersList'),
        clearList('agenda', 'categories', 'categoriesList'),
        clearList('agenda', 'groups', 'groupsList'),
        clearList('agenda', 'days', 'daysList'),
        clearList('agenda', 'events', 'eventsList'),
        clearList('modules', 'parters', 'partnersList'),
    ])
        .then(_ => {
            return Promise.all([
                daysPromise,
                groupsPromise,
                categoriesPromise,
                locationsPromise,
                speakersPromise,
                eventsPromise,
                articlesPromise,
                partnersPromise,
            ])
        })
        .then((data) => {
            const daysSave = saveToDoc('agenda', 'days', {
                'daysList': data[0]
            })

            const groupsSave = saveToDoc('agenda', 'groups', {
                'groupsList': data[1]
            })

            const categoriesSave = saveToDoc('agenda', 'categories', {
                'categoriesList': data[2]
            })

            const locationsSave = saveToDoc('agenda', 'locations', {
                'locationsList': data[3]
            })

            const speakersSave = saveToDoc('agenda', 'speakers', {
                'speakersList': data[4]
            })

            const eventsSave = saveToDoc('agenda', 'events', {
                'eventsList': data[5]
            })

            const partnersSave = saveToDoc('modules', 'parters', {
                'partnersList': data[7]
            })

            const articlesArray = (data[6] as Array<any>)
            const articlePromises = new Array<Promise<WriteResult>>();

            articlesArray.forEach((article, index) => {
                const articlesSave = admin.firestore().collection('modules').doc('article' + index).set(article)
                articlePromises.push(articlesSave)
            })

            articlePromises.push(daysSave)
            articlePromises.push(groupsSave)
            articlePromises.push(categoriesSave)
            articlePromises.push(locationsSave)
            articlePromises.push(speakersSave)
            articlePromises.push(eventsSave)
            articlePromises.push(partnersSave)

            return Promise.all(articlePromises)
        }).then(async () => {
            const versionDoc = await admin.firestore().collection('agenda').doc('version').get();
            const currentVersion = versionDoc.get('number');
            return admin.firestore().collection('agenda').doc('version').update({
                'number': currentVersion + 1
            });

        }).then(() => response.send('OK')
        ).catch(err => {
            console.log(err)
            response.status(500).send('Error: ' + err)
        })
})