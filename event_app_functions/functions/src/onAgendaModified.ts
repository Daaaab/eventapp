import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'

export const onAgendaModified = functions.firestore.document('agenda/version').onUpdate((change, context) => {

    const payload = {
        notification: {
            title: "You got a new Message",
            body: '/refreshAgenda',
        }
    }

    return admin.firestore().collection('tokens').get()
        .then(snap => {
            const tokens = snap.docs.map<string>(tokenSnap => {
                return tokenSnap.get('token')
            })
            return admin.messaging().sendToDevice(tokens, payload)
        })
        .then(response => {
            console.log('pushed: ' + response.successCount + ' fails: ' + response.failureCount)
            response.results.forEach(element => {
                console.log('push err: ' + element)
            });
        })
        .catch(err => {
            console.log(err)
        })
})